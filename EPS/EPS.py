#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 14:38:11 2019

@author: Claire Castell, 2019
@checked: Boris Segret, 2020
Version: 5.7 (May 2020)
+ 5.7.1: the Lancelot fix in eclipse computations
"""
import sys
from pathlib import Path
# DARKMAGIC this is just to overcome the fact we are not using pip to install docks_tools & Intervisibility as a real dependancy
sys.path.append(str(Path(__file__).resolve().parent.parent / "docks_tools/"))
sys.path.append(str(Path(__file__).resolve().parent.parent / "Intervisibility/"))

import config_EPS as conf_data
import energy_sim_battery as esb
import yaml
import warnings
from os import system
from astropy.utils.exceptions import AstropyWarning

warnings.simplefilter("ignore", category=AstropyWarning)
# import the configuration file from the terminal


def EPS_simulation(data, input_yaml):
    if sys.platform == "linux":
        system("clear")
    elif sys.platform == "windows":
        system("cls")
    print("\n" + "-" * 30 + input_yaml.split("/")[-1].split(".")[0] + "-" * 30 + "\n")
    launching_path = Path.cwd()
    config_path = launching_path / input_yaml

    yaml_directory = str(config_path.parent) + "/"

    esb.evaluate_battery_charge(data, yaml_directory=yaml_directory)
    return 1





