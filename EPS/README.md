DOCKS EPS module, __v6.0__

Date of modification : *28-Jan-2021*

Authors
=============================================

Development by Observatory of Paris - PSL Université Paris - 
Laboratory of Excellence ESEP, PSL Space Pole [CENSUS](https://cceres.psl.eu/?lang=en) 

2020: Boris SEGRET, Thibault DELRIEU, Rashika JAIN

Past contributors: J.DALBINS (2019), C.CASTELL (2019), G.THEBAULT (2017), B.TERRIS (2017), E.FOURNIER-BIDOZ (2015), F.ORSTADIUS (2015), S.WOHLGEMUT (2015), A.NIRELLO (2015), S.DELAIRE (2015)

&nbsp;

---

[TOC]

---

&nbsp;

Features of the DOCKS EPS module
=============================================

The DOCKS EPS module estimates the remaining energy over the time of a nanosatellite mission:
it cumulates the solar energy produced by fixed solar cells and substracts the energy used as specified by a list of functional modes.

- 	computes Eclipse events via DOCKS Intervisibility module: an "Event File" (EVTF) is produced
-	accepts any kind of design of fixed solar cells (the shadowing possible effects are not computed)
-	solar cells: includes a raw material model, a temperature model and an efficiency model (no aging)
-	battery: includes a basic model (no temperature, no aging)
-	a single configuration file gathers all paths & files needed

**Compatibility: This module has been successfullt tested with Ubuntu 18 and Windows 10. If it does not work with other OS, the user can use remote service!**

&nbsp;

Remote Service
=============================================

A remote service has been developed recently to prevent the user from doing any installation process on their system. Folow these steps to take advantage of this:

1. Send an email to **`docks.contact@obspm.fr`** with the keyword **`[server]`** in the subject. 
2. In return you will receive an email with a link to a shared directory.
3. Read the file "README.md" to understand the working of remote service.
4. Upload the input files in "input" folder and configuration file in "EPS" folder. 
3. Once you upload all the required files, our server will compute the results for you.
4. All the output files will be saved in "output" folder. 
5. If the submitted configuration file is moved to output folder, then the computation is complete.

**Note:** for now, the user is not notified once the computation is complete. So, the user will have to check manually if the results are generated are not. **There is a limit of 1 week on this shared directory, after which the directory will be deleted automatically.**


&nbsp;

Installing DOCKS EPS module
=============================================

**Before installing DOCKS EPS module, please install:** 

* DOCKS Tools module  1.3+ [link](https://gitlab.com/cceres-docks/docks_tools)
* DOCKS Intervisibility 2.0+ module[link](https://gitlab.com/cceres-docks/intervisibility)
* Python 3.7+ 

### Linux __and__ MS-Windows Users

1. Considering that `(project/)` is the path to your local workspace for DOCKS modules, create there a subfolder __"DOCKS"__ to store all functional modules. 

2. Download the full module from the link provided in Gitlab > Repository > __Tags__ and decompress as `(project/)DOCKS/EPS` subfolder. 

3. Put the folder docks_tools previously downloaded in  `(project/)DOCKS/docks_tools` 

4. Put the folder Intervisibility previously downloaded in  `(project/)DOCKS/Intervisibility` 

That's it, you don't have to do anything more: at the first run, the program will automatically install the dependencies needed if it doesn't find them. For compatibility reasons with embedded libraries, DOCKS may need to *downgrade* some libraries to specific versions and will ask you before proceeding. The versions of libraries before DOCKS installation will then be saved at the root of the module's folder EPS.

### Test the installation

Two test cases are provided in `example/`. The inputs required for these tests are in `example/input`. In order to test if the installation is successful, just run the test config file using the following command in terminal/windows prompt:
`$ python3 (path-to-EPS/)main.py --config_file=(path-to-EPS/)example/testMe_config.yaml>`

`(path-to-EPS/)` being the path to your DOCKS EPS module folder. 

The program shall finish with "End of processing" and the output files should be saved in `example/output`.

### Common installation errors 

1. **Linux  LLVM error**: If you have an LLVM error on linux, open a shell and write :
` $ sudo apt install llvm-9`
and then 
` $ export LLVM_CONFIG=/usr/bin/llvm-config-9`

2. Sometimes, if after first installation the program does not run successfully, try to run it again.

&nbsp;

Configuration File: .yaml
=============================================

From this version, the configuration must be given in a ".yaml" file. You can find an example of this file here: `(path-to-EPS/)example/testMe_config.yaml`. The section of the lines after '#' are comments, not interpreted by the program. The example test file provides extensive comments to help you with the needed inputs.

>==**WARNING** : Both absoloute or relative paths can be used in the configuration file. If relative paths are used, they should be specified relative to the location of the configuration file.==

&nbsp;

Running DOCKS EPS module
=============================================

In order to run the EPS module you need to enter a command with the following pattern:
`$ python3 (path-to-EPS/)main.py --config_file=(path-to-config/)CONFIG_FILE`

`(path-to-EPS/)` : relative or absolute path to EPS.py (within DOCKS EPS), no need to run python from any particular location. 
`(path-to-config/)` : relative or absolute path to your configuration file folder. 

For instance :

1. your current directory is : `/home/user/IamHere/`
2. DOCKS EPS is at : `/home/DOCKS/EPS` 
3. the configuration file is at : `/home/config_folder/config_file.yaml` 

Then enter the commande line :
`$ python3 ../../DOCKS/EPS/main.py --config_file=../../config_folder/config_file.yaml`

or you can also use the absolute path (with your OS' syntax, here in Linux): 
` $ python3 /home/DOCKS/EPS/main.py --config_file=/home/config_folder/config_file.yaml`

&nbsp;

Inputs 
=============================================

In order to compute with DOCKS EPS module, you need to provide 4 to 5 inputs :

1. a config file (.yaml), see section above. 

2. a trajectory file (.txt), in the CCSDS format given by the example provided in `(project/)DOCKS/EPS/example/input/traj_test_file.txt`. The DOCKS Propagator module produces trajectories in this format. 

3. a file of quaternions (.txt), in CCSDS the format given by the example provided in `(project/)DOCKS/EPS/example/input/QUATERNIONS.txt`. 

4. a file of modes (.txt), in the CCSDS format given by the example provided in `(project/)DOCKS/EPS/example/input/MODES.txt`. 

5. (optional) a file of eclipse events (.txt). This is a binary file generated specifically by the DOCKS Intervisibility module. 
 **If you don't have the file**, your configuration file must give the path wanted for output of DOCKS Intervisibility module (`output_eclipse_file_EPS` and `output_eclipse_file_vts`). DOCKS EPS will itself run the Intervisibility module.
  **If you have the file**, then you need to write the path of this file in `input_eclipse_file_EPS` (*in this case you can also specify the path `output_eclipse_file_vts` where you want the eclipse file for vts to be written*)

&nbsp;

Outputs
============================================================

DOCKS EPS module generates 3 or 5 outputs:

1. An energy file that indicates the energy present on-board at any time during the mission.
2. A flags file that generates warning flags if energy is below a specific level.
3. An angles file that gives the solar incidence angle of solar panels.
4. (optional) If demanded by the user, 2 intervisibility EVTF files (1 for user, 1 for EPS) containing information about eclipses and ground station passes.
