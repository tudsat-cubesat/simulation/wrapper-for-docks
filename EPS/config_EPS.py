#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 17:13:59 2019

@author: ccastell
"""
import numpy as np


def config_eps(config_file_path, action_type, var=[]):
    """
    This function generates a list of data related to the simulator

    :param config_file_path: (file .conf) Data file
    :param action_type: (str) Action requested ('save' the data in the file or 'load' the data)
    :param var: (list, size: number of variables)

        output_trajectory_intervisibility_file: (file) file with the satellite position and intervisibility value
        time_format: (str) exemple 'mjd', 'jd', 'iso'
        start: (astropy format) start time
        end:   (astropy format) end timed
        attitude_sat_file: (file) file with the satellite attitude, either in quaternions or in euler angles
        output_incident_angle: (file) output angles file
        output_energy: (file) output energy file
        output_eclipse: (file) output eclipse file

        mode_file: (file) contains the modes during the simulation
        modes_number: (int) number of modes N
        modes_names: (str list, 1*N)
        modes_values: (float list, 1*N)
        modes_average: (float) if the user wants to simulate a trajectory with an average power consumption in W

        capacity_Wh: (float) battery capacity in Wh (can not be filled if both capacity_Ah and voltage are given)
        capacity_Ah: (float) battery capacity in Ah (can not be filled if capacity_Wh is given)
        voltage: (float) battery voltage in V (can not be filled if capacity_Wh is given)
        initial_charge: (float) level of charge of the battery, number between 0 and 1
        minimum_charge: (float) minimum level of charge of the battery, the programs sends a flag if not respected, number between 0 and 1

        cells_type: (str) cells material ('gaas', 'si')  (can not be filled if (Absorption_coefficient, Maximum_efficiency, Coeff_efficiency) are given)
        surface: (float) surface of one cell, (str) surface unit
        thickness: (float) thickness of the cell in cm
        density: (float) density of the material in g/cm³
        conductivity: (float) conductivity of the material in (J/degC.kg)
        absorption_coefficient: (float) depends on the material, (can not be filled if cells_type is given)
        maximum_efficiency: (float) depends on the material, in percent, (can not be filled if cells_type is given)
        efficiency_coefficient: (float) depends on the material, in degC/percent, (can not be filled if cells_type is given)
        panels_position: (list of vector, N*3) euler angle describing the position of the panels, angles in degrees
        sequences

    """
    meta_start = "**EPS_START**"
    meta_stop = "**EPS_STOP**"

    param_number = [
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        2,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
    ]
    param_name = [
        "input_trajectory_intervisibility_file",
        "input_eclipse_file",
        "time_format",
        "start",
        "end",
        "attitude_sat_file",
        "output_incident_angle",
        "output_energy",
        "output_eclipse",
        "output_flag",
        "mode_file",
        "modes_number",
        "modes_names",
        "modes_values",
        "modes_average",
        "capacity_Wh",
        "capacity_Ah",
        "voltage",
        "initial_charge",
        "minimum_charge",
        "cells_type",
        "surface",
        "thickness",
        "density",
        "thermal_capacity",
        "absorption_coefficient",
        "maximum_efficiency",
        "efficiency_coefficient",
        "panel_position",
        "sequences",
    ]

    param_name = np.array(param_name)
    param_number = np.array(param_number)
    if action_type == "save":
        data = save_config(
            config_file_path, meta_start, meta_stop, param_name, param_number, var
        )
    elif action_type == "load":
        data = load_config(
            config_file_path, meta_start, meta_stop, param_name, param_number
        )
    else:
        raise ValueError("wrong value for action-type, just save or load")
    return data


name_param_separator = "="
sous_param_separator = ";"
comment_separator = "#"


def save_config(config_file_path, meta_start, meta_stop, param_name, param_number, var):
    f_save = open(config_file_path, "w")

    f_save.write(meta_start + "\n")

    for i in range(param_name.size):
        st_var = ""
        #        if (param_number[i] != 1):
        for j in range(param_number[i]):
            st_var = st_var + np.str(var[i][j]) + ";"
        #        else:
        #            st_var=np.str(var[i])
        f_save.write(param_name[i] + " = " + st_var[:-1] + "\n")

    f_save.write(meta_stop + "\n")

    f_save.close()

    return 1


def load_config(config_file_path, meta_start, meta_stop, param_name, param_number):
    if param_number.size != param_name.size:
        raise ValueError("wrong number for param_name and param_number")

    f_load = open(config_file_path, "r")
    text = f_load.readlines()
    f_load.close()
    text = np.array(text)
    for i in range(text.size):
        text[i] = text[i].strip()
    for i in range(text.size):
        text[i] = text[i].replace(" ", "")

    text_no_comment = []
    for i in range(text.size):
        if text[i][0] != comment_separator:
            res = text[i].find(comment_separator)
            if res != -1:
                text_no_comment.append(text[i][:res])
            else:
                text_no_comment.append(text[i])
    text_no_comment = np.array(text_no_comment)

    start = -1
    stop = -1
    for i in range(text_no_comment.size):
        if text_no_comment[i] == meta_start:
            start = i
        if text_no_comment[i] == meta_stop:
            stop = i

    if start == -1 or stop == -1 or stop - start <= 0:
        raise ValueError(
            "no have meta_start or/and meta_stop in config file or wrong position"
        )
    else:
        text_sta_sto_detect = text_no_comment[start + 1 : stop]

    filter_text = []
    for i in range(param_name.size):
        s_size = len(param_name[i])
        for j in range(text_sta_sto_detect.size):
            if text_sta_sto_detect[j][:s_size] == param_name[i]:
                filter_text.append(text_sta_sto_detect[j])
                break
    if len(filter_text) != param_name.size:
        raise ValueError("all parameter are not detected")

    var_filter = []
    for i in filter_text:
        var_filter.append(i.split(name_param_separator))

    var_filter_f = []
    for i in var_filter:
        var_filter_f.append(i[1:])

    var_filter_ff = []
    for i in var_filter_f:
        var_filter_ff.append(i[0].split(sous_param_separator))

    for i in range(len(var_filter_ff)):
        if len(var_filter_ff[i]) != param_number[i]:
            raise ValueError(
                "Wrong number parameters for this parameter :", param_name[i]
            )

    var_filter_ff = np.array(var_filter_ff)

    return var_filter_ff
