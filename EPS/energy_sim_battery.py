
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 10 14:22:38 2019

@author: ccastell
"""

from math import *
from astropy.time import Time
from astropy import units as u
from datetime import datetime
import numpy as np
import sys
from tqdm import tqdm

import ccsds as cc
import energy_sim_panel as esp
import modes_powerdemand as mpd

# ______________________________START_PROGRAM_________________________________
# _____________________________________________________________________________


def evaluate_battery_charge(data, yaml_directory):
    """
    This function computes the charge of the battery

    :param power_demand: (list, size:number of steps) value of the power demand at each step
    :param capacity_wh: (float)
    :param initial_charge: (float)
    :param sum_power: (list, size:number of steps) total of the produced power for all panels at each step

    :return: battery_energy: (list, size:number of steps) value of the energy of the battery
             charge_info: (list, size:number of steps) values between 0 and 1 giving the level of charge of the battery
    """

    # Data reading

    path_input = data["pathFiles"]["input"]
    path_output = data["pathFiles"]["output"]
    time_informations = data["timeInformations"]
    modes = data["modes"]
    battery_characteristics = data["batteryCharacteristics"]
    cells_characteristics = data["cellsCharacteristics"]
    satellite_geometry = data["satelliteGeometry"]

    capacity_ah = battery_characteristics["capacity_Ah"]
    voltage = battery_characteristics["voltage"]
    capacity_wh = battery_characteristics["capacity_Wh"]
    if type(capacity_wh) != float:
        if capacity_ah == "" or voltage == "":
            raise ValueError("Data value missing")
        else:
            capacity_wh = (float(capacity_ah) * float(voltage)) * u.W * u.h
    else:
        capacity_wh = float(capacity_wh) * u.W * u.h

    initial_charge = (
        float(battery_characteristics["initial_charge"]) * u.dimensionless_unscaled
    )
    minimum_charge = (
        float(battery_characteristics["minimum_charge"]) * u.dimensionless_unscaled
    )

    # Calculate the produced power and the consumed power
    from time import time
    
    sum_power, time_vector, time_step, time_vector_vts = esp.evaluate_produced_power(
        data, yaml_directory
    )

    power_demand = mpd.modes_values_time(time_vector, data, yaml_directory) * u.W

    print("Compute battery charge")
    # Start compute the battery charge

    dp = []
    battery_energy = [capacity_wh * initial_charge]
    charge_info = [initial_charge]

    for i in tqdm(range(1, len(time_step))):
        dpower = sum_power[i - 1] - power_demand[i - 1]
        dp.append(dpower)

        if 0 * u.W * u.h < battery_energy[i - 1] < capacity_wh:
            q_value = dpower * (time_step[-1] - time_step[0]) / (len(time_step))
            if battery_energy[i - 1] + q_value > capacity_wh:
                battery_energy.append(capacity_wh)
                charge_info.append(capacity_wh / capacity_wh)
            elif battery_energy[i - 1] + q_value <= 0 * u.W * u.h:
                battery_energy.append(0 * u.W * u.h)
                charge_info.append((0 * u.W * u.h) / capacity_wh)
            else:
                battery_energy.append(battery_energy[i - 1] + q_value)
                charge_info.append((battery_energy[i - 1] + q_value) / capacity_wh)

        elif battery_energy[i - 1] <= 0 * u.W * u.h:
            if dpower < 0:
                battery_energy.append(0 * u.W * u.h)
                charge_info.append((0 * u.W * u.h) / capacity_wh)
            else:
                q_value = dpower * (time_step[-1] - time_step[0]) / (len(time_step))
                battery_energy.append(battery_energy[i - 1] + q_value)
                charge_info.append((battery_energy[i - 1] + q_value) / capacity_wh)

        else:

            if dpower > 0:
                battery_energy.append(battery_energy[i - 1])
                charge_info.append((battery_energy[i - 1]) / capacity_wh)

            else:
                q_value = dpower * (time_step[-1] - time_step[0]) / (len(time_step))
                battery_energy.append(battery_energy[i - 1] + q_value)
                charge_info.append((battery_energy[i - 1] + q_value) / capacity_wh)

    for i in range(len(battery_energy)):
        battery_energy[i] = (round(battery_energy[i].value, 3)) * u.W * u.h

    # Start flag writing

    flag_list = []
    for i in range(len(charge_info)):
        flag_list.append(str(round(charge_info[i].value, 2)))

    (
        cells_type,
        surface,
        thick,
        dens,
        cond,
        abs_c,
        eff,
        coeff,
        position,
        seq,
    ) = esp.data_read(data)
    s = ""
    for dim in surface:
        s = s + str(dim.value) + "m2  "

    # Start writing the output files

    """
    path_input = data["pathFiles"]["input"]
    path_output = data["pathFiles"]["output"]
    time_informations = data["timeInformations"]
    modes = data["modes"]
    battery_characteristics = data["batteryCharacteristics"]
    cells_characteristics = data["cellsCharacteristics"]
    satellite_geometry = data["satelliteGeometry"]
    """
    sat_quat_path_out = path_output["output_energy_file"]
    sat_quat_path_out_flag = path_output["output_flag_file"]
    cic = "CIC_MEM_VERS = 1.0"
    origin = (
        "ORIGINATOR = CENSUS / LabEx ESEP - Paris Observatory - PSL Univeristy Paris"
    )
    comment = [
        "COMMENT = Charge of the battery [Wh]",
        "COMMENT = Columns : DATE_MJDday + DATE_MJDseconds + ENERGY + MAX_CAPACITY + MIN_CHARGE",
        "COMMENT = Inputs : Satellite attitude :  "
        + path_input["input_attitude_sat_file"],
        "COMMENT = Inputs : Satellite trajectoire :  "
        + path_input["input_trajectory_intervisibility_file"],
        "COMMENT = Inputs : Start / End :  "
        + str(time_vector[0].mjd)
        + "/"
        + str(time_vector[-1].mjd)
        + "\n",
        "COMMENT = Inputs : Modes files :  " + path_input["input_mode_file"],
        "COMMENT = Inputs : Modes names :  " + modes["modes_names"],
        "COMMENT = Inputs : Modes values :  " + modes["modes_values"],
        "COMMENT = Inputs : Modes average :  " + str(modes["modes_average"]) + "\n",
        "COMMENT = Inputs : Capacity (Wh) :  "
        + str(battery_characteristics["capacity_Wh"])
        + "Wh",
        "COMMENT = Inputs : Capacity (Ah) / Voltage (V) :  "
        + str(battery_characteristics["capacity_Wh"])
        + " Ah "
        + " / "
        + str(battery_characteristics["capacity_Ah"])
        + " V ",
        "COMMENT = Inputs : Initial charge :  "
        + str(battery_characteristics["initial_charge"]),
        "COMMENT = Inputs : Minimum charge :  "
        + str(battery_characteristics["minimum_charge"])
        + "\n",
        "COMMENT = Inputs : Cells material :  " + cells_type,
        "COMMENT = Inputs : Cells surfaces :  " + s,
        "COMMENT = Inputs : Cells thickness :  " + str(thick),
        "COMMENT = Inputs : Material density :  " + str(dens),
        "COMMENT = Inputs : Absorption coefficient :  " + str(abs_c),
        "COMMENT = Inputs : Maximum cells efficiency :  " + str(eff),
    ]
    header = [
        "OBJECT_NAME = CubeSat \n",
        "USER_DEFINED_PROTOCOL = NONE",
        "USER_DEFINED_CONTENT = CHRONOGRAMS",
        "USER_DEFINED_SIZE = 3",
        "USER_DEFINED_TYPE = REAL",
        "USER_DEFINED_UNIT = [n/a] \n",
        "TIME_SYSTEM = UTC",
    ]
    comment_flag = [
        "COMMENT = Flag on the battery charge",
        "COMMENT = Columns : DATE_MJDday + DATE_MJDseconds + COLOR",
        "COMMENT = RED means charge < minimum charge",
        "COMMENT = ORANGE means charge < 1/3*maximum charge + 2/3*minimum charge",
        "COMMENT = YELLOW means charge < 2/3*maximum charge + 1/3*minimum charge",
        "COMMENT = GREEN means charge > 2/3*maximum charge + 1/3*minimum charge",
    ]
    header_flag = [
        "OBJECT_NAME = CubeSat \n",
        "USER_DEFINED_PROTOCOL = NONE",
        "USER_DEFINED_CONTENT = CHRONOGRAMS",
        "USER_DEFINED_SIZE = 3",
        "USER_DEFINED_TYPE = INTEGER",
        " USER_DEFINED_UNIT = [n/a]",
        "TIME_SYSTEM = UTC",
    ]

    sequence = ["%s", "%s", "%s", "%s", "%s"]

    data, flag_data = [], []

    for energy in battery_energy:
        data.append(
            [
                '{:10.3f}'.format(energy.value),
                '{:10.3f}'.format(capacity_wh.value),
                '{:10.3f}'.format((minimum_charge * capacity_wh).value),
            ]
        )

    for flag in charge_info:
        if flag.value < minimum_charge.value:
            flag_data.append(['{:5d}'.format(255), '{:5d}'.format(0), '{:5d}'.format(0)])
        elif flag.value < 1/3+2/3*minimum_charge.value:
            flag_data.append(['{:5d}'.format(255), '{:5d}'.format(140), '{:5d}'.format(30)])
        elif flag.value < 2/3+1/3*minimum_charge.value:
            flag_data.append(['{:5d}'.format(255), '{:5d}'.format(255), '{:5d}'.format(0)])
        else:
            flag_data.append(['{:5d}'.format(0), '{:5d}'.format(255), '{:5d}'.format(0)])
        

    time_vector = time_vector_vts.tolist()
    flag_sorted, time_flag = [flag_data[0]], [time_vector[0]]

    for i in range(1, len(flag_data) - 1):
        if flag_data[i] == flag_data[i - 1]:
            if flag_data[i] != flag_data[i + 1]:
                flag_sorted.append(flag_data[i])
                time_flag.append(time_vector[i])
        elif flag_data[i] != flag_data[i - 1]:
            flag_sorted.append(flag_data[i])
            time_flag.append(time_vector[i])
    flag_sorted.append(flag_data[-1])
    time_flag.append(time_vector[-1])

    # for i in range(len(time_vector)):
    #     time_vector[i][0] = int(time_vector[i][0])
    #     time_vector[i][1] = round(float(time_vector[i][1]), 6)

    # for i in range(len(time_vector)):
    #     for j in range(len(time_vector[i])):
    #         time_vector[i][j] = str(time_vector[i][j])
    # time_vector = np.array(time_vector)

    time_vector = np.array(time_vector)
    a = [[*map("{:.0f}".format, time_vector[:,0])], [*map("{:15.6f}".format, time_vector[:,1])]]
    time_vector = np.array(a).T

    # for i in range(len(time_flag)):
    #     time_flag[i][0] = int(time_flag[i][0])
    #     time_flag[i][1] = round(float(time_flag[i][1]), 6)
    #
    # for i in range(len(time_flag)):
    #     for j in range(len(time_flag[i])):
    #         time_flag[i][j] = str(time_flag[i][j])
    # time_flag = np.array(time_flag)

    time_flag = np.array(time_flag)
    a = [[*map("{:.0f}".format, time_flag[:, 0])], [*map("{:15.6f}".format, time_flag[:, 1])]]
    time_flag = np.array(a).T

    cc.write_ccsds(
        f"{yaml_directory}{sat_quat_path_out}",
        cic,
        origin,
        comment,
        header,
        sequence,
        time_vector,
        data,
        separator="\t",
    )

    cc.write_ccsds(
        f"{yaml_directory}{sat_quat_path_out_flag}",
        cic,
        origin,
        comment_flag,
        header_flag,
        sequence,
        time_flag,
        flag_sorted,
        separator="\t",
    )

    return battery_energy, charge_info
