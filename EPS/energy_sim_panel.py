#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 10:05:25 2019
@author: ccastell
+ 2020-07-29: the Lancelot fix (eclipses were not computed in last simulated day)
"""


from math import *
from astropy.time import Time
from astropy.time import TimeDelta
from astropy.coordinates import CartesianRepresentation, CartesianDifferential
from astropy.coordinates import spherical_to_cartesian, cartesian_to_spherical
from astropy.coordinates import GCRS
from astropy import units as u
import matplotlib.pyplot as plt
import numpy as np
import sys
import ast
from pyquaternion import Quaternion
from time import *
from tqdm import tqdm
import threading as thr
from mkit import sph2cart as sph2cart_mkit
from docks_helper import arange, mjd1_to_mjd2

import euler2quat as e2q
import quat_change as qc
import time_function as tt
import ccsds as cc
import pickle as pck
from pathlib import Path

import intervisibility as inter
import interpolation as ip

sys.path.append("../python_wrapper")
import Wrapper
DEBUG = Wrapper.DEBUG
DEBUG_PATH = '../Output/'

# ________________________CELLS_DATABASE_IMPORT___________________________________
# _____________________________________________________________________________
path_file = Path(__file__)
cell_base_path = path_file.parent / Path("config_files/cells_database.txt")

cells_database = open(cell_base_path, "r")

# _____________________________TIME_FORMAT_____________________________________
# _____________________________________________________________________________


def mjd_to_vts(time):
    mjd_integer_part = int(time) // 1
    mjd_decimal_part = TimeDelta((time % 1), format="jd").sec
    return mjd_integer_part, mjd_decimal_part


def vts_to_mjd(time):  # time is a tuple
    mjd_hms_f = strftime("%H %M %S", gmtime(time[1])).replace(" ", ":")
    mjd_integer_iso = Time(time[0], format="mjd").iso
    mjd_time = mjd_integer_iso[: mjd_integer_iso.index(" ")]
    mjd_time = Time(mjd_time + " " + mjd_hms_f, format="iso").mjd
    return mjd_time


# _____________________________DATA_CELLS______________________________________
# _____________________________________________________________________________

# Reading the solar cells database
lines = cells_database.readlines()
for i in range(len(lines)):
    lines[i] = lines[i].replace("\n", "")
    lines[i] = lines[i].replace(" ", "\t")
    while "\t\t" in lines[i]:
        lines[i] = lines[i].replace("\t\t", "\t")
    lines[i] = lines[i].split("\t")

# Reading the configuration file


def data_read(data):
    cells_characteristics = data["cellsCharacteristics"]
    satellite_geometry = data["satelliteGeometry"]
    cells_type = cells_characteristics["cells_type"].lower()
    if cells_type != None:
        index = lines[0].index(cells_type)

    absorption_coeff = cells_characteristics["absorption_coefficient"]

    if absorption_coeff == None:
        absorption_coeff = float(lines[2][index]) * u.dimensionless_unscaled
    else:
        absorption_coeff = float(absorption_coeff) * u.dimensionless_unscaled

    max_efficiency, coeff_efficiency = (
        cells_characteristics["maximum_efficiency"],
        cells_characteristics["efficiency_coefficient"],
    )
    if max_efficiency == None and coeff_efficiency == None:
        max_efficiency = float(lines[3][index]) * u.percent
        coeff_efficiency = float(lines[4][index]) * u.percent / u.deg_C
    else:
        if float(max_efficiency) > 1:  # the efficiency can be % or between 0 & 1
            max_efficiency, coeff_efficiency = (
                float(max_efficiency) * u.percent,
                float(coeff_efficiency) * u.percent / u.deg_C,
            )
        elif float(max_efficiency) <= 1:
            max_efficiency, coeff_efficiency = (
                float(max_efficiency) * 100
            ) * u.percent, (float(coeff_efficiency) * 100) * u.percent / u.deg_C

    surface = ast.literal_eval(cells_characteristics["surface"])

    if type(surface) == tuple:
        surface_cells = list(surface)
    elif type(surface) == float:
        surface_cells = [surface] * len(
            ast.literal_eval(cells_characteristics["panel_position"][0])
        )
    surface_cells_unit = cells_characteristics["surface_unity"]
    if surface == None:
        surface_cells = [float(lines[1][index])] * len(
            ast.literal_eval(cells_characteristics["panel_position"][0])
        )
    else:
        if surface_cells_unit.lower() == "m2":
            surface_cells = surface_cells * u.m ** 2
        elif surface_cells_unit.lower() == "cm2":
            surface_cells = (surface_cells * u.cm ** 2).to(u.m ** 2)
        elif surface_cells_unit.lower() == "mm2":
            surface_cells = (surface_cells * u.mm ** 2).to(u.m ** 2)

    thickness = cells_characteristics["thickness"]
    if thickness == None:
        thickness = lines[7][index] * u.cm
    else:
        thickness = float(thickness) * u.cm

    density = cells_characteristics["thickness"]
    if density == None:
        density = lines[6][index] * (u.g) / (u.cm ** 3)
    else:
        density = float(density) * u.g / u.cm ** 3

    conductivity, c = [], cells_characteristics["thermal_capacity"]

    if c == "" or c == None:
        cond = lines[5][index] * (u.J) / (u.deg_C * u.g)
        for sc in surface_cells:
            conductivity.append((sc * thickness * density * cond).to(u.J / u.deg_C))
    else:
        for sc in surface_cells:
            conductivity.append(
                ((float(c) * (u.J) / (u.deg_C * u.g)) * thickness * density * sc).to(
                    u.J / u.deg_C
                )
            )

    panels_position = satellite_geometry["panel_position"]

    panels_seq = satellite_geometry["sequences"]
    a_b_g, q_element, sequence = [], [], []

    for angle, seq in zip(panels_position, panels_seq):

        a_b_g.append(ast.literal_eval(angle))
        sequence.append(ast.literal_eval(seq))
    a_b_g = np.array(a_b_g[0])
    alpha_beta_gamma = []
    for a in a_b_g:
        alpha_beta_gamma.append(
            (a[0] * np.pi / 180, a[1] * np.pi / 180, a[2] * np.pi / 180)
        )

    sequence = np.array(sequence[0])
    for i in range(len(alpha_beta_gamma)):
        q_element.append(
            Quaternion(
                e2q.euler_2_quat(
                    alpha_beta_gamma[i], beta=None, gamma=None, seq=str(sequence[i])
                )
            )
        )

    return (
        cells_type,
        surface_cells,
        thickness,
        density,
        conductivity,
        absorption_coeff,
        max_efficiency,
        coeff_efficiency,
        q_element,
        sequence,
    )


# ___________________________INTERVISIBILITY_FILE______________________________
# _____________________________________________________________________________


def data_files(data, yaml_directory):
    path_input = data["pathFiles"]["input"]
    path_output = data["pathFiles"]["output"]

    orbit_sat_file = (
        f'{yaml_directory}{path_input["input_trajectory_intervisibility_file"]}'
    )
    attitude_sat_file = f'{yaml_directory}{path_input["input_attitude_sat_file"]}'
    input_eclipse_file_EPS = f'{yaml_directory}{path_input["input_eclipse_file_EPS"]}'
    mode_file = f'{yaml_directory}{path_input["input_mode_file"]}'

    output_eclipse_file = (
        f'{yaml_directory}{path_output["output_eclipse_file_vts"]}',
        f'{yaml_directory}{path_output["output_eclipse_file_EPS"]}',
    )

    output_angles_file = f'{yaml_directory}{path_output["output_incident_angle_file"]}'

    return (
        orbit_sat_file,
        attitude_sat_file,
        input_eclipse_file_EPS,
        output_eclipse_file,
        mode_file,
        output_angles_file,
    )


# _________________________separator_reading____________________________________


def separator_reading(data, yaml_directory):  # determines if sep="\t" or sep=" "

    orbit_sat_file, attitude_sat_file = (
        data_files(data, yaml_directory)[0],
        data_files(data, yaml_directory)[1],
    )

    traj, att = open(orbit_sat_file, "r"), open(attitude_sat_file, "r")
    text_traj, text_att = traj.readlines(), att.readlines()
    text_traj, text_att = np.array(text_traj), np.array(text_att)
    traj.close(), att.close()

    for i in range(text_traj.size):
        text_traj[i] = text_traj[i].replace("\n", "")
    for j in range(text_att.size):
        text_att[j] = text_att[j].replace("\n", "")
    for i in range(text_traj.size):
        if text_traj[i] != "":
            line_t = text_traj[i]
    for j in range(text_att.size):
        if text_att[j] != "":
            line_a = text_att[j]

    sep = ["\t", " "]
    for i in range(len(sep)):
        if sep[i] in line_t:
            index_t = line_t.index(sep[i])
            x = 0
            for caracter in line_t[index_t:]:
                if caracter == sep[i]:
                    x = x + 1
                elif caracter != sep[i]:
                    break
                separator_traj = sep[i] * x
        elif sep[0] not in line_t and sep[1] not in line_t:
            raise ValueError("The separator of the trajectory file is not recognized")

    for i in range(len(sep)):
        if sep[i] in line_a:
            index_a = line_a.index(sep[i])
            x = 0
            for caracter in line_a[index_a:]:
                if caracter == sep[i]:
                    x = x + 1
                elif caracter != sep[i]:
                    break
                separator_att = sep[i] * x
        elif sep[0] not in line_a and sep[1] not in line_a:
            raise ValueError("The separator of the attitude file is not recognized")

    return separator_traj, separator_att


# ____________________________end_separator_reading_____________________________


def compute_eclipse(data, yaml_directory):
    time_informations = data["timeInformations"]
    (
        orbit_sat_file,
        attitude_sat_file,
        input_eclipse_file_EPS,
        output_eclipse,
        mode_file,
        output_angles,
    ) = data_files(data, yaml_directory)
    separator_traj, separator_att = separator_reading(data, yaml_directory)
    time_format, start_simulation, end_simulation = (
        time_informations["time_format"],
        time_informations["start"],
        time_informations["end"],
    )



    # read sat orbit  ( in gcrs frame or local icrs earth center)
    cic, origin, comment, header, data_time1, data_science = cc.read_ccsds(
        orbit_sat_file
    )
    # convert CIC time to astropy time
    time_vector_traj = Time(tt.vector_julian_to_julian(data_time1), format="mjd")

    """
    limiting the time to the period of simulation
    DOES ONLY WORK IF TIME LIMITS ARE GIVEN
    """
    if start_simulation != None and end_simulation != None:
        simIni = Time(-1 + round(float(start_simulation)), format=time_format, scale="utc")
        simFin = Time(1 + round(float(end_simulation)), format=time_format, scale="utc")
        index_start = 0
        index_end = len(time_vector_traj)
        for i in range(len(time_vector_traj)):
            if time_vector_traj[i] >= simIni:
                index_start = i
                break
        for i in range(len(time_vector_traj)):
            if time_vector_traj[i] >= simFin:
                index_end = i
                break
        time_vector_traj = time_vector_traj[index_start:index_end]

    # convert sat orbit in astropy GCRS position
    x_sat = data_science.T[0].tolist() * u.km

    y_sat = data_science.T[1].tolist() * u.km

    z_sat = data_science.T[2].tolist() * u.km

    if start_simulation != None and end_simulation != None:
        x_sat = x_sat[index_start:index_end]
        y_sat = y_sat[index_start:index_end]
        z_sat = z_sat[index_start:index_end]
    sat_gcrs = GCRS(
        obstime=time_vector_traj,
        x=x_sat,
        y=y_sat,
        z=z_sat,
        representation_type=CartesianRepresentation,
        differential_type=CartesianDifferential,
    )

    # If the user gives his eclipse file:
    """print("--- len(time_vector_traj) ---")
    print(len(time_vector_traj))
    print(time_vector_traj[0])
    print(time_vector_traj[-1])
    print("--- ok? ---")""" 
    ''
    if input_eclipse_file_EPS.split("/")[-1] == "None":
        input_eclipse_file_EPS = None

    if (
            output_eclipse[1].split("/")[-1] == "None"
    ):
        output_eclipse = (None, None)
    elif output_eclipse[0].split("/")[-1] == "None" :
        output_eclipse = (None, output_eclipse[1])
    if input_eclipse_file_EPS != None:

        # time_event_sun, event_type_sun, sun_eph_sat_sky, sun_inter_sat = inter.sun_intervisibility(time_vector_traj, sat_gcrs, plot=0, interpolation_pos_input="none",event_duration=120,event_precision=120)
        with open(input_eclipse_file_EPS, "rb") as file:
            sun_eph_sat_sky = pck.load(file)
            sun_inter_sat = pck.load(file)

            time_vector_traj_new = pck.load(file)
           
    elif output_eclipse[1] != None:
        event_duration = round(
            (float(str(time_vector_traj[1] - time_vector_traj[0]))) * 86400
        )
        (
            time_event_sun,
            event_type_sun,
            sun_eph_sat_sky,
            sun_inter_sat,
            time_vector_traj_new,
        ) = inter.sun_intervisibility(
            time_vector_traj,
            sat_gcrs,
            plot=0,
            interpolation_pos_input=[],
            event_duration=event_duration,
            event_precision=event_duration,
        )
        if output_eclipse[0] != None :
            inter.cic_intervisibility(
                time_event_sun, event_type_sun, "EPS config", output_eclipse[0]
            )
        inter.eps_output(output_eclipse[1], sun_eph_sat_sky, sun_inter_sat, time_vector_traj_new)

    else:

        print(
            "You need either the input eclipse file or either the output eclipse file for EPS!"
        )
        sys.exit()
    right_asc = sun_eph_sat_sky.ra
    declinaison = sun_eph_sat_sky.dec
    distance_to_the_sun = sun_eph_sat_sky.distance
    inter_value = sun_inter_sat
    cartesian_coordinates = []



    
    cartesian_coordinates = np.transpose(
        spherical_to_cartesian(distance_to_the_sun, declinaison, right_asc)
    )
    

    # read sat attitude (in quaternions)
    cic2, origin2, comment2, header2, data_time2, data_science2 = cc.read_ccsds(
        attitude_sat_file
    )
    # convert CIC time to astropy time
    time_vector_att = Time(tt.vector_julian_to_julian(data_time2), format="mjd")

    q_sat0 = []
    
    
    print("compute eclipse")
    progress_bar = tqdm(total=4)
    
    
    for i in range(len(data_science2)):
        q_sat0.append(
            Quaternion(data_science2[i].tolist())
        )  # q_sat: list with the satellite's attitude values

    object_position0 = []
    for i in range(len(cartesian_coordinates)):
        object_position0.append(
            (
                cartesian_coordinates[i][0],
                cartesian_coordinates[i][1],
                cartesian_coordinates[i][2],
            )
        )
    
    progress_bar.update(1)
    
    # Apply the simulation time
    # TODO: check functionality if trajectory timeline has smaller sampling rate than quaternion timeline (otherwise checked)
    if len(time_vector_traj) != len(object_position0) :
        time_vector_traj = time_vector_traj_new
    result = ip.interpolation(
        time_vector_traj, time_vector_att, object_position0, q_sat0
    )

    object_position, q_sat, time_vector = result[0], result[1], result[2]

    # print(np.shape(object_position)) # = (22129,3)
    # print(len(q_sat))                # = 22129
    ##ip.interpolation are wrong => distance_to_the_sun = ip.interpolation(time_vector_traj, time_vector_att, distance_to_the_sun, q_sat0)[0][0].tolist()*u.km
    # print(len(time_vector))          # = 22129
    # print(time_vector[0])
    # print(len(time_vector_traj))     # = 720
    # print(time_vector_traj[0])
    # print(len(distance_to_the_sun))  # = 720
    distance_to_the_sun = (
        ip.interp_intervalue(time_vector, time_vector_traj, distance_to_the_sun)[
            0
        ].tolist()
        * u.km
    )
    # print(len(distance_to_the_sun))  # = 22129
    # print(len(time_vector))          # = 22129
    # print(len(time_vector_traj))     # = 720
    # print(len(inter_value))          # = 720
    inter_value = ip.interp_intervalue(time_vector, time_vector_traj, inter_value)[
        0
    ].tolist()
    # print(len(inter_value))          # = 22129

    progress_bar.update(1)

    if mode_file != None:
        file = open(mode_file, "r")
        text = file.readlines()
        text = np.array(text)
        file.close()
        for i in range(text.size):
            text[i] = text[i].replace("\n", "")
        for i in range(text.size):
            if text[i] == "META_STOP":
                index_start = i
        start_modes = text[index_start + 2].replace(" ", "\t")
        while "\t\t" in start_modes:
            start_modes = start_modes.replace("\t\t", "\t")
        start_modes = start_modes.split("\t")
        mj_s, sec_s = ast.literal_eval(start_modes[0]), ast.literal_eval(start_modes[1])
        start_modes = Time(vts_to_mjd((mj_s, sec_s)), format="mjd")

        for elt in text[::-1]:
            if elt != "":
                end_modes = elt.replace(" ", "\t")
                break
        while "\t\t" in end_modes:
            end_modes = end_modes.replace("\t\t", "\t")
        end_modes = end_modes.split("\t")
        mj_e, sec_e = ast.literal_eval(end_modes[0]), ast.literal_eval(end_modes[1])
        end_modes = Time(vts_to_mjd((mj_e, sec_e)), format="mjd")

    else:
        start_modes = time_vector[0]
        end_modes = time_vector[-1]

    if start_simulation == None and end_simulation == None:
        start_simulation = max(start_modes, time_vector[0])
        end_simulation = min(end_modes, time_vector[-1])

    else:
        if time_format.lower() == "mjd":
            start_simulation = Time(float(start_simulation), format="mjd", scale="utc")
            end_simulation = Time(float(end_simulation), format="mjd", scale="utc")
        elif time_format.lower() == "iso":
            start_simulation = Time(float(start_simulation), format="iso", scale="utc")
            end_simulation = Time(float(end_simulation), format="iso", scale="utc")
        elif time_format.lower() == "jd":
            start_simulation = Time(float(start_simulation), format="jd", scale="utc")
            end_simulation = Time(float(end_simulation), format="jd", scale="utc")
        if start_simulation < max(start_modes, time_vector[0]) or end_simulation > min(
            end_modes, time_vector[-1]
        ):
            raise ValueError("Wrong simulation interval, bigger than the values")

    for i in range(len(time_vector)):
        if time_vector[i] >= start_simulation:
            index_start = i
            break
    for i in range(len(time_vector)):
        if time_vector[i] >= end_simulation:
            index_end = i
            break

    progress_bar.update(1)


    q_sat = q_sat[index_start:index_end]
    distance_to_the_sun = distance_to_the_sun[index_start:index_end]
    inter_value = inter_value[index_start:index_end]
    time_vector = time_vector[index_start:index_end]
    object_position = object_position[index_start:index_end]
    step_sec, step_days = (
        time_vector[-1].datetime - time_vector[0].datetime
    ).seconds, (time_vector[-1].datetime - time_vector[0].datetime).days
    if step_days == 0:
        length = (step_sec * u.s).to(u.h)
        step_values = length / len(time_vector)
    elif step_days != 0:
        length = (step_days * u.d).to(u.h) + (step_sec * u.s).to(u.h)
        step_values = length / len(time_vector)
    step_time = [0 * u.h]
    for i in range(1, len(time_vector)):
        step_time.append(step_time[i - 1] + step_values)

    progress_bar.update(1)
    progress_bar.close()

    return (
        q_sat,
        object_position,
        time_vector,
        step_time,
        distance_to_the_sun,
        inter_value,
    )


# _____________________________________________________________________________
# _____________________________________________________________________________


# ____________________________ANGLE_CALCULATION________________________________
# _____________________________________________________________________________


def calculate_ray_incident_angle(data, yaml_directory):

    q_element = data_read(data)[8]
    orbit_sat_file, attitude_sat_file, output_angles = (
        data_files(data, yaml_directory)[0],
        data_files(data, yaml_directory)[1],
        data_files(data, yaml_directory)[5],
    )

    (
        q_sat,
        object_position,
        time_vector,
        step_time,
        distance_to_the_sun,
        inter_value,
    ) = compute_eclipse(data, yaml_directory)

    cic = "CIC_MEM_VERS = 1.0"
    origin = (
        "ORIGINATOR = CENSUS / LabEx ESEP - Paris Observatory - PSL Univeristy Paris"
    )
    comment = [
        "COMMENT = Solar incident angles in degree",
        "COMMENT = Columns : DATE MJDday + MJDseconds + PANELS  ",
        "COMMENT = Inputs : Satellite attitude :  " + attitude_sat_file,
        "COMMENT = Inputs : Satellite trajectoire :  " + orbit_sat_file,
    ]
    header = [
        "OBJECT_NAME = CubeSat",
        "OBJECT_ID = CubeSat \n",
        "USER_DEFINED_PROTOCOL = NONE",
        "USER_DEFINED_CONTENT = CHRONOGRAMS",
        "USER_DEFINED_SIZE = " + str(len(q_element)),
        "USER_DEFINED_TYPE = REAL",
        "USER_DEFINED_UNIT = [deg] \n",
        "TIME_SYSTEM = UTC",
    ]

    sequence = ["%d", "%15.6f"]
    for i in range(len(q_element)):
        sequence.append("%7.1f")
    data_time = []
    for i in range(len(time_vector)):
        data_time.append(mjd_to_vts(time_vector[i].value))
    data_time = np.array(data_time)

    results = qc.quat_time(q_sat, q_element, object_position)
    # data_angles = results[2]
    data_angles = results[0]
    ##angles = qc.quat_time(q_sat, q_element, object_position)[3]
    # angles = results[3]
    angles = results[1]

    print("compute angles...")

    for values in angles:
        for i in range(len(values)):
            if values[i] < 0:
                # bug => values[i] = np.pi/2
                values[i] = 0

    if output_angles != None:
        # print("(writing angles)")
        cc.write_ccsds(
            output_angles,
            cic,
            origin,
            comment,
            header,
            sequence,
            data_time,
            data_angles,
            separator="\t",
        )

    return angles, time_vector, step_time, data_time, distance_to_the_sun, inter_value


# ______________________________POWER_PROGRAM__________________________________
# _____________________________________________________________________________

# adjustment parameters:
temp_sun = 5800 * u.K
radius_sun = 695600 * u.km
sigma = 5.67e-8 * u.W / (u.m ** 2 * u.K ** 4)


def evaluate_incident_power(ray_angle, distance_to_the_sun, inter_value, surface_cells):

    solar_flux = (sigma * temp_sun ** 4) * (radius_sun / distance_to_the_sun) ** 2

    incident_power = []
    for i in range(len(ray_angle)):
        # bug => incident_power.append((surface_cells[i] * solar_flux * cos(ray_angle[i]) * inter_value).value)
        incident_power.append(
            (surface_cells[i] * solar_flux * sin(ray_angle[i]) * inter_value).value
        )

    return incident_power


def evaluate_incident_power_t(data, yaml_directory):
    """
    This function computes the incident power on solar cells

    :param distance_to_the_sun: (list, size:number of steps)
    :param inter_value: (list, size:number of steps)
    :param sigma: (float) solar constant
    :param radius_sun: (float)
    :param temp_sun: (float)

    :return: incident power: (list, size:number of panels)
             time_vector: (list MJD format)

    """

    surface_cells = data_read(data)[1]

    (
        ray_incident_angle,
        time_vector,
        step_time,
        data_time,
        distance_to_the_sun,
        inter_value,
    ) = calculate_ray_incident_angle(data, yaml_directory)
    
    print("compute incident power")
    
    incident_power = []
    for angle, dist, interv in tqdm(zip(
        ray_incident_angle, distance_to_the_sun, inter_value),total=len(ray_incident_angle)):
        incident_power.append(
            evaluate_incident_power(angle, dist, interv, surface_cells)
        )

    incident_power = np.array(incident_power)
    for power in incident_power:
        power[power < 0] = 0


    if DEBUG == 1:
        cic = "CIC_MEM_VERS = 1.0"
        origin = "ORIGINATOR = CENSUS / LabEx ESEP - Paris Observatory - PSL Univeristy Paris"
        comment = [
            "COMMENT = Inter_Value (DEBUG)",
            "COMMENT = Columns : DATE MJDday + MJDseconds + PANELS  ",
        ]
        header = [
            "OBJECT_NAME = CubeSat",
            "OBJECT_ID = CubeSat \n",
            "USER_DEFINED_PROTOCOL = NONE",
            "USER_DEFINED_CONTENT = CHRONOGRAMS",
            "USER_DEFINED_SIZE = 1",
            "USER_DEFINED_TYPE = REAL",
            "USER_DEFINED_UNIT = [n/a] \n",
            "TIME_SYSTEM = UTC",
        ]
        sequence = ["%d", "%.3f"]
        sequence.append("%.3f")
        data_time = []
        for i in range(len(inter_value)):
            data_time.append(mjd_to_vts(time_vector[i].value))
        data_time = np.array(data_time)
        inter_value = np.reshape(np.array(inter_value), (len(inter_value), 1))
        print("(writing inter_values)")
        cc.write_ccsds(
            DEBUG_PATH + "inter_values.txt",
            cic,
            origin,
            comment,
            header,
            sequence,
            data_time,
            inter_value,
            separator="\t",
        )

    return incident_power, time_vector, step_time, data_time


# ______________________________________________________________________________

# adjustment parameters:
adj_time = 60 * u.s
temp_rate1 = 0.127 * u.K / u.s
sigma = 5.67e-8 * u.W / (u.m ** 2 * u.K ** 4)
v_cooling_vacuum = -0.0198 * u.K / u.s
# (doc min temp = -150°C), wrong => min_t = 100*u.K
min_t = (273.15 - 150) * u.K


def evaluate_cells_temperature(
    temp_t0, power_t0, power_t1, timestep, absorption_coeff, surface_cells, conductivity
):


    # temperatures.append(evaluate_cells_temperature(temp_t0, power_t0, power_t1, timestep, absorption_coeff, surface_cells, conductivity))


    cond_k = []

    for cond in conductivity:
        cond_k.append(cond * 1 * u.deg_C / u.K)

    temperature = []


   
    for i in range(len(power_t0)):
        
        absorbed_power = power_t1[i] * u.W * absorption_coeff

        # dirty => if power_t0[i] > 1e-3 and power_t1[i] > 1e-3:
        

        if power_t0[i] > 1 and power_t1[i] > 1:
            # was and remains illuminated

            dtemp = (
                (absorbed_power * timestep / conductivity[i]).to(
                    u.K, equivalencies=u.temperature()
                )
            ) - temp_t0[i]
            if abs(dtemp / timestep) > temp_rate1:
                dtemp = temp_rate1 * timestep

        # elif round(power_t0[i],1) == 0.0 and round(power_t1[i],1) == 0.0 :
        elif power_t0[i] < 1 and power_t1[i] < 1:
            # was and remains shadowed
            dtemp = v_cooling_vacuum * timestep

        # elif power_t1[i] > 1e-3 and round(power_t0[i],1) == 0.0:
        elif power_t0[i] < 1 and power_t1[i] > 1:
            # was shadowed and becomes illuminated

            dtemp = ((absorbed_power * timestep) / conductivity[i]).to(
                u.K, equivalencies=u.temperature()
            ) - temp_t0[i]

            if abs(dtemp / timestep) > temp_rate1:
                dtemp = temp_rate1 * timestep

        else:
            # power_t0[i] > 1 and power_t1[i] < 1
            # was illuminated and becomes shadowed
            dtemp = v_cooling_vacuum * timestep
        
        

        # t_eq_sup = ((power_t0[i]*u.W * absorption_coeff)/(sigma * surface_cells[i]))**(1/4)
        
        t_eq_sup = (
            (power_t1[i] * u.W * absorption_coeff) / (sigma * surface_cells[i])
        ) ** (1 / 4)
  
        # if power_t0[i] > 1e-3 and power_t1[i] > 1e-3 and (temp_t0[i] + dtemp) > t_eq_sup :
        if power_t0[i] > 1 and power_t1[i] > 1 and (temp_t0[i] + dtemp) > t_eq_sup:
            temperature.append(t_eq_sup)
        # elif round(power_t0[i],1) == round(power_t1[i],1) == 0.0 and (temp_t0[i] + dtemp) < min_t:
        elif abs(power_t0[i] - power_t1[i]) < 1 and (temp_t0[i] + dtemp) < min_t:
            temperature.append(min_t)
        else:
            temperature.append(temp_t0[i] + dtemp)


    return temperature


def evaluate_cells_temperature_t(data, yaml_directory):
    """
    This function computes the cells temperature

    :param conductivity: (float)
    :param absorption_coeff: (float)
    :param sigma: (float)
    :param surface_cells: (float)
    :param distance_to_the_sun: (list of float)
    :param radius_sun: (float)
    :param temp_sun: (float)
    :param inter_value: (float)

    :return: temperatures: (list, size: number of panels) temperature in kelvin for each panel at each step
             time_vector: (list, size:number of steps)
    """

    incident_power, time_vector, step_time, data_time = evaluate_incident_power_t(
        data, yaml_directory
    )

    surface_cells, conductivity, absorption_coeff = (
        data_read(data)[1],
        data_read(data)[4],
        data_read(data)[5],
    )

    temperatures = [[]]
    for i in range(len(incident_power[0])):
        # if incident_power[0][i] > 0:
        #    temperatures[0].append((incident_power[0][i] * u.W * 1 * u.s / conductivity[i]).to(u.K, equivalencies=u.temperature()))
        # else:
        temperatures[0].append(273 * u.K)
        
    print("\nCompute cells temperature")

    for i in tqdm(range(1, len(incident_power))):
        
        power_t0 = incident_power[i - 1]

        power_t1 = incident_power[i]
        temp_t0 = temperatures[i - 1]
      
        timestep = (step_time[i] - step_time[i - 1]).to(u.s)

        temperatures.append(
            evaluate_cells_temperature(
                temp_t0,
                power_t0,
                power_t1,
                timestep,
                absorption_coeff,
                surface_cells,
                conductivity,
            )
        )

    for i in range(len(temperatures)):
        for j in range(len(temperatures[i])):
            temperatures[i][j] = temperatures[i][j].value

    if DEBUG == 1:
        cic = "CIC_MEM_VERS = 1.0"
        origin = "ORIGINATOR = CENSUS / LabEx ESEP - Paris Observatory - PSL Univeristy Paris"
        comment = [
            "COMMENT = Cells temperatures",
            "COMMENT = Columns : DATE MJDday + MJDseconds + PANELS  ",
        ]
        header = [
            "OBJECT_NAME = CubeSat",
            "OBJECT_ID = CubeSat \n",
            "USER_DEFINED_PROTOCOL = NONE",
            "USER_DEFINED_CONTENT = CHRONOGRAMS",
            "USER_DEFINED_SIZE = " + str(len(incident_power[0])),
            "USER_DEFINED_TYPE = REAL",
            "USER_DEFINED_UNIT = [n/a] \n",
            "TIME_SYSTEM = UTC",
        ]
        sequence = ["%d", "%.3f"]
        for i in range(len(incident_power[0])):
            sequence.append("%.2f")
        data_time = []
        for i in range(len(time_vector)):
            data_time.append(mjd_to_vts(time_vector[i].value))
        data_time = np.array(data_time)
        print("(writing temperatures)")
        cc.write_ccsds(
            DEBUG_PATH + "temperatures.txt",
            cic,
            origin,
            comment,
            header,
            sequence,
            data_time,
            temperatures,
            separator="\t",
        )
        comment = [
            "COMMENT = Incident Power, including sin(incidence)",
            "COMMENT = Columns : DATE MJDday + MJDseconds + PANELS  ",
        ]
        print("(writing incident power)")
        cc.write_ccsds(
            DEBUG_PATH + "incident_pwr.txt",
            cic,
            origin,
            comment,
            header,
            sequence,
            data_time,
            incident_power,
            separator="\t",
        )

    return temperatures, incident_power, time_vector, step_time, data_time


# ______________________________________________________________________________


def evaluate_cells_efficiency(data, yaml_directory):
    """
    This function computes the cells efficiency

    :param max_efficiency: (float)
    :param coeff_efficiency: (float)

    :return: cells_efficiency: (list, size: number of panels) efficiency for each panel at each step
    """

    max_efficiency, coeff_efficiency = data_read(data)[6], data_read(data)[7]

    (
        temperatures,
        incident_power,
        time_vector,
        step_time,
        data_time,
    ) = evaluate_cells_temperature_t(data, yaml_directory)

    cells_efficiency_inter = []

    print("Compute cells efficiency")
    for i in tqdm(range(len(temperatures))):
        for j in range(len(temperatures[i])):
            e = max_efficiency - coeff_efficiency * (temperatures[i][j] - 273) * u.deg_C
            cells_efficiency_inter.append(e)
    cells_efficiency = list(zip(*[iter(cells_efficiency_inter)] * len(temperatures[0])))

    if DEBUG == 1:
        cic = "CIC_MEM_VERS = 1.0"
        origin = "ORIGINATOR = CENSUS / LabEx ESEP - Paris Observatory - PSL Univeristy Paris"
        comment = [
            "COMMENT = Cells efficiency",
            "COMMENT = Columns : DATE MJDday + MJDseconds + PANELS  ",
        ]
        header = [
            "OBJECT_NAME = CubeSat",
            "OBJECT_ID = CubeSat \n",
            "USER_DEFINED_PROTOCOL = NONE",
            "USER_DEFINED_CONTENT = CHRONOGRAMS",
            "USER_DEFINED_SIZE = " + str(len(incident_power[0])),
            "USER_DEFINED_TYPE = REAL",
            "USER_DEFINED_UNIT = [n/a] \n",
            "TIME_SYSTEM = UTC",
        ]
        sequence = ["%d", "%.3f"]
        for i in range(len(incident_power[0])):
            sequence.append("%.2f")
        data_time = []
        for i in range(len(time_vector)):
            data_time.append(mjd_to_vts(time_vector[i].value))
        data_time = np.array(data_time)
        print("(cells efficiency)")
        cc.write_ccsds(
            DEBUG_PATH + "cells_efficiency.txt",
            cic,
            origin,
            comment,
            header,
            sequence,
            data_time,
            cells_efficiency,
            separator="\t",
        )

    return cells_efficiency, incident_power, time_vector, step_time, data_time


# ______________________________________________________________________________

# adjustments parameters:

cells_age = 0
first_level = 3600
second_level = 50400
first_coeff = 1
second_coeff = 1


def characterize_cells_ageing(data):

    cells_ageing = []
    #    for i in range(len(step_time)):
    #        if (step_time[i].to(u.min)).value + cells_age < first_level:
    #            x = first_coeff
    #            cells_ageing.append(x)
    #        elif first_level <= (step_time[i].to(u.min)).value + cells_age < second_level:
    #            x = second_coeff
    #            cells_ageing.append(x)

    return cells_ageing


# ______________________________________________________________________________


def evaluate_produced_power(data, yaml_directory):
    """
    This function computes the produced power

    :return: sum_produced_power: (list, size:number of steps) total of the produced power for all panels at each step
    """

    (
        efficiency,
        incident_power,
        time_vector,
        step_time,
        data_time,
    ) = evaluate_cells_efficiency(data, yaml_directory)

    cells_ageing = characterize_cells_ageing(data)

    produced_power = incident_power * efficiency

    print("Compute produced power")
    sum_produced_power = []
    for i in tqdm(range(len(produced_power))):
        sum_produced_power.append((sum(produced_power[i])) * u.W)

    if DEBUG == 1:
        cic = "CIC_MEM_VERS = 1.0"
        origin = "ORIGINATOR = CENSUS / LabEx ESEP - Paris Observatory - PSL Univeristy Paris"
        comment = [
            "COMMENT = Produced power per cell",
            "COMMENT = Columns : DATE MJDday + MJDseconds + PANELS  ",
        ]
        header = [
            "OBJECT_NAME = CubeSat",
            "OBJECT_ID = CubeSat \n",
            "USER_DEFINED_PROTOCOL = NONE",
            "USER_DEFINED_CONTENT = CHRONOGRAMS",
            "USER_DEFINED_SIZE = " + str(len(incident_power[0])),
            "USER_DEFINED_TYPE = REAL",
            "USER_DEFINED_UNIT = [n/a] \n",
            "TIME_SYSTEM = UTC",
        ]
        sequence = ["%d", "%.3f"]
        for i in range(len(incident_power[0])):
            sequence.append("%.2f")
        data_time = []
        for i in range(len(time_vector)):
            data_time.append(mjd_to_vts(time_vector[i].value))
        data_time = np.array(data_time)
        print("(writing produced power per cell)")
        cc.write_ccsds(
            DEBUG_PATH + "prod_power.txt",
            cic,
            origin,
            comment,
            header,
            sequence,
            data_time,
            produced_power,
            separator="\t",
        )

    return sum_produced_power, time_vector, step_time, data_time


"""
CALLs SEQUENCE:
EPS.py: EPS_simulation() => esb.evaluate_battery_charge
energy_sim_battery.py: evaluate_battery_charge => esp.evaluate_produced_power
energy_sim_panel.py: evaluate_produced_power
=> evaluate_cells_efficiency
   => evaluate_cells_temperature_t
      => evaluate_incident_power_t
	     => calculate_ray_incident_angle (write POWER_angles)
		    => compute_eclipse => cc.read_ccsds
			=> qc.quat_time
			   quat_change.py: quat_time => quat_unit
			=> cc.write_ccsds
=> characterize_cells_ageing

"""
