#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 10:19:13 2019

@author: ccastell
"""

from statistics import mean
import numpy as np
from math import *
from astropy.time import Time
from pyquaternion import Quaternion
import sys

sys.path.append("../docks_tools/")


def time_format(time_vector, time_vector2):

    # Converts time to MJD format

    if time_vector.format == "mjd":
        time_vector = time_vector.value
    elif (
        time_vector.format == "jd"
        or time_vector.format == "iso"
        or time_vector.format == "isot"
    ):
        time_vector = time_vector.mjd
    else:
        raise ValueError("time_list can only be in mjd, jd, iso or isot format")
    if time_vector2.format == "mjd":
        time_vector2 = time_vector2.value
    elif (
        time_vector2.format == "jd"
        or time_vector2.format == "iso"
        or time_vector2.format == "isot"
    ):
        time_vector2 = time_vector2.mjd
    else:
        raise ValueError("time_list can only be in mjd, jd, iso or isot format")
    return time_vector, time_vector2


def interpolation(time_vector, time_vector2, values, values2):
    """
    synchronisation and interpolation of trajectory (time_vector, values) and quaternion (time_vector2, values2) time lines
    TODO: check functionality if trajectory timeline has smaller sampling rate than quaternion timeline (otherwise checked)
    """

    # Converts both time vectors to to MJD format
    time_vector, time_vector2 = (
        time_format(time_vector, time_vector2)[0],
        time_format(time_vector, time_vector2)[1],
    )

    # Creates arrays for calculation of average time step in each vector
    time_step_list, time_step_list2 = [], []

    for i in range(1, len(time_vector)):
        time_step_list.append(time_vector[i] - time_vector[i - 1])
    for i in range(1, len(time_vector2)):
        time_step_list2.append(time_vector2[i] - time_vector2[i - 1])
    time_step, time_step2 = mean(time_step_list), mean(time_step_list2)

    real_step, real_time = min(time_step, time_step2), [
        max(time_vector[0], time_vector2[0])
    ]
    # len_time = ((min(time_vector[-1],time_vector2[-1])-max(time_vector[0],time_vector2[0]))/real_step) +1

    values_new2 = []

    j = 0  # Counter for real_time
    start = 0
    # Find the first quaternion event in new timeline
    while time_vector2[start] < real_time[0]:
        start += 1

    # Add time steps and first quaternion event values in these time steps until first quaternion event in new timeline
    values_new2.append(values2[start])
    while time_vector2[start] >= real_time[j]:
        values_new2.append(values2[start])
        real_time.append(real_time[j] + real_step)
        j += 1

    # Putting the first time value from quaternion event timeline
    real_time[j] = time_vector2[start]

    count = len(values2) - 1
    for i in range(start, count):
        evnt_time = (
            time_vector2[i + 1] - time_vector2[i]
        )  # Time steps between quaternion events
        n = ceil(evnt_time / real_step)  # Amount of interpolation steps
        new_real_step = evnt_time / (
            n + 1
        )  # Updated time step according to amount of time steps between quaternion events
        # (n+1) in order to later add the last values for interpolation
        for q in Quaternion.intermediates(
            values2[i], values2[i + 1], n, include_endpoints=False
        ):
            values_new2.append(q)
            real_time.append(real_time[j] + new_real_step)
            j += 1

        # Last values for the end of interpolation
        real_time.append(time_vector2[i + 1])
        j += 1
        values_new2.append(values2[i + 1])

    real_time = np.array(real_time)

    # TODO: Does not have time steps the same way as quaternions
    if type(values[0]) == tuple:
        xi, yi, zi, values_new = [], [], [], []
        for i in range(len(values)):
            xi.append(values[i][0])
            yi.append(values[i][1])
            zi.append(values[i][2])
        
        x_interpolate = np.interp(real_time, time_vector, xi).tolist()
        y_interpolate = np.interp(real_time, time_vector, yi).tolist()
        z_interpolate = np.interp(real_time, time_vector, zi).tolist()
        for i in range(len(x_interpolate)):
            values_new.append((x_interpolate[i], y_interpolate[i], z_interpolate[i]))

    elif type(values[0].value) == float:
        values_new, v = [], []
        for i in range(len(values)):
            v.append(values[i].value)
        values_new.append(np.interp(real_time, time_vector, v))

    return values_new, values_new2, Time(real_time, format="mjd", scale="utc")


def interp_intervalue(time_vector, time_vector2, values):
    values_new = []
    time_vector, time_vector2 = (
        time_format(time_vector, time_vector2)[0],
        time_format(time_vector, time_vector2)[1],
    )
    values_new.append(np.interp(time_vector, time_vector2, values))
    return values_new
