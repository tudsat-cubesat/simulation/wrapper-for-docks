from requirements import requirements
try :
    from EPS import EPS_simulation
    import click
    import yaml
except ImportError:
    requirements()
    from EPS import EPS_simulation
    import click
    import yaml
@click.command()
@click.option(
    "--config_file",
    help="config file of intervisibility module relative to exe--> yaml extension",
)
def EPS_cmd(config_file):
    with open(config_file) as config_yaml:
        data = yaml.safe_load(config_yaml)
    crit = EPS_simulation(data,config_file)
    return crit

if __name__ == "__main__":

    crit = EPS_cmd()

    if crit == 1:
        print("\nEnd of simulation \n")


