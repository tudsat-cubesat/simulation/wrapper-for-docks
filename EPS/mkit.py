"""
Math toolkit including:
    + linspace improvements
    + basic geometry
"""

import math
import sys
import time

import numpy
from nptyping import NDArray


def adapt_shape_vector(vector: NDArray[float]) -> NDArray[float]:
    """Adapt vector shape to ensure coherent axis calculations.
    Desired dimensions for a vector: Nx3."""
    if len(vector.shape) > 1:
        return vector
    return vector.reshape((1, vector.size))


def adapt_shape_data(vector: NDArray[float]) -> NDArray[float]:
    """Adapt data shape to ensure coherent axis calculations.
    Desired dimensions for a vector: Nx3."""
    if len(vector.shape) > 1:
        return vector
    return vector.reshape((vector.size, 1))


def arange(start: float, end: float, step: float) -> NDArray[float]:
    """Implementation of numpy.arange to include end."""
    return numpy.linspace(start, end, math.ceil((end - start) / step) + 1)


def dot(vectors_1: NDArray[float], vectors_2: NDArray[float]) -> NDArray[float]:
    """Compute dot product of vectors."""
    if vectors_1.shape != vectors_2.shape:
        sys.exit(
            f"Error dimensions: mkit.dot(A, B) where A.shape={vectors_1.shape} "
            f"and B.shape={vectors_2.shape}."
        )
    result = numpy.zeros(vectors_1.shape[0])
    i = 0
    for (vector_1, vector_2) in zip(vectors_1, vectors_2):
        result[i] = numpy.dot(vector_1, vector_2)
        i += 1
    return result


def distance(vector: NDArray[float]) -> float:
    """Compute the norm of a vector."""

    vector = adapt_shape_vector(vector)
    # return numpy.tensordot(vector, vector, axes=1) ** (1 / 2)
    return dot(vector, vector) ** (1 / 2)


def unit(vector: NDArray[float]) -> NDArray[float]:
    """Compute the unit vector."""
    vector = adapt_shape_vector(vector)
    return vector / adapt_shape_data(distance(vector))


def tic() -> float:
    """Initiate timer."""
    return time.time()


def toc(tic: float) -> float:
    """Compute elapsed time from a reference."""
    return time.time() - tic


def cart2sph(vector: NDArray[float]) -> NDArray[float]:
    """
    Convert cartesian to spherical coordinates (longitude, latitude, radius).
    Nx3 Radians
    """
    vector = adapt_shape_vector(vector)
    radius = distance(vector)
    return numpy.concatenate(
        (
            adapt_shape_data(numpy.arctan2(vector[:, 1], vector[:, 0])),
            adapt_shape_data(numpy.arcsin(vector[:, 2] / radius)),
            adapt_shape_data(radius),
        ),
        axis=1,
    )


def sph2cart(vector: NDArray[float]) -> NDArray[float]:
    """
    Convert spherical (longitude, latitude, radius) to cartesian coordinates.
    Nx3 Radians
    """
    vector = adapt_shape_vector(vector)
    return numpy.concatenate(
        (
            adapt_shape_data(
                vector[:, 2] * numpy.cos(vector[:, 0]) * numpy.cos(vector[:, 1])
            ),
            adapt_shape_data(
                vector[:, 2] * numpy.sin(vector[:, 0]) * numpy.cos(vector[:, 1])
            ),
            adapt_shape_data(vector[:, 2] * numpy.sin(vector[:, 1])),
        ),
        axis=1,
    )


def mrotx(angle: float) -> NDArray[float]:
    """Rotation matrix around x axis."""
    return numpy.array(
        [
            [1, 0, 0],
            [0, math.cos(angle), -math.sin(angle)],
            [0, math.sin(angle), math.cos(angle)],
        ]
    )


def mroty(angle: float) -> NDArray[float]:
    """Rotation matrix around x axis."""
    return numpy.array(
        [
            [math.cos(angle), 0, math.sin(angle)],
            [0, 1, 0],
            [-math.sin(angle), 0, math.cos(angle)],
        ]
    )


def mrotz(angle: float) -> NDArray[float]:
    """Rotation matrix around x axis."""
    return numpy.array(
        [
            [math.cos(angle), -math.sin(angle), 0],
            [math.sin(angle), math.cos(angle), 0],
            [0, 0, 1],
        ]
    )


def mrot3(anglex: float, angley: float, anglez: float) -> NDArray[float]:
    """Rotation matrix around x axis."""
    return mrotz(anglez) * mroty(angley) * mrotx(anglex)
