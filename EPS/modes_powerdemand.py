#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 11:30:29 2019

@author: ccastell
"""

import numpy as np
from astropy.time import *
from time import *
import ast, sys
import ccsds as cc
from docks_helper import mjd2_to_mjd1

sys.path.append("../python_wrapper")
import Wrapper
DEBUG = Wrapper.DEBUG
DEBUG_PATH = '../Output/'

def mjd_to_vts(time):
    mjd_integer_part = int(time) // 1
    mjd_decimal_part = TimeDelta((time % 1), format="jd").sec
    return mjd_integer_part, mjd_decimal_part


def vts_to_mjd(time):
    
    bp()
    mjd_hms_f = strftime("%H %M %S", gmtime(time[1])).replace(" ", ":")
    mjd_integer_iso = Time(time[0], format="mjd").iso
    mjd_time = mjd_integer_iso[: mjd_integer_iso.index(" ")]
    mjd_time = Time(mjd_time + " " + mjd_hms_f, format="iso").mjd
    print(mjd_time)
    return mjd_time


# _________________________________MODES_______________________________________


def separator_regulation(array):
    for i in range(array.size):
        array[i] = array[i].replace(" ", "\t")

    for i in range(len(array)):
        while "\t\t" in array[i]:
            array[i] = array[i].replace("\t\t", "\t")
    return array


def modes_read(modes_file, modes_names, modes_values):
    file = open(modes_file, "r")
    text = file.readlines()
    text = np.array(text)
    file.close()
    for i in range(text.size):
        text[i] = text[i].replace("\n", "")
    for i in range(text.size):
        if text[i] == "META_STOP":
            index_start = i
    text = text[index_start + 1 :]
    text = separator_regulation(text)
    separated_list = []
    for i in range(text.size):
        if text[i] != "":
            separated_list.append(text[i].split("\t"))
    time_list = []
    for i in range(len(separated_list)):
        time_list.append(separated_list[i][0] + " " + separated_list[i][-2])
    modes_list = []
    for i in range(len(separated_list)):
        modes_list.append(separated_list[i][-1])
    modes_values_list = []
    for i in range(len(modes_list)):
        if modes_list[i] not in modes_names:
            raise ValueError("The given modes names do not match with the file")
        else:
            index = modes_names.index(modes_list[i])
            modes_values_list.append(modes_values[index])
    return modes_values_list, time_list


def modes_values_time(time_vector, data, yaml_directory):
    
    
    # Data reading
    path_input = data["pathFiles"]["input"]
    modes = data["modes"]

    modes_file = f'{yaml_directory}{path_input["input_mode_file"]}'
    modes_names = modes["modes_names"]
    modes_values = modes["modes_values"]
    modes_average = modes["modes_average"]

    print("Compute power demand")
    if modes_file == "" and modes_average == "":
        raise ValueError("The conf_file must have modes_file or modes_average")

    if modes_names != "":
        modes_names = ast.literal_eval(modes_names)

    if modes_values != "":
        modes_values = ast.literal_eval(modes_values)

    if modes_average != "":
        modes_average = float(modes_average)

    # Reading the modes_file
    
    if modes_file != "":
        time_list = modes_read(modes_file, modes_names, modes_values)[1]
        modes_values_list = modes_read(modes_file, modes_names, modes_values)[0]
        
        for i in range(len(time_list)):
            time_list[i] = time_list[i].replace(" ", ",")
        
        for i in range(len(time_list)):
            integer_part = int(time_list[i][: time_list[i].index(",")])
            decimal_part = float(time_list[i][time_list[i].index(",") + 1 :])
            time_list[i] = (integer_part, decimal_part)
        
   
        time_list = list(mjd2_to_mjd1(np.array(time_list)))

        time_list = Time(time_list, format="mjd")
        modes_list = []
        
        for i in range(len(modes_values_list)):
            modes_list.append(float(modes_values_list[i]))
        time_vector = time_vector.value.tolist()
        time_list = time_list.value.tolist()
        
    # Making a list of the modes during the simulation
    
    if modes_file != "":
        
        modes_values = []
        if time_list[0] > time_vector[0]:  # if modes begin after the sim start
            for i in range(len(time_vector)):
                if time_vector[i] >= time_list[0]:
                    start = i
                    break
                else:
                    start = len(time_vector)
            for i in range(start):
                modes_values.append(0)

        for time in time_vector:
            new_modes = [value for (value, before, after) in zip(modes_list[1:], time_list[:-1], time_list[1:]) if before <= time < after]
            modes_values.extend(new_modes)

        ##################################################################################
        ##################################################################################

        diff = len(time_vector) - len(modes_values)

        for i in range(diff):
            modes_values.append(modes_list[-1])
        
    elif modes_file == "" and modes_average != "":
        modes_values = []
        for i in range(len(time_vector)):
            modes_values.append(modes_average)
    
    
    if DEBUG == 1:
        cic = "CIC_MEM_VERS = 1.0"
        origin = "ORIGINATOR = CENSUS / LabEx ESEP - Paris Observatory - PSL Univeristy Paris"
        comment = [
            "COMMENT = Power request (DEBUG)",
            "COMMENT = Columns : DATE MJDday + MJDseconds + REQUEST  ",
        ]
        header = [
            "OBJECT_NAME = CubeSat",
            "OBJECT_ID = CubeSat \n",   
            "USER_DEFINED_PROTOCOL = NONE",
            "USER_DEFINED_CONTENT = CHRONOGRAMS",
            "USER_DEFINED_SIZE = 1",
            "USER_DEFINED_TYPE = REAL",
            "USER_DEFINED_UNIT = [n/a] \n",
            "TIME_SYSTEM = UTC",
        ]
        sequence = ["%d", "%.3f"]
        sequence.append("%.3f")
        data_time = []

        for i in range(len(time_vector)):
            data_time.append(mjd_to_vts(time_vector[i]))
        data_time = np.array(data_time)
        results = np.reshape(np.array(modes_values), (len(modes_values), 1))
        print("(writing power_request)")
        cc.write_ccsds(
            DEBUG_PATH + "power_request.txt",
            cic,
            origin,
            comment,
            header,
            sequence,
            data_time,
            results,
            separator="\t",
        )

    return modes_values
