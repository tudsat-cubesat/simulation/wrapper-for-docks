DOCKS Intervisibility module, __v2.0__

Date of modification : *28-Jan-2021*

Authors
=============================================


Development by Observatory of Paris - PSL Université Paris - 
Laboratory of Excellence ESEP, PSL Space Pole [CENSUS](https://cceres.psl.eu/?lang=en) 

2020: Thibault DELRIEU, Boris SEGRET
Past contributors: Sebastien DURAND (2019), Janis DALBINS (2019)

&nbsp;

---

[TOC]

---

&nbsp;

Features of the DOCKS Intervisibility module
=============================================

The DOCKS Intervisibility module computes the intervisibility of the satellite with the sun and ground stations. As a result, it provides compete information about the eclipses and ground station passes of a satellite for the complete mission duration. 

&nbsp;

Remote Service
=============================================

A remote service has been developed recently to prevent the user from doing any installation process on their system. Folow these steps to take advantage of this:

1. Send an email to **`docks.contact@obspm.fr`** with the keyword **`[server]`** in the subject. 
2. In return you will receive an email with a link to a shared directory.
3. Read the file "README.md" to understand the working of remote service.
4. Upload the input files in "input" folder and configuration file in "Intervisibility" folder. 
3. Once you upload all the required files, our server will compute the results for you.
4. All the output files will be saved in "output" folder. 
5. If the submitted configuration file is moved to output folder, then the computation is complete.

**Note:** for now, the user is not notified once the computation is complete. So, the user will have to check manually if the results are generated are not. **There is a limit of 1 week on this shared directory, after which the directory will be deleted automatically.**

&nbsp;

Installing DOCKS Intervisibility module
=============================================

**Before installing DOCKS Intervisibility, please install:** 
* DOCKS Tools module 1.3+ [link](https://gitlab.com/cceres-docks/docks_tools)
* Python 3.7+

### Linux __and__ MS-Windows Users

1. Considering that `(project/)` is the path to your local workspace for DOCKS modules, create there a subfolder __"DOCKS"__ to store all functional modules. 

2. Download the full module from the link provided in Gitlab > Repository > __Tags__ and decompress as `(project/)DOCKS/Intervisibility` subfolder. 

3. Put the folder docks_tools previously downloaded in `(project/)DOCKS/docks_tools` 

That's it, you don't have to do anything more: at the first run, the program will automatically install the dependencies needed if it doesn't find them. For compatibility reasons with embedded libraries, DOCKS may need to *downgrade* some libraries to specific versions and will ask you before proceeding. The versions of libraries before DOCKS installation will then be saved at the root of the module's folder Intervisibility.

### Test the installation

Two test cases are provided in `example/`. The inputs required for these tests are in `example/input`. In order to test if the installation is successful, just run the test config file using the following command in terminal/windows prompt:
`$ python3 (path-to-intervisi/)main.py --config_file=(path-to-intervisi/)input/testMe_config.yaml`

`(path-to-intervisi/)` being the path to your DOCKS Intervisibility module folder. 

The program shall finish with "End of processing" and the output files should be saved in `example/output`.


### Common installation errors 

1. **Linux  LLVM error**: If you have an LLVM error on linux, open a shell and write :
` $ sudo apt install llvm-9`
and then 
` $ export LLVM_CONFIG=/usr/bin/llvm-config-9`

2. Sometimes, if after first installation the program does not run successfully, try to run it again.

&nbsp;

Configuration File: .yaml
=============================================

From this version, the configuration must be given in a ".yaml" file. You can find an example of this file here: 
`(path-to-intervisi/)input/testMe_config.yaml`. The section of the lines after '#' are comments, not interpreted by the program. The example test_file.yaml provides extensive comments to help you with the needed inputs.

>==**WARNING** : Both absoloute or relative paths can be used in the configuration file. If relative paths are used, they should be specified relative to the location of the configuration file.==

&nbsp;

Running DOCKS Intervisibility module
=============================================

In order to run the Intervisibility module you need to enter a command with the following pattern:
`$ python3 (path-to-intervisi/)main.py --config_file=(path-to-config/)CONFIG_FILE`

`(path-to-intervisi/)` : relative or absolute path to main.py (within DOCKS Intervisibility), no need to run python from any particular location. 

`(path-to-config/)` : relative or absolute path to your configuration file folder. 

For instance :

1. your current directory is : `/home/myfolder/IamHere/`
2. DOCKS Intervisibility is at : `/home/DOCKS/Intervisibility` 
3. the configuration file is at : `/home/config_folder/config_file.yaml` 

Then enter the commande line :
`$ python3 ../../DOCKS/Intervisibility/main.py --config_file=../../config_folder/config_file.yaml`

or you can also use the absolute path (with your OS' syntax, here in Linux): 
`$ python3 /home/DOCKS/Intervisibility/main.py --config_file=/home/config_folder/config_file.yaml`

&nbsp;

Inputs 
=============================================
In order to compute with DOCKS Intervisibility module you need to provide 2 inputs :

1. a configuration file (.yaml), see section above. 

2. a trajectory file (.txt), in the format given by the example provided in `(project/)DOCKS/Intervisibility/input/propagator/traj_test_file.txt`. The DOCKS Propagator module produces trajectories in this format. 

&nbsp;

Outputs 
=============================================

DOCKS Intervisibility module generates two output files:

1. A human readable **EVTF** file for the user that summarises the eclipses and ground station passes for the given mission trajectory.
2. A binary EVTF file which is required as an input for the DOCKS EPS module.
