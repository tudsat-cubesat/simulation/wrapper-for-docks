"""
# DOCKS helper

Latest update: 03-SEP-2020
Contacts:
    + Grégoire Henry
    + Boris Segret

## Definition

Collection of functions related to DOCKS that are used for several scripts.
"""

import datetime
import os
import sys
from typing import List

import numpy
from nptyping import NDArray
from scipy import interpolate
import math


def read_cic(
    fname: str, ind: slice = slice(None, None), meta: bool = True
) -> NDArray[float]:
    """Open trajectory file with cic format to import the state vector as
    ndarray. It extracts state vector columns withing the given range (default
    is the whole state vector)."""
    T = []
    if not os.path.isfile(fname):
        fname_save = fname
        fname = f"rsc/traj/{fname.split('/')[-1]}"
        if not os.path.isfile(fname):
            fname = fname_save
    with open(fname) as F:
        for L in F:
            L = L.strip()
            if L == "META_STOP":
                meta = False
                continue
            if meta or not L:
                continue
            T.append([float(e) for e in L.split()[ind]])
    return numpy.asarray(T)


def rframe_change(
    fname_target: str,
    fname_other: str,
    header: str = None,
    metas: List[bool] = None,
    sign: str = "+",
):
    """Proceed to a change of reference frame on the target trajectory by
    adding the other trajectory to it, and exporting it to replace the target"""
    signs = {"+": 1, "-": -1}
    if metas is None:
        metas = [True, True]
    ind = slice(None, 8)
    target = read_cic(fname_target, ind, metas[0])
    other = read_cic(fname_other, ind, metas[1])
    other = interpolate_trajectories(target, other)
    time = target[:, :2]
    target = target[:, 2:]
    other = other[:, 2:]
    target = target + other * signs[sign]
    target = numpy.concatenate((time, target), axis=1)
    write_cic(fname_target, target, header)


def interpolate_trajectories(
    target: NDArray[float], other: NDArray[float]
) -> (NDArray[float], NDArray[float]):
    """Interpolate other trajectory according to target trajectory."""
    return interpolate_trajectory(target[:, :2], other)


def interpolate_trajectory(
    time: NDArray[float], target: NDArray[float]
) -> NDArray[float]:
    """Interpolate trajectory according to time."""
    if len(time.shape) == 1:
        time = mjd1_to_mjd2(time)
    time_old = target[:, :2]
    time1d_old = mjd2_to_mjd1(time_old)
    time1d = mjd2_to_mjd1(time)
    target_new = numpy.zeros((time.shape[0], target.shape[1]))
    target_new[:, :2] = time
    for i in range(2, target.shape[1]):
        interp = interpolate.interp1d(time1d_old, target[:, i], kind="cubic")
        target_new[:, i] = interp(time1d)
    return target_new


def mjd2_to_mjd1(time: NDArray[float]) -> NDArray[float]:
    """Convert time format from MJD two columns to MJD one column"""
    return time[:, 0] + time[:, 1] / 86400


def mjd1_to_mjd2(time: NDArray[float]) -> NDArray[float]:
    """Convert time format from MJD one column to MJD two columns"""
    col0 = time // 1
    col1 = (time - time // 1) * 86400
    col0 = col0.reshape((col0.size, 1))
    col1 = col1.reshape((col1.size, 1))
    return numpy.concatenate((col0, col1), axis=1)


def write_cic(fname: str, data: NDArray[float], header: str):
    """Write trajectory to cic file."""
    if data.shape[1] == 8:
        fmt = "%d\t%d\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f"
    else:
        sys.exit(
            "Error: expected trajectory shape (N, 8) but %s is given" % (data.shape,)
        )
    numpy.savetxt(fname, data, fmt=fmt, header=header, comments="")


def create_header(
    object_name: str = "",
    center_name: str = "",
    ref_frame: str = "",
    time_system: str = "",
) -> str:
    """Create header for cic file."""
    date = datetime.datetime.now().isoformat()
    return f"""CIC_OEM_VERS = 2.0
CREATION_DATE = {date}
ORIGINATOR = docks_helper.py

META_START

COMMENT         DOCKS Propagator version 4.0.0
COMMENT         Propagator method : rk4_module
COMMENT         Colomn 1:2 Unit : Time (MJD)
COMMENT         Colomn 3:5 Unit : Position (KM)
COMMENT         Colomn 6:8 Unit : Velocity (KM/S)
COMMENT         Colomn 9:11 Unit : Acceleration (KM/S^2)

OBJECT_NAME     = {object_name}
OBJECT_ID       = CORPS

CENTER_NAME     = {center_name}
REF_FRAME       = {ref_frame}
TIME_SYSTEM     = {time_system}

META_STOP
"""


def create_config(
    fname: str,
    name: str,
    gravitational_perturbations: List,
    new_perturbations: List,
    duration: float,
    time_step: float,
):
    """Create config file for DOCKS propagator."""
    config = f"""**Propagator_START**
Initial_conditions_file=initial-condition-files/init-{name}.txt
Output_file_name={name}.txt
Output_directory=output-trajectories
Init_input=sun;ICRF;MJD;KM;KM/S
Gravitational_perturbations={gravitational_perturbations}
Ephem_type=jpl
New_Perturbations={new_perturbations}
Complex_grav_model_activated=False
Complex_grav_model_bodies=[]
Non_gravitational_perturbations=[]
Number_of_bodies={len(gravitational_perturbations)}
Number_of_new_bodies={len(new_perturbations)}
Number_of_cgm_bodies=0
Number_of_non_gravitational_perturbations=0
Propagation_param=rk4_module;0;0
Time=Duration;{duration};0:0:0.0
Time_step=Hours;{time_step};0;0
Number_of_steps={duration * 24 / time_step}
output_format=sun;ICRF;MJD;KM;KM/S;KM/S^2;1
**Propagator_STOP**"""
    with open(fname, "w") as F:
        F.write(config)


def arange(start: float, end: float, step: float) -> NDArray[float]:
    """Implementation of numpy.arange to include end."""
    return numpy.linspace(start, end, math.ceil((end - start) / step) + 1)
