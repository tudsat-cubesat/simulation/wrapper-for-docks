#-------------------------------------------------
#
# Project created by QtCreator 2018-06-29T11:57:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Intervisibility
TEMPLATE = app



FORMS += \
        setting_groundstations.ui

CONFIG += c++11

OTHER_FILES +=
