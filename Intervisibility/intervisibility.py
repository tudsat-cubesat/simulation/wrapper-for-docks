#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 10:56:30 2019

@author: Sébastien Durand
"""

from astropy.coordinates import AltAz
from astropy.coordinates import solar_system_ephemeris, SkyCoord, EarthLocation
from astropy.time import Time
from astropy import units as u
import numpy as np
from poliastro import bodies
import matplotlib.pyplot as plt
import sys
import time_function as tt
from nptyping import NDArray
from typing import List, Tuple, Union
import threading as thr
from utils_intervisibility_functions import (
    check_event_precision,
    check_event_duration,
    enablePrint,
    blockPrint,
)

import ccsds as cc
import pickle as pck
import astropy_ephem as as_eph

from docks_helper import interpolate_trajectory, arange, mjd1_to_mjd2

solar_system_ephemeris.set("de432s")
plt.ion()  # To immediately show plots

from astropy.coordinates import GCRS, ICRS
from astropy.coordinates import CartesianRepresentation, CartesianDifferential

# _________________________________START Fonction____________________________


def eclipse_calc(
    time_vector: NDArray[float],
    sat_orbit_icrs: ICRS,
    cible_eph_sat_sky,
    cible_ray,
    object_name,
    object_ray,
):

    # compute bolies numbers
    nb_bodie = len(object_name)

    # initialise value
    angle_bodies = np.zeros(nb_bodie)
    sep_bodies = []

    # compute size of angular cible for all time in rad
    cible_angle = np.arcsin(cible_ray * u.m / cible_eph_sat_sky.distance.to("m"))
    cible_angle = cible_angle.to("rad").value

    # compute angular separation for all bodie in all time
    for i in range(nb_bodie):
        # compute bodie eph
        __, bodie_eph_icrs = as_eph.astropy_eph_body(
            object_name[i], time_vector, frame="ICRS", plot=0
        )

        # convert in local icrf
        bodie_eph_sat_skyh_sat = (
            bodie_eph_icrs.cartesian.without_differentials()
            - sat_orbit_icrs.cartesian.without_differentials()
        )
        # convert in sky coord
        bodie_eph_sat_sky = SkyCoord(bodie_eph_sat)

        # compute bodie angular size
        angle_bodies[i] = (
            np.arcsin(object_ray[i] * u.m / bodie_eph_sat_sky.distance.to("m"))
            .to("rad")
            .value
        )

        # compute angular separation
        sep_bodies.append(
            [cible_eph_sat_sky.separation(bodie_eph_sat_sky).to("rad").value]
        )

    # compute idf eclipse is true for all time
    ecl = []
    for i in range(nb_bodie):
        ecl.append(sep_bodies[i] > (angle_bodies[i] + cible_angle))
    ecl_bodies = np.sum(ecl, axis=0)
    ecl_bodies_bol = np.bool(ecl_bodies)

    event_type = []
    time_event = []
    state = 0

    # search all event (ingres, egres)
    for i in range(len(time_vector) - 1):
        if not (ecl_bodies_bol[i]) and ecl_bodies_bol[i + 1]:
            if state == 0:
                event_type.append("ECL/IN00001")
                time_event.append(time_vector[i].mjd)
                state = 1
        elif ecl_bodies_bol[i] and not (ecl_bodies_bol[i + 1]):
            if state == 1:
                event_type.append("ECL/EG00001")
                time_event.append(time_vector[i].mjd)
                state = 0

    time_event = np.array(time_event)
    event_type = np.array(event_type)

    return time_vector, ecl, time_event, event_type


def ground_station_intervisibility(
    time_vector: NDArray[float],
    sat_orbit: GCRS,
    ground_station: Tuple[str, EarthLocation, float, str],
    interpolation_pos_input: NDArray[float],
    event_duration: float,
    event_precision: float,
    plot_data: int = 0,
) -> Tuple[NDArray[float], NDArray[str], NDArray[int], Tuple[float, float]]:
    """
    This fonction compute visibility enter ground station and satellite for discrete time

    :param time_vector:(Astropy time, size: N) Vector of time
    :param sat_orbit:(Astropy position, with frame, size: N*3 or N*6) Array of float or Numpy.float (not Numpy.float32)
                       Sat position in Frame
    :param ground_station:(Array Mx4) 1: ground station name, 2: Astropy EarthLocation,
                                      3: (float) elevation limit in degree 4: (str) ground station number
    :param plot_data: option for plot visibility over time and all event.

    :return: time_event:(float vector, size: K) mjd date of event
             event_type:(string vector, size K) COMINxxxxx and COMEGxxxxx (xxxxx = ground station number)
             __Warning__: visibility_gs and sat_altaz_gs is just for last ground station.
             visibility_gs:(int vector, size: N) state visibility for all time (1=visible, 0=not visible)
             sat_altaz_gs:(float array, size: N*2) satellite position in sky, 1:(degree) Altitude, 2:(degree) Azimuth

    TODO: Input: Add eclipse concept and Adding an object list that can eclipse (limit in astropy object, no earth)
    """
    print("Threading process >", end="")

    def ground_station_calcul(gs: int) -> GCRS:

        sat_altaz = sat_orbit.transform_to(
            AltAz(obstime=time_vector, location=ground_station[gs][1])
        )

        result[gs] = sat_altaz

    result = [None] * len(ground_station)

    thread_dict = {}

    for gs, _ in enumerate(ground_station):
        thread_dict[gs] = thr.Thread(
            name=f"thread {gs}", target=ground_station_calcul, args=(gs,)
        )
        thread_dict[gs].start()
    for gs, _ in enumerate(ground_station):
        thread_dict[gs].join()

    event_type = []
    time_event = []

    for gs in range(len(ground_station)):

        event_in = []
        event_out = []
        nb_in = []
        nb_out = []
    
        azi_values = result[gs].az.value
        alt_values = result[gs].alt.value

        sat_altaz_gs = (azi_values, alt_values)

        visibility_gs = np.zeros(len(time_vector))

        state = 0

        for i in range(len(time_vector)):
            if sat_altaz_gs[1][i] > ground_station[gs][2]:
                visibility_gs[i] = 1
                if state == 0:
                    event_in.append(time_vector[i])
                    nb_in.append(i)

                state = 1

            else:
                if state == 1:
                    event_out.append(time_vector[i])
                    nb_out.append(i)
                state = 0

        if plot_data == 1:
            plt.figure()
            plt.plot(visibility_gs)
            visibility_gs_in = np.zeros(len(time_vector))
            visibility_gs_in[nb_in] = 1
            plt.plot(visibility_gs_in)
            visibility_gs_out = np.zeros(len(time_vector))
            visibility_gs_out[nb_out] = 1
            plt.plot(visibility_gs_out)

        for i in range(len(event_in)):
            event_in[i] = event_in[i].mjd
        for i in range(len(event_out)):
            event_out[i] = event_out[i].mjd

        time_event = np.append(time_event, np.append(event_in, event_out))

        event_type_in = np.zeros(len(event_in), dtype=np.dtype("<U63"))
        event_type_in[:] = f"COM/IN\t0000{int(ground_station[gs][3])}"
        event_type_out = np.zeros(len(event_in), dtype=np.dtype("<U63"))
        event_type_out[:] = f"COM/EG\t0000{int(ground_station[gs][3])}"
        event_type = np.append(event_type, np.append(event_type_in, event_type_out))

    new_order = np.argsort(time_event)
    event_type = event_type[new_order]
    time_event = time_event[new_order]

    if (
        len(interpolation_pos_input) != 0
        and event_duration != event_precision
        and len(event_type) != 0
    ):
        print("Interpolation >", end="")
        sat_gcrs, new_time = check_event_precision(
            time_event, interpolation_pos_input, event_duration, event_precision
        )
        (
            time_event,
            event_type,
            sun_eph_sat_sky,
            sun_inter_sat,
        ) = ground_station_intervisibility(
            new_time,
            sat_gcrs,
            ground_station,
            interpolation_pos_input=np.array([]),
            event_duration=event_duration,
            event_precision=event_precision,
            plot_data=0,
        )

    else:
        print("Checking inter-event duration >", end="")
        event_type, time_event = check_event_duration(
            event_type, time_event, event_duration, "COM/IN", len(ground_station)
        )

    return time_event, event_type, visibility_gs, sat_altaz_gs


def cic_intervisibility(
    time_event: NDArray[float],
    event_type: NDArray[str],
    config_name: str,
    path_inter: str,
) -> None:
    """
    This fonction writes the events in file at CIC format

    :param time_event: (float vector, size: K) mjd date of event
    :param event_type: (string vector, size K) COMINxxxxx and COMEGxxxxx (xxxxx = ground station number)
    :param orbit_sat_file: (string) Name and path of Satellite orbit files
    :param path_inter: (string) Name and path for intervisibility CIC file

    :return: No have return
    """
    new_order = np.argsort(time_event)
    event_type = event_type[new_order]
    time_event = time_event[new_order]

    time_event = Time(time_event, format="mjd")

    cic = "CIC_MEM_VERS = 1.0"
    origin = "ORIGINATOR = DOCKS / CENSUS / LabEx ESEP - Paris Observatory - PSL University Paris"

    comment = ["COMMENT = Intervisibility for config : " + config_name.split("/")[-1],
               "COMMENT = Columns : DATE MJDday + MJDseconds + EVENT + EventIndex"]
    header = [
        "USER_DEFINED_PROTOCOL = NONE",
        "USER_DEFINED_CONTENT = OEF",
        "USER_DEFINED_SIZE = 2",
        "USER_DEFINED_TYPE = STRING",
        "USER_DEFINED_UNIT = [n/a]",
        "TIME_SYSTEM = UTC",
    ]
    sequence = ["%s", "%s", "%s"]
    data_time_temp = np.array(
        [np.int32(time_event.mjd), np.float32((time_event.mjd % 1) * 60 * 60 * 24)]
    )
    # data_time = np.zeros((2, len(data_time_temp[0])), dtype=np.dtype("<U60"))
    # for i in range(len(data_time[0])):
    #     data_time[0][i] = str(np.int32(data_time_temp[0][i]))
    # for i in range(len(data_time[1])):
    #     data_time[1][i] = str(round(data_time_temp[1][i], 6))
    # data_time = data_time.T

    data_time = [[*map("{:.0f}".format, data_time_temp[0,:])], [*map("{:15.6f}".format, data_time_temp[1,:])]]
    data_time = np.array(data_time).T

    data_science = []
    for i in range(len(event_type)):
        data_science.append([f'{event_type[i]: >15}'])
    cc.write_ccsds(
        path_inter, cic, origin, comment, header, sequence, data_time, data_science
    )


def add_sun_to_gs(
    orbit_sat_file: str, path_add_sun: str, path_add_gs: str, path_add_gs_sun: str
) -> None:
    """
    This fonction merged the solar file events and ground stations file events in file a one CIC file.

    :param orbit_sat_file: (string) Name and path of Satellite orbit files
    :param path_add_sun: (string) Name and path of Sun intervisibility events files
    :param path_add_gs: (string) Name and path of Ground Station intervisibility events files
    :param path_add_gs_sun: (string) Name and path for Sun and ground station intervisibility events files

    :return: No have return
    """
    path_inter = []
    path_inter_sun = []
    path_inter_station = []

    for i in orbit_sat_file:
        path_inter_sun.append(path_add_sun + i)
    for i in orbit_sat_file:
        path_inter_station.append(path_add_gs + i)
    for i in orbit_sat_file:
        path_inter.append(path_add_gs_sun + i)

    for i in range(len(orbit_sat_file)):
        (
            cic_sun,
            origin_sun,
            comment_sun,
            header_sun,
            data_time_sun,
            data_science_sun,
        ) = cc.read_ccsds(path_inter_sun[i], separator="\t")
        cic, origin, comment, header, data_time, data_science = cc.read_ccsds(
            path_inter_station[i], separator="\t"
        )

        time_sun = []
        for ss in data_time_sun:
            time_sun.append(np.int32(ss[0]) + np.float32(ss[1]) / (60 * 60 * 24))

        time_station = []
        for gs in data_time:
            time_station.append(np.int32(gs[0]) + np.float32(gs[1]) / (60 * 60 * 24))

        event_type = np.append(data_science, data_science_sun)
        time_event = np.append(time_station, time_sun)

        cic_intervisibility(time_event, event_type, orbit_sat_file[i], path_inter[i])


def eps_output(
    path_output: str, sun_eph_sat_sky: SkyCoord, sun_inter_sat: NDArray[int], time_vector : NDArray[float]
) -> None:
    """
    write pickle variables for EPS.
    """
    if path_output == None:
        return 0

    with open(path_output, "wb") as file:
        pck.dump(sun_eph_sat_sky, file)
        pck.dump(sun_inter_sat, file)
        pck.dump(time_vector, file)

    return 0


def sun_intervisibility(
    time_vector: NDArray[float],
    sat_gcrs: GCRS,
    interpolation_pos_input: NDArray[float],
    event_duration: float,
    event_precision: float,
    plot: int = 0,
) -> Tuple[NDArray[float], NDArray[str], SkyCoord, NDArray[int]]:
    """
    Compute Sun intervisibility in low earth orbit for all time

    :param time_vector:(Astropy time, size: N) Vector of time
    :param sat_gcrs:(Astropy position, with GCRS frame, size: N*3 or N*6) Array of float or Numpy.float
                    (not Numpy.float32) Sat position and velocity in Frame
    :param plot: option for plot ephemeris of earth and sun position.

    :return:time_event:(float vector, size: K) mjd date of event
            event_type:(string vector, size K) ECLIN00001 and ECLEG00001
            sun_eph_sat_sky:(Astropy coordinate) Sun position in Satellite sky
            sun_inter_sat:(int vector) Sun intervisibility (1 = Sun visibility, 0 = eclipse or partial by object)


    TODO: Add compute value for partial eclipse (need add eclipse model)
    TODO: Run all in ICRS frame (obvious)
    TODO: Input : Adding an object list that can eclipse (limit in astropy object)

    """

    sun_ray = ["none"]
    earth_ray = ["none"]
    sun_eph_gcrs = ["none"]
    earth_eph_gcrs = ["none"]
    print("Threading process >", end="")

    # queue = SimpleQueue()

    def compute_ephemeris_sun(thread_num: int) -> GCRS:
        blockPrint()

        __, sun_eph_gcrs[0] = as_eph.astropy_eph_body(
            "sun", time_vector, frame="GCRS", plot=plot
        )

        enablePrint()

    def compute_ephemeris_earth(thread_num: int) -> GCRS:
        blockPrint()
        __, earth_eph_gcrs[0] = as_eph.astropy_eph_body(
            "earth", time_vector, frame="GCRS", plot=plot
        )

        enablePrint()

    thread1 = thr.Thread(name=f"thread {1}", target=compute_ephemeris_sun, args=(1,))
    thread2 = thr.Thread(name=f"thread {2}", target=compute_ephemeris_earth, args=(2,))

    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()

    sun_ray[0] = bodies.Sun.R.base
    earth_ray[0] = bodies.Earth.R.base

    blockPrint()
    sun_eph_sat = (
        sun_eph_gcrs[0].cartesian.without_differentials()
        - sat_gcrs.cartesian.without_differentials()
    )
    earth_eph_sat = (
        earth_eph_gcrs[0].cartesian.without_differentials()
        - sat_gcrs.cartesian.without_differentials()
    )
    enablePrint()
    sun_eph_sat_sky = SkyCoord(sun_eph_sat)
    earth_eph_sat_sky = SkyCoord(earth_eph_sat)

    sep_sun_earth = sun_eph_sat_sky.separation(earth_eph_sat_sky)

    sun_angle = np.arcsin(sun_ray[0] * u.m / sun_eph_sat_sky.distance.to("m"))
    earth_angle = np.arcsin(earth_ray[0] * u.m / earth_eph_sat_sky.distance.to("m"))

    angular_size_sun = sun_angle.to("rad").value
    angular_size_2 = earth_angle.to("rad").value
    angular_distance = sep_sun_earth.to("rad").value

    # def occultation_model_meter(angular_distance, angular_size_1, angular_size_2, po=0):

    # ec = angular_distance >= (angular_size_1 + angular_size_2)

    # ec = angular_distance>=R2
    event_type = []
    time_event = []
    state = 0
    iterator_eclin = 1
    iterator_ecleg = 1
    sun_inter_sat = np.int32(angular_distance >= (angular_size_2 + angular_size_sun))
    for i in range(len(angular_distance) - 1):
        if (
            state == 0
            and angular_distance[i] <= angular_size_2[i] + angular_size_sun[i]
        ):
            event_type.append("ECL/IN\t" +f"{iterator_eclin:05d}")

            time_event.append(time_vector[i].mjd)
            state = 1
            iterator_eclin += 1

        elif state == 1 and angular_distance[i] >= angular_size_2[i]:

            if iterator_eclin == 1:
                iterator_ecleg = 0

            event_type.append("ECL/EG\t" +f"{iterator_ecleg:05d}")
   
            time_event.append(time_vector[i].mjd)
            state = 0

            iterator_ecleg += 1

    if (
        len(interpolation_pos_input) != 0
        and event_duration != event_precision
        and len(event_type) != 0
    ):
        print("Interpolation >", end="")
        sat_gcrs, new_time = check_event_precision(
            time_event, interpolation_pos_input, event_duration, event_precision
        )

        time_event, event_type, _, _, _ = sun_intervisibility(
            new_time,
            sat_gcrs,
            interpolation_pos_input=np.array([]),
            event_duration=event_duration,
            event_precision=event_precision,
            plot=0,
        )


            
    else:
        print("Checking inter-event duration", end="")
        event_type, time_event = check_event_duration(
            event_type, time_event, event_duration, "ECL/IN"
        )

    return time_event, event_type, sun_eph_sat_sky, sun_inter_sat, time_vector


# _________________________________END Fonction____________________________
