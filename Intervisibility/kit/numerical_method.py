"""
Numerical iterative method to solve ordinary differential equations.
Supported methods:
    + Euler
    + Runge-Kutta 4
"""

import math
import sys
from typing import Callable, Dict

import numpy
from nptyping import NDArray

import mkit


def physics_check(_state_vector: NDArray[float]) -> int:
    """Check when physics is broken."""
    # Bellow ground: if state_vector[2] < 0: return 1
    return 0


def integrate(
    dynamical_model: Callable,
    state_vector: NDArray[float],
    delta_time: float,
    method="euler",
) -> NDArray[float]:
    """Numerical integration methods."""
    if dynamical_model["method"].__name__ not in [
        "integrate_euler",
        "integrate_rk4",
    ]:
        sys.exit(f"ERROR: method {method} not implemented.")
    state_vector_new = dynamical_model["method"](
        dynamical_model, state_vector, delta_time
    )

    return state_vector_new


def integrate_euler(
    dynamical_model: Dict, state_vector: NDArray[float], delta_time: float
) -> NDArray[float]:
    """Numerical integration with the Euler method:
    $q_{n+1}=\frac{dq}{dt}dt+q_{n}$"""
    return state_vector + delta_time * compute_derivative(dynamical_model, state_vector)


def integrate_rk4(
    dynamical_model: Callable,
    state_vector: NDArray[float],
    delta_time: float,
) -> NDArray[float]:
    """Numerical integration with the Runge-Kutta 4 method"""
    k_1 = delta_time * compute_derivative(dynamical_model, state_vector)
    k_2 = delta_time * compute_derivative(dynamical_model, state_vector + k_1 / 2)
    k_3 = delta_time * compute_derivative(dynamical_model, state_vector + k_2 / 2)
    k_4 = delta_time * compute_derivative(dynamical_model, state_vector + k_3)
    return state_vector + (k_1 + 2 * k_2 + 2 * k_3 + k_4) / 6


def compute_derivative(
    dynamical_model: Dict, state_vector: NDArray[float]
) -> NDArray[float]:
    """Compute the derivative"""
    acceleration = compute_accelerations(dynamical_model, state_vector)
    return numpy.concatenate((state_vector[3:], acceleration))


def compute_accelerations(model: Dict, state_vector: NDArray[float]) -> NDArray[float]:
    """Dynamical model to compute acceleration from state vector (position and
    velocity)."""
    acceleration = numpy.zeros(3)
    for (force, parameters) in model["forces"].items():
        if force == "attraction":
            position_relative = state_vector[:3] - numpy.zeros(3)
            acceleration -= (
                model["constants"]["G"]
                * parameters["mass"]
                * mkit.unit(position_relative)
                / mkit.distance(position_relative) ** 2
            )
        elif force == "gravity":
            acceleration -= numpy.array([0, 0, model["constants"]["G"]])
    return acceleration


def solve(
    time_vector: NDArray[float],
    state_vector: NDArray[float],
    dynamical_model: Dict,
    verbose: bool = True,
) -> NDArray[float]:
    """Solve system."""
    tic = mkit.tic()
    time_previous = time_vector[0]
    loading_step = time_vector[-1] / 100
    loading_current = loading_step
    for time_current in time_vector[1:]:
        if abs(time_current) > abs(loading_current):
            if verbose:
                print(
                    "Start propagation.. "
                    f"{math.floor((time_current / time_vector[-1]) * 100)}%",
                    end="\r",
                )
            loading_current += loading_step
        delta_time = time_current - time_previous
        state_vector = integrate(dynamical_model, state_vector, delta_time)
        check = physics_check(state_vector)
        if check == 1:
            break
        time_previous = time_current
    if verbose:
        print(f"{mkit.toc(tic)} s elapsed.")
    return state_vector
