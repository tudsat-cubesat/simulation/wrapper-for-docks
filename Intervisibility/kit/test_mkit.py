"""Usage example for mkit module."""

import math

import numpy

import mkit

#####

a = numpy.random.random((2, 3))
b = mkit.unit(a)
da = mkit.distance(a)
db = mkit.distance(b)

c = mkit.cart2sph(a)
d = mkit.sph2cart(c)

####

a = numpy.array([1, 0, 0])

b = numpy.matmul(a, mkit.mrot3(0, 0, math.pi))
