"""
Toolkit usage example: space propagation.
"""

import numpy

import mkit
import numerical_method as nm

# Constants
dynamical_model = dict(
    method=nm.integrate_rk4,
    constants=dict(G=6.67408e-11, day=86400),
    forces=dict(attraction=dict(mass=1e13, position=numpy.zeros(3))),
    # forces=dict(gravity=None),
)


# Initialization
state_vector = numpy.array([-5e3, 0, 0, 0, 1, 0])
time_vector = mkit.arange(0, -dynamical_model["constants"]["day"] * 15, -10)

# Solve
nm.solve(time_vector, state_vector, dynamical_model, verbose=True)
