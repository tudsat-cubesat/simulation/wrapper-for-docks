#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Apr 10, 2019
@author: Sébastien Durand, CENSUS, Paris Observatory - PSL University
         first adaptations for NCU-SCION needs by Boris Segret, CCERES, Paris Observatory - PSL University
"""

from os import listdir, system
import warnings
import pkgutil
import sys
from math import isclose

from pathlib import Path

# DARKMAGIC this is just to overcome the fact we are not using pip to install docks_tools as a real dependancy
sys.path.append(str(Path(__file__).resolve().parent.parent / "docks_tools/"))

from astropy.utils.exceptions import AstropyWarning

warnings.simplefilter("ignore", category=AstropyWarning)
from astropy.coordinates import EarthLocation
from astropy import units as u
from astropy.coordinates import CartesianRepresentation, CartesianDifferential
from astropy.coordinates import GCRS, ICRS


from astropy.time import Time
import numpy as np
import click


# sys.path.append("../docks_tools/")
from utils_intervisibility_functions import enablePrint, blockPrint
import intervisibility as inter


import time_function as tt
import ccsds as cc
from docks_helper import read_cic, interpolate_trajectory, arange, mjd1_to_mjd2
import yaml




# _________________________START Personal specification_______________________


def launching_inter(config_file: str, server = False) -> None:

    """
    Fonction of initialisation for launching Intervisibility module
    """ 
    if sys.platform == "linux":
        system("clear")
    elif sys.platform == "windows":
        system("cls")

    launching_path = Path.cwd()
    config_path = launching_path / config_file

    ground_station = []
    info_station = {}

    print("\n" + "-" * 30 + config_file.split("/")[-1].split(".")[0] + "-" * 30 + "\n")
    print("Reading files>", end="")
    with open(config_path) as config_yaml:
        info_input = yaml.safe_load(config_yaml)

    if server : 
        orbit_sat_file = (
            config_path.parent / ".." / "input" / info_input["input_trajectory_name"]
        )  # --> sat orbit CIC file in GCRS or local ICRS Earth center Frame. After TRAJ_Formatting.m.
        path_inter = (
            config_path.parent / ".." / "output" / info_input["intervisibility_output_name"]
        )  # output file

    else : 
        orbit_sat_file = (
            config_path.parent / info_input["input_trajectory_name"]
        )  # --> sat orbit CIC file in GCRS or local ICRS Earth center Frame. After TRAJ_Formatting.m.
        path_inter = (
            config_path.parent / info_input["intervisibility_output_name"]
        )  # output file

    for station in info_input["stations"]:

        actual_station = info_input["stations"][station]

        ground_station.append(
            [
                str(actual_station["name"]),
                EarthLocation(
                    lat=float(actual_station["latitude"]) * u.deg,
                    lon=float(actual_station["longitude"]) * u.deg,
                    height=float(actual_station["height"]) * u.m,
                ),
                float(actual_station["elevationLimit"]),
                str(actual_station["stationIndex"]),
            ]
        )
    ground_station = tuple(ground_station)
    # _________________________END Personal specification__________________________

    # read sat orbit ( in gcrs frame or local icrs earth center)
    #  in the future, it will be better to change function read_ccsds and remove this try and except*


    blockPrint()
    cic, origin, comment, header, data_time, data_science = cc.read_ccsds(
        orbit_sat_file
    )
    enablePrint()

    # convert CIC time to astropy time
    # print(data_time)

    time_vector = Time(tt.vector_julian_to_julian(data_time), format="mjd")

    # convert sat orbit in astropy GCRS position
    # print(time_vector)
    # print(type(time_vector))
    input_interpolation = read_cic(orbit_sat_file, slice(0, 5))
    print("Transforming inputs>", end="")
    if isclose(
        int(float(str((time_vector[1] - time_vector[0]) * 86400))),
        float(info_input["minimum_inter_event_duration"]),
        abs_tol=1,
    ):
        x_sat = data_science.T[0].tolist() * u.km
        y_sat = data_science.T[1].tolist() * u.km
        z_sat = data_science.T[2].tolist() * u.km

    else:

        new_time = arange(
            float(str(time_vector[0])),
            float(str(time_vector[-1])),
            float(info_input["minimum_inter_event_duration"]) / 86400,
        )

        interpolation_trajectory = interpolate_trajectory(new_time, input_interpolation)
        new_time = mjd1_to_mjd2(new_time)

        new_time[:, 1] = np.around(new_time[:, 1], 0)

        time_vector = Time(tt.vector_julian_to_julian(new_time), format="mjd")
        x_sat = interpolation_trajectory.T[2].tolist() * u.km
        y_sat = interpolation_trajectory.T[3].tolist() * u.km
        z_sat = interpolation_trajectory.T[4].tolist() * u.km

    # print(data_science.T[0].tolist())

    sat_gcrs = GCRS(
        obstime=time_vector,
        x=x_sat,
        y=y_sat,
        z=z_sat,
        representation_type=CartesianRepresentation,
        differential_type=CartesianDifferential,
    )

    # _________________________________START SCRIPT____________________________
    # produce Ground station intervisibility :

    print("Launching ground station process>", end="")
    (
        time_event_gs,
        event_type_gs,
        visibility_GS,
        sat_altaz_GS,
    ) = inter.ground_station_intervisibility(
        time_vector,
        sat_gcrs,
        ground_station,
        interpolation_pos_input=input_interpolation,
        event_duration=float(info_input["minimum_inter_event_duration"]),
        event_precision=float(info_input["event_precision"]),
        plot_data=0,
    )

    # produce Sun intervisibility

    print("Launching sun process>", end="")
    (
        time_event_sun,
        event_type_sun,
        sun_eph_sat_sky,
        sun_inter_sat,
        time_vector,
    ) = inter.sun_intervisibility(
        time_vector,
        sat_gcrs,
        interpolation_pos_input=input_interpolation,
        event_duration=float(info_input["minimum_inter_event_duration"]),
        event_precision=float(info_input["event_precision"]),
        plot=0,
    )

    # Add Sun event at GS event :
    event_type = np.append(event_type_gs, event_type_sun)
    time_event = np.append(time_event_gs, time_event_sun)

    # Write CIC file intervisibility

    inter.cic_intervisibility(time_event, event_type, config_file, path_inter)
    if info_input["EPS_output"] != None:
        if server :
            inter.eps_output(
                config_path.parent / ".." / "output" / info_input["EPS_output"],
                sun_eph_sat_sky,
                sun_inter_sat,
                time_vector
            )
        else : 
            inter.eps_output(
                config_path.parent / info_input["EPS_output"],
                sun_eph_sat_sky,
                sun_inter_sat,
                time_vector
            )

    print("\nEnd of processing")


# All commands #################################################

