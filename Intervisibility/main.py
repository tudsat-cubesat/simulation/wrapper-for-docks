from requirements import requirements
try :
	from launching_inter import launching_inter
	import click
except ImportError:
    requirements()
    import click
    from launching_inter import launching_inter

@click.command()
@click.option(
    "--config_file",
    help="config file of intervisibility module relative to exe--> yaml extension",
)
def intervisibility_cmd(config_file):
    try :
        launching_inter(config_file)
    except ImportError:
        requirements()
        launching_inter(config_file)

if __name__ == "__main__":
    intervisibility_cmd()
