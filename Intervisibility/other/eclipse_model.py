#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 14:04:13 2018

@author: developpeur

Visibilty of target in sky ( taking into account a list of objects)

"""

import numpy as np
from astropy import units as u

# from astropy.time import Time
from astropy.coordinates import SkyCoord, AltAz, ICRS
from astropy.coordinates import get_body_barycentric, EarthLocation
from astropy.coordinates import CartesianRepresentation, CartesianDifferential
from poliastro import bodies

# Object list : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter, Saturn,
# Uranus, Neptune, Pluto

objects_list = np.array(
    [
        "Sun",
        "Earth",
        "Moon",
        "Mercury",
        "Venus",
        "Mars",
        "Jupiter",
        "Saturn",
        "Uranus",
        "Neptune",
        "Pluto",
    ]
)


def eph_data(time, objects, ephemeride="de432s"):

    for i in range(len(objects)):
        if len(np.where(objects[i] == objects_list)[0]) == 0:
            raise NameError(
                objects[i]
                + " is not in the available objects list\n"
                + "Available Object list : "
                + str(objects_list)
            )

    body_eph_xyz = np.zeros((len(objects), 3))
    body_diam = np.zeros(len(objects))
    icrs_eph_pos = []
    for i in range(len(objects)):
        body_eph_xyz[i] = get_body_barycentric(objects[i], time, ephemeris=ephemeride)
        icrs_eph_pos.append(
            ICRS(
                body_eph_xyz[i][0],
                body_eph_xyz[i][0],
                body_eph_xyz[i][0],
                representation=CartesianRepresentation,
            )
        )

        if objects[i] == "Sun" or objects[i] == 10:
            body_diam[i] = bodies.Sun.R.base
        elif objects[i] == "Earth" or objects[i] == 399:
            body_diam[i] = bodies.Earth.R.base
        elif objects[i] == "Moon" or objects[i] == 301:
            body_diam[i] = bodies.Moon.R.base
        elif objects[i] == "Mercury" or objects[i] == 1:
            body_diam[i] = bodies.Mercury.R.base
        elif objects[i] == "Venus" or objects[i] == 2:
            body_diam[i] = bodies.Venus.R.base
        elif objects[i] == "Mars" or objects[i] == 4:
            body_diam[i] = bodies.Mars.R.base
        elif objects[i] == "Jupiter" or objects[i] == 5:
            body_diam[i] = bodies.Jupiter.R.base
        elif objects[i] == "Saturn" or objects[i] == 6:
            body_diam[i] = bodies.Saturn.R.base
        elif objects[i] == "Uranus" or objects[i] == 7:
            body_diam[i] = bodies.Uranus.R.base
        elif objects[i] == "Neptune" or objects[i] == 8:
            body_diam[i] = bodies.Neptune.R.base
        elif objects[i] == "Pluto" or objects[i] == 9:
            body_diam[i] = bodies.Pluto.R.base

    body_diam = (body_diam * u.m).to("km")

    return icrs_eph_pos, body_diam
