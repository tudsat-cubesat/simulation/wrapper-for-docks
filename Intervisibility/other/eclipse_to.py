#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 13:39:23 2018

@author: Sébastien Durand
"""
import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u

plt.ion()


def occulation_model_meter(Di, R1, R2, po=0):

    print(Di)
    print(R1)
    print(R2)

    if np.mean(R1) >= np.mean(R2):
        Rg = R1
        Rp = R2
    else:
        Rg = R2
        Rp = R1

    Sg = np.arccos(-1) * Rg ** 2
    Dia = np.abs(Di)
    S1 = np.zeros(len(Di))
    S2 = np.zeros(len(Di))

    for i in range(len(Di)):

        if Dia[i] >= 0 and Dia[i] <= Rg[i] - Rp[i]:
            h = 0
            dg = Rg[i]
            dp = -Rp[i]

        elif Dia[i] > Rg[i] - Rp[i] and Dia[i] <= (Rg[i] + Rp[i]):
            dg = (Rg[i] ** 2 + Dia[i] ** 2 - Rp[i] ** 2) / (2 * Dia[i])
            dp = Dia[i] - dg
            h = np.sqrt(Rg[i] ** 2 - dg ** 2)

        else:
            h = 0
            dg = Rg[i]
            dp = Rp[i]

        S1[i] = Rg ** 2 * np.arccos(dg / Rg) - (dg * h)
        S2[i] = Rp ** 2 * np.arccos(dp / Rp) - (dp * h)

    if po == 1:
        plt.figure()
        plt.plot(Di, (Sg - (S1 + S2)) / Sg)

    return (Sg - (S1 + S2)) / Sg


def occultation_model_rad(Di, R1, R2, po=0):

    # R1 = max 180°
    # R2 = max 180°
    # Di = max 180° et min -180°

    if R1 > np.pi * u.rad:
        raise ValueError("no have correct object angular size")
    if R2 > np.pi * u.rad:
        raise ValueError("no have correct object angular size")

    if Di > np.pi * u.rad:
        Di = Di - 2 * np.pi * u.rad
    elif Di < -1 * np.pi * u.rad:
        Di = 2 * np.pi * u.rad + Di
    else:
        Di = Di.to("rad")

    Di = Di.to("rad").value
    R1 = R1.to("rad").value
    R2 = R2.to("rad").value
    aa = occulation_model_meter(Di, R1, R2, po=po)

    return aa


# ____________________________________________
R1 = (np.pi / 4) * u.rad  # rayon angulaire du première objet
R2 = (np.pi / 2) * u.rad  # rayon angulaire du deuxième object
Di = np.arange(-np.pi, np.pi, 0.1) * u.rad
test = occulation_model_meter(
    Di.to("rad").value, R1.to("rad").value, R2.to("rad").value, po=1
)
