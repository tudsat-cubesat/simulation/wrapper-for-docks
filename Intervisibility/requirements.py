from os import system
import sys
from pathlib import Path
import subprocess


def pip_install_req(req="requirements.txt"):
    req_path = str(Path(__file__).parent / req)
    subprocess.check_call([sys.executable, "-m", "pip", "install","--user", "-r", req_path])


def pip_install_pkg(pkg: str):
    subprocess.check_call([sys.executable, "-m", "pip", "install","--user", pkg])

def test_version_libs(libs : list) :

    libs[libs.index("yaml")] = "pyyaml"
    libs_currently_installed = []
    for counter,lib in enumerate(libs) :
        try :
            outputs = subprocess.check_output([sys.executable, "-m", "pip", "show", lib])
            version_install = str(outputs).split('\\n')[1].split(' ')[1]
            libs_currently_installed.append(f"{lib}=={version_install} currently installed")
        except : 
            libs_currently_installed.append(f"{lib} not currently installed")
    return libs_currently_installed
    
def requirements():
    """
        Fonction which is call if intervisibility is called by the user,
        it will check if the user have all the dependencies and
        install them if not in order to run intervisibility module
    """
    path_file_previous_lib = Path(__file__).parent / "previous_lib.txt"
    libs = [
        "wheel",
        "numpy",
        "astropy",
        "astroquery",
        "scipy",
        "tqdm",
        "lxml",
        "matplotlib",
        "PyQt5",
        "pyquaternion",
        "yaml",
        "poliastro",
        "jplephem",
        "nptyping"
    ]


    libs_currently_installed = test_version_libs(libs)

    user_autorization = input("The program need to install this librairies in order to work:\n\n"
        f"\t{libs_currently_installed[0]} --> last version of wheel\n"
        f"\t{libs_currently_installed[1]} --> numpy==1.18.1\n"
        f"\t{libs_currently_installed[2]} --> astropy==3.1\n"
        f"\t{libs_currently_installed[3]} --> astroquery==0.4.1\n"
        f"\t{libs_currently_installed[4]} --> scipy==1.5.2\n"
        f"\t{libs_currently_installed[5]} --> tqdm==4.48.2\n"
        f"\t{libs_currently_installed[6]} --> lxml==4.5.2\n"
        f"\t{libs_currently_installed[7]} --> matplotlib==3.3.1\n"
        f"\t{libs_currently_installed[8]} --> pyqt5==5.12\n"
        f"\t{libs_currently_installed[9]} --> last version of pyquaternion\n"
        f"\t{libs_currently_installed[10]} --> last version of pyyaml\n"
        f"\t{libs_currently_installed[11]} --> poliastro==0.10.0\n"
        f"\t{libs_currently_installed[12]} --> last version of jplephem\n"
        f"\t{libs_currently_installed[13]} --> last version of nptyping\n"
        f"\tlast version of click will be installed \n\n"
        "would you like the program to install them for you ?\n"
        "If it's the case, a file will be created in order to keep your previous librairies at :\n" 
        f"{path_file_previous_lib} y/n ")

    with open(str(path_file_previous_lib),"w") as previous_lib:
        for lib in libs_currently_installed :
            if "not currently installed" in lib :
                pass
            else :
                previous_lib.write(f"{lib.split('currently installed')[0]}\n")

    if user_autorization == "y" or user_autorization == "Y" :
        pip_install_pkg("numpy==1.18.1")
        pip_install_pkg("wheel")
        pip_install_req()
    else :
        print("abord")
        sys.exit()
