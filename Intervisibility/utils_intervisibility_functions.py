import os
import sys
from nptyping import NDArray
from typing import Tuple
from docks_helper import interpolate_trajectory, mjd1_to_mjd2, arange
import numpy as np
from astropy.time import Time
from itertools import product
import time_function as tt
from astropy import units as u
from astropy.coordinates import GCRS
from astropy.coordinates import CartesianRepresentation, CartesianDifferential


def check_event_precision(
    time_event: NDArray[float],
    interpolation_pos_input: NDArray[float],
    event_duration: int,
    precision_event: int,
) -> Tuple[GCRS, NDArray[float]]:
    """
    create new GCRS & time vector with the proper precision
    """
    for counter, value in enumerate(time_event):
        if counter == 0:
            new_time = arange(
                float(str(value - event_duration / 86400)),
                float(str(value)),
                precision_event / 86400,
            )
        else:
            new_time = np.append(
                new_time,
                arange(
                    float(str(value - event_duration / 86400)),
                    float(str(value)),
                    precision_event / 86400,
                ),
            )

    n = 0

    while new_time[n] < time_event[0]:
        n += 1
    for deletion_index in range(0, n):
        new_time = np.delete(new_time, 0)

    interpolation_trajectory = interpolate_trajectory(new_time, interpolation_pos_input)

    new_time = mjd1_to_mjd2(new_time)
    new_time = np.round(new_time, 0)

    new_time[:, 1] = np.around(new_time[:, 1], 0)

    new_time = Time(tt.vector_julian_to_julian(new_time), format="mjd")

    x_sat = interpolation_trajectory.T[2].tolist() * u.km
    y_sat = interpolation_trajectory.T[3].tolist() * u.km
    z_sat = interpolation_trajectory.T[4].tolist() * u.km
    sat_gcrs = GCRS(
        obstime=new_time,
        x=x_sat,
        y=y_sat,
        z=z_sat,
        representation_type=CartesianRepresentation,
        differential_type=CartesianDifferential,
    )

    return sat_gcrs, new_time


def check_event_duration(
    event_type: NDArray[str],
    time_event: NDArray[float],
    event_duration: int,
    type_intervisibility: str,
    number_station: int = 1,
) -> Tuple[NDArray[str], NDArray[float]]:
    """
    Check if the event duration is minimum the amount specified by the user in the config file
    """
    new_time_event = np.array([])
    new_event_type = np.array([])
    sorted_new_event_type = np.array([])
    sorted_new_time_event = np.array([])

    stations = list(range(1, number_station + 1))
   
    check_change = 0

    for station, event in product(stations, event_type):
        change = station

        if check_change != change:
            counter = 0
            check_change = change
        if int(event[-5:]) == station or number_station == 1:

            sorted_new_event_type = np.append(sorted_new_event_type, event)
            sorted_new_time_event = np.append(
                sorted_new_time_event, time_event[counter]
            )

        counter += 1

    check_change = 0

    for counter, value in enumerate(sorted_new_event_type):
        change = int(value[-5:])

        if (
            event_type[0][:6] == type_intervisibility
            and check_change != change
            and (number_station != 1 or (number_station == 1 and counter == 0))
        ):
            scenario = 1
            check_change = change
            counter_different_station = 0
        elif (
            event_type[0][:6] != type_intervisibility
            and check_change != change
            and (number_station != 1 or (number_station == 1 and counter == 0))
        ):
            scenario = 2
            check_change = change
            counter_different_station = 0

        if (
            scenario == 1
            and counter_different_station % 2 == 1
            and sorted_new_time_event[counter_different_station]
            - sorted_new_time_event[counter_different_station - 1]
            > (float(event_duration) - 1) / 86400
        ):
            new_event_type = np.append(
                new_event_type, sorted_new_event_type[counter - 1]
            )
            new_time_event = np.append(
                new_time_event, sorted_new_time_event[counter - 1]
            )
            new_event_type = np.append(new_event_type, value)
            new_time_event = np.append(new_time_event, sorted_new_time_event[counter])

        elif scenario == 2 and counter_different_station == 0:

            new_event_type = np.append(new_event_type, value)
            new_time_event = np.append(new_time_event, sorted_new_time_event[counter])
        elif (
            scenario == 2
            and counter_different_station % 2 == 0
            and sorted_new_time_event[counter] - sorted_new_time_event[counter - 1]
            > (float(event_duration) - 1) / 86400
        ):
            new_event_type = np.append(
                new_event_type, sorted_new_event_type[counter - 1]
            )
            new_time_event = np.append(
                new_time_event, sorted_new_time_event[counter - 1]
            )
            new_event_type = np.append(new_event_type, value)
            new_time_event = np.append(new_time_event, sorted_new_time_event[counter])

        if len(sorted_new_event_type) - 1 == counter:

            if value[:6] == type_intervisibility:
                new_event_type = np.append(new_event_type, value)
                new_time_event = np.append(
                    new_time_event, sorted_new_time_event[counter]
                )

        elif (
            int(sorted_new_event_type[counter + 1][-5:]) != change
            and value[:6] == type_intervisibility
            and number_station != 1
        ):
            new_event_type = np.append(new_event_type, value)
            new_time_event = np.append(new_time_event, sorted_new_time_event[counter])
        counter_different_station += 1

    new_order = np.argsort(new_time_event)
    event_type = new_event_type[new_order]
    time_event = new_time_event[new_order]

    return event_type, time_event


def blockPrint():
    return
    sys.stdout = open(os.devnull, "w")


# Restore
def enablePrint():
    return
    sys.stdout = sys.__stdout__
