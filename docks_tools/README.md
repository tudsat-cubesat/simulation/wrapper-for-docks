DOCKS_Tools module, __v1.3__

Date of modification : *28-Jan-2021*

Authors
========

Development by Observatory of Paris - PSL Université Paris - 
Laboratory of Excellence ESEP, PSL Space Pole [CENSUS](https://cceres.psl.eu/?lang=en) 

2020: Rashika JAIN, Boris SEGRET

Past contributors: Janis DALBINS (2019), Sebastien DURAND (2019)

&nbsp;

----

[TOC]

----

&nbsp;

# 1.	Installing DOCKS Tools (only for DOCKS Propagator users)

1. Considering that `<PROJECT>` is the path to your local workspace, create there a subfolder __"DOCKS"__ to store all functional modules.
2. Download the full module from the **v0.2** link  provided in `Gitlab > Repository > Tags"` and decompress as `<PROJECT>/DOCKS/docks_tools` subfolder.
3. Do the rest of the installation procedure below, even if you already did it for a previous version of the docks_tools module.

<a name="linux_install"></a>

## 1.1	Linux Users

System requirements: Ubuntu 16.04 or 18.04 64-bits

1. Run the following command in an ubuntu terminal:
    `$ sudo apt update`
2. Continue the installation based on your choice of Python [with](#linux_with_conda) or [without](#linux_without_conda) Anaconda. For users who are not familiar with this, continue the installation **without** Anaconda.

<a name="linux_without_conda"></a>

### 1.1.1	Without Anaconda/ Miniconda

1. Install python 3 and its basic libraries for your system. Run the following commands in an ubuntu terminal:
    `$ sudo apt install -y python3`
    `$ sudo apt install -y python3-pip python3-venv`
    
    for users without sudo priveleges:
    `$ pip3 install virtualenv`
    
2. Now change the directory of the terminal to your `<PROJECT>/DOCKS/docks_tools` folder. Run the bash script __"install_linux_docks.sh"__ using the command:
    `$ bash ./install_linux_docks.sh`
3. If everything went well you __must__ close the terminal before using DOCKS for the last settings to take effect.
4. Now, if you open the __"DOCKS"__ folder, there is a new folder called __"docks_python_env"__, this is the virtual environment for DOCKS. Always run DOCKS in this environment. Read section 2 ([link](#use_venv )) to learn how to use it.

<a name="linux_with_conda"></a>

### 1.1.2	With Anaconda/ Miniconda

1. Change the directory of the terminal to your `<PROJECT>/DOCKS/docks_tools` folder. Run the bash script __"install_linux_docks_conda.sh"__ using the command:
   `$ bash ./install_linux_docks_conda.sh`
2. The script asks if you have Anaconda/Miniconda already installed. If your system does not have any conda installation then the script will install Anaconda3, 2020 release at your root directory `/home/$USER/`.
3. If everything went well you __must__ close the terminal before using DOCKS for the last settings to take effect.
4. Now, if you go to __"/home/$USER/anaconda3/envs/"__ folder, there is a new folder called __"docks_python_env"__, this is the virtual environment for DOCKS. Always run DOCKS in this environment. Read section 2 ([link](#use_venv )) to learn how to use it.

&nbsp;
<a name="windows_install"></a>

## 1.2	MS-Windows Users

**Note**: *Windows 10 users can also use the "Windows subsystem for Linux" and follow the instructions given in the [Linux](#linux_install) section (this installation is not yet tested). The user will have to do some additional research to make GUI work in this case. In such case ignore this section, otherwise continue.*

1. Continue the installation based on your choice of Python - [with](#win_with_conda) or [without](#win_without_conda) Anaconda. For users who are not familiar with this, continue the installation [**without**](#win_without_conda) Anaconda.

<a name="win_without_conda"></a>

### 1.2.1	Without Anaconda/ Miniconda

1. Download and install Python 3, if you do not have it.
    * Download the latest version of [Python 3](https://www.python.org/downloads/windows/). You __must__ download 64-bit Python for 64-bit windows.
    * Select the Option __"Add Python to Path"__ while installing Python.
2. Check your Python installation by opening a command prompt and typing `python`. You must see before the python prompt "Python 3.x.x (....) \[MSC v. ....] on win32". If so, then close the window.
3. Go to the directory `<PROJECT>/DOCKS/docks_tools`.
4. Run the file `"install_win64_docks.bat"` **as administrator**.
5. DOCKS creates a virtual environment __"docks_python_env"__, always run DOCKS in this environment. Read section 2 ([link](#use_venv )) to learn how to use it.

<a name="win_with_conda"></a>

### 1.2.2	With Anaconda/ Miniconda

1. Install the latest Anaconda 3 or Miniconda 3 (with path add) according to your personal preferences.
Check that your install is recognized by opening an Anaconda prompt and typing `python`:
you must see before the python prompt "Python 3.x.x | Anaconda, Inc|...". If so, then close the window.
2. Open an Anaconda prompt **as administrator**. From this terminal, change the directory to the `<PROJECT>/DOCKS/docks_tools` folder.
3. Run the file by typing `"install_win64_docks_conda.bat"`.
4. Answer yes if any questions are asked.
5. DOCKS creates a virtual environment __"docks_python_env"__, always run DOCKS in this environment. Read section 2 ([link](#use_venv )) to learn how to use it.

&nbsp;
<a name="use_venv"></a>

#2.	Using the virtual environment

DOCKS takes the benefit of python virtual environment (hereafter, referred to as venv) to install project specific libraries only. Hence, DOCKS creates a directory (default name is **"docks_python_env"**) at the time of the installation. The location of this directory depends on the installation procedure followed by the user (see below). This leads to a clean installation as the user can just delete this directory to uninstall DOCKS.

* The default name of the venv is **"docks_python_env"**. If needed, the user can change the name of the venv in the installation script.
* To use the venv, you just need to activate it and then continue using python as you will do without venv. (see below)
* <u>Always remember to use this venv to run DOCKS.</u>


#### Important information about venv

<u>Without anaconda:</u>

* **Location** of venv directory:
"docks_python_env" directory in the `<PROJECT>/DOCKS/`
* The **activation** of venv is simplified during the installation process by creating alias in the bashrc/regedit for linux/window. To activate the venv python, type the following command in the terminal (*the location of the terminal does not matter*):
`$ docks_python`
* To **deactivate** the venv, type `$ deactivate`
* To **install new libraries** in this venv, first activate the venv then use:
	- For linux: `$ python3 -m pip install <name>` 
	- For windows: `$ py -m pip install <name>` 

<u>With conda:</u>

* **Location** of venv directory:
"docks_python_env" directory in the `<Anaconda installation location>/anaconda3/envs/`
* To **activate** the conda venv python, type the following command in the terminal (*the location of the terminal does not matter*):
Use terminal for linux or anaconda prompt for windows.
	`$ conda activate docks_python_env <name>`
* To **deactivate** the venv, type `$ deactivate`
* To **install new libraries** in this venv, use:
	- For conda packages: `$ conda install -n docks_python_env <name>` 
	- For pip packages, first activate the conda venv, then use: `$ pip install <name>`
