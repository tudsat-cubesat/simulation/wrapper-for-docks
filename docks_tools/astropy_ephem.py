#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 01:05:15 2019

@author: Sébastien Durand
"""

from astropy.coordinates import CartesianRepresentation
from astropy.coordinates import CartesianDifferential
from astropy.coordinates import ICRS, GCRS
from astropy.coordinates import get_body_barycentric_posvel
from astropy.coordinates import solar_system_ephemeris
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time_function as tt
import ccsds as cc

solar_system_ephemeris.set('de432s')
plt.ion()  # To immediately show plots


def astropy_eph_body(object_name, time_vector, frame='ICRS', plot=0):
    """
    production position and speed for body with astropy (de432s)

    Available Body : mercury, venus, earth, moon, jupiter, saturn, uranus, neptune and sun

    :param object_name:(string) Body name
    :param time_vector:(Astropy Time) Compute Time
    :param frame:(string) Frame for output (GCRS or ICRS)
    :param plot:(int) Option for plot body position

    :return:time_vector:(Astropy Time) Compute Time
            object_eph:(Astropy Position) Body Position in frame
    """
    object_eph = get_body_barycentric_posvel(object_name, time_vector, ephemeris='de432s')

    object_eph = ICRS(x=object_eph[0].x, y=object_eph[0].y, z=object_eph[0].z,
                      v_x=object_eph[1].x, v_y=object_eph[1].y, v_z=object_eph[1].z,
                      representation_type=CartesianRepresentation,
                      differential_type=CartesianDifferential)
    if frame == 'GCRS':
        print("convert body position in GCRS")
        object_eph = object_eph.transform_to(GCRS(obstime=time_vector, representation_type=CartesianRepresentation,
                                                  differential_type=CartesianDifferential))
    elif frame == 'ICRS':
        print("convert body position in ICRS")

    else:
        raise ValueError("not support this frame, just ICRS and GCRS")

    if plot == 1:
        object_eph_xyz = object_eph.cartesian
        fig_xyz = plt.figure()
        ax = fig_xyz.add_subplot(111, projection='3d')
        ax.scatter(object_eph_xyz.x.to('km').value,
                   object_eph_xyz.y.to('km').value,
                   object_eph_xyz.z.to('km').value,
                   c='black')

        ax.set_xlabel('X (km)')
        ax.set_ylabel('Y (km)')
        ax.set_zlabel('Z (km)')

    return time_vector, object_eph


def generate_eph_cic_xyz(time_vector, xyz_c, center_body='Earth',
                         output_frame="local_body_ICRS", output_path="",
                         origin_in="DOCKS", object_name="Moon", object_id="Earth_Moon",
                         time_system="utc"):

    """
    :param time_vector:(Astropy Time) Compute Time
    :param xyz_c:
    :param center_body:(string) Name of Body in center of frame
    :param output_frame:(string) Name of frame
    :param output_path:(string) Name of path and file for cic file
    :param origin_in:(string) Name of producer of file
    :param object_name:(string) Object name
    :param object_id:(string) name of the object identifier
    :param time_system:(string) name of time type ( utc or tt)

    :return: no have return
    """
    data = xyz_c.T.value

    cic = "CIC_OEM_VERS = 1.0"
    origin = "ORIGINATOR = " + origin_in

    comment = ["COMMENT ="]
    header = ["OBJECT_NAME = " + object_name,
              "OBJECT_ID = " + object_id,
              "CENTER_NAME = " + center_body,
              "REF_FRAME = " + output_frame,
              "TIME_SYSTEM = " + time_system]

    time_vector = tt.julian_to_vector_julian(time_vector.mjd)

    sequence = ["%d", "%.6f", "%.8f", "%.8f", "%.8f"]
    cc.write_ccsds(output_path, cic, origin, comment, header, sequence,
                   time_vector, data)

    return "File generation successful"
