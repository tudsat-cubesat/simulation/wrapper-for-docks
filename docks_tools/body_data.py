#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 18:34:27 2019

@author: developpeur
"""

import numpy as np
from astroquery.jplhorizons import conf
import spice_body_acess as imm

conf.horizons_server = 'https://ssd.jpl.nasa.gov/horizons_batch.cgi'
from astroquery.jplhorizons import Horizons


def body_position_speed_horizon(epoch, naif_id):

    """
    :param epoch:
    :param naif_id:
    :return:
    """

    if ((naif_id >= 0) and (naif_id < 999)):
        body_type = "majorbody"
    elif ((naif_id > 2000000) and (naif_id < 3000000)):
        body_type = "smallbody"

    if len(epoch > 400):
        reste = epoch % 400
        div = np.int(epoch / 400)
        for i in range(div):
            param = Horizons(id=naif_id,
                             location="500@0",
                             epochs=epoch,
                             id_type=body_type).vectors()
            speed = [param['vx'], param['vy'], param['vz']]
            position = [param['x'], param['y'], param['z']]

        ep_fin = epoch[-1 * reste:-1]
        param = Horizons(id=naif_id,
                         location="500@0",
                         epochs=ep_fin,
                         id_type=body_type).vectors()
        speed = [param['vx'], param['vy'], param['vz']]
        position = [param['x'], param['y'], param['z']]
    else:

        param = Horizons(id=naif_id,
                         location="500@0",
                         epochs=epoch,
                         id_type=body_type).vectors()
        speed = [param['vx'], param['vy'], param['vz']]
        position = [param['x'], param['y'], param['z']]

    return position, speed


def body_position_speed_spice(epoch, naif_id, ep_type='jpl', custom_spice=[]):
    calceph_object = imm.calceph_eph_import(ep_type=ep_type, add_file=custom_spice)
    [x, y, z, vx, vy, vz] = imm.eph_object_SSB(calceph_object, naif_id, epoch, order=1)
    eph_astropy = imm.calceph2astropy_icrf(x.astype(np.float), y.astype(np.float), z.astype(np.float),
                                           vx=vx.astype(np.float), vy=vy.astype(np.float), vz=vz.astype(np.float))

    return eph_astropy


def convert_string_to_naifid(body_name):
    if body_name == 'Sun':
        naif_ID = 10
    elif body_name == 'Earth':
        naif_ID = 399
    elif body_name == 'Moon':
        naif_ID = 301
    elif body_name == 'Mercury':
        naif_ID = 199
    elif body_name == 'Venus':
        naif_ID = 299
    elif body_name == 'Mars':
        naif_ID = 499
    elif body_name == 'Jupiter':
        naif_ID = 599
    elif body_name == 'Saturn':
        naif_ID = 699
    elif body_name == 'Uranus':
        naif_ID = 799
    elif body_name == 'Neptune':
        naif_ID = 899
    elif body_name == 'Pluto':
        naif_ID = 999
    else:
        raise ValueError('this name is not support')

    return naif_ID
