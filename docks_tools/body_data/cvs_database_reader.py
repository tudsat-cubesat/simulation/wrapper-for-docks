#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 15:40:16 2018

@author: developpeur
"""

import pandas as pd
import numpy as np
from astropy import units as u
from astroquery.jplsbdb import SBDB

path="./data/"
j2_file="planetary_j2_value.csv"
planet_file="planete_phy.csv"
planet_ref_file="planet_ref_phy.csv"
sat_earth="sat_earth_phy.csv"
sat_mars="sat_martian_phy.csv"
sat_jupiter="sat_jovian_phy.csv"
sat_saturn="sat_saturnian_phy.csv"
sat_uranus="sat_urania_phy.csv"
sat_neptune="sat_neptunian_phy.csv"
sat_pluton="sat_pluto_phy.csv"
sat_ref_file="sat_ref_phy.csv"

from astropy.constants import G
G_v = G.value

def read_ref_planet(ref_text,ref_type="planet"):
    
    search=0
    if ref_type == "planete":
        ref_file=planet_ref_file
    elif ref_type == "sat":
        ref_file=sat_ref_file

    file_reader = pd.read_csv(ref_file, delimiter=';')
    for i in range(len(file_reader['ref_number'])):
        if file_reader['ref_number'][i] == ref_text:
            ref = file_reader['ref'][i]
            search=1
    if search == 0:
        raise ValueError("this reference not exist") 
    
    return ref


def read_mass_database(naif_id):
    
    search=0
    file=""
    if ((naif_id >= 301) and (naif_id < 400)):
        file=sat_earth
    elif ((naif_id >= 401) and (naif_id < 500)):
        file=sat_mars
    elif ((naif_id >= 501) and (naif_id < 600)):
        file=sat_jupiter
    elif ((naif_id >= 601) and (naif_id < 700)):
        file=sat_saturn
    elif ((naif_id >= 701) and (naif_id < 800)):
        file=sat_uranus
    elif ((naif_id >= 801) and (naif_id < 900)):
        file=sat_neptune
    elif ((naif_id >= 901) and (naif_id < 1000)):
        file=sat_pluton
    elif ((naif_id > 2000000) and (naif_id < 3000000)):
        search = 1
        print("is small body")
        param = SBDB.query(naif_id, phys=True)
        name = param['object']['fullname'].split(" ")[1]
        param = param['phys_par']
        if 'diameter' in param:
            diam = param['diameter'].to("km")*u.Unit("km")
            print(diam)
            delta_diam = param['diameter_sig'].to("km")*u.Unit("km")
            R = diam/2.
            R_p= delta_diam/2.
            Rref = param['diameter_ref']
            if 'GM' in param:
                k = param['GM'].to("km3/s2")*u.Unit("km3/s2")
                k_p = param['GM_sig'].to("km3/s2")*u.Unit("km3/s2")
                kref = param['GM_ref']

            elif 'density' in param:
                mu = param['density'].to("g/cm3")*u.Unit("g/cm3")
                print(mu)
                delta_mu = param['density_sig'].to("g/cm3")*u.Unit("g/cm3")
                print(delta_mu)
                mass = ((4*np.pi/3)*(R.to("m")**3)*mu.to("kg/m3"))
                print(mass)
                delta_mass = mass*((R_p.to("m")/R.to("m"))+(delta_mu.to("kg/m3")/mu.to("kg/m3")))
                print(delta_mass)
                k = (mass*G).to("km3/s2")
                k_p = (delta_mass*G).to("km3/s2")
                kref = param['density_ref']
                print("this small body have just density")
                print("GM is compute using density and diameter")
            else:
                print("this small body no have GM or density")
                raise ValueError("no have GM or density")
        else:
            print("the small database does not have enough data on this small body")
            raise ValueError("no have diameter, GM or density")

        
    if file != "":
        file_reader = pd.read_csv(file, delimiter=';')
        for j in range(len(file_reader["NAIF_ID"])):
            if np.int32(file_reader["NAIF_ID"][j]) == naif_id:
                search=1
                k=np.float(file_reader["GM"][j])*u.Unit(file_reader["GM (unit)"][j])
                k_p=np.float(file_reader["GM (precision)"][j])*u.Unit(file_reader["GM (unit)"][j])
                kref=read_ref_planet(file_reader["GM (ref)"][j],ref_type="sat")
                R=np.float(file_reader["Mean radius"][j])*u.Unit(file_reader["Mean radius (unit)"][j])
                R_p=np.float(file_reader["Mean radius (precision)"][j])*u.Unit(file_reader["Mean radius (unit)"][j])
                Rref=read_ref_planet(file_reader["Mean radius (ref)"][j],ref_type="sat")
                name=file_reader["Sat."][j]
                
    if search == 0:
        raise ValueError("this NAIF_ID not exist") 
    
    
    return naif_id, name, k, k_p, kref, R, R_p, Rref


def read_J2_database(naif_id):
    search=0

    reader = pd.read_csv(j2_file, delimiter=';')
    for j in range(len(reader["NAIF_ID"])):
        if np.int32(reader["NAIF_ID"][j]) == naif_id:
            search=1
            j2=np.float(reader["J2 Values"][j])
            j2_ref=reader["ref"][j]
            name=reader["Body name"][j]
    
    if search == 0:
        raise ValueError("No have J2 for this NAIF_ID")
    
    return naif_id, name, j2, j2_ref
    