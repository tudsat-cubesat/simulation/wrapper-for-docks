#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 11:30:18 2018

@author: developpeur
"""
from astropy.time import Time
from astropy.coordinates import ICRS, HCRS, HeliocentricTrueEcliptic, BarycentricTrueEcliptic, GCRS
from astropy.coordinates import CartesianRepresentation, CartesianDifferential
from astropy import units as u
from astropy.coordinates import Angle
import numpy as np

import body_data as bd
import eph_text_acess as file_eph

second_day = 24 * 60 * 60


# convert local frame (x,y and z align ti ICRS frame) to ICRS or
# HeliocentricTrueEcliptic or BarycentricTrueEcliptic or HCRS (Helio ICRS)
# Norm : IAU 2006, IERS 2010
# eph available : de430 and de432s (INPOP17a futur feature)
# default de430t

# Use Astropy for frame convertion


def Inc_for_sun_synchronous_earth(a, unit_a='km'):
    a = a * u.Unit(unit_a)

    theta = 1.9910e-7 * u.Unit('s-1')
    b = -2 / 3
    j2 = 1.08e-3
    re = 6378.14 * u.Unit('km')
    mu = 398600.44 * u.Unit('km3/s2')
    i = Angle(np.arccos(b * (theta / j2) * (a / re) ** 2 * np.sqrt(a ** 3 / mu)), u.radian)

    return i


def ICRS_convert(central_body, epoch, position, speed, ephemeride="jpl", custom_spice=[], central_body_ephe_file=""):
    """
    Convert Position and Speed body in Local ICRS frame (center is center body)
    to a position and Speed body in ICRS frame (Solar system Barycenter). 
    
    Input:
        central_body (string or int): string for body or int for NAIFID
        epoch (Float array, Nx1, astropy Time): Date for convert
        position (Float array, Nx3, astropy unit): body position in local frame (unit = km)
        speed (float array, Nx3, astropy unit): body speed in local frame (unit = km/s)
        ephemeride (string): Name for ephemeride need for convert : custom, 
        custom_spice (list of string): path for add customs spices for compute new bodies orbits
        central_body_ephe_file (string): path for custom body orbit file
        
    Output:
        icrs_traj:(Astropy object, in ICRS frame)
            
    All Output is in same format/sale or unit of Input
        
    Body available : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter,
    Saturn, Uranus, Neptune, Pluto
    For all of this body, center = Geometric center
    (because is keplerian orbit)
    NAIFID available : all major body and all naif id in custom_spice
    
    """

    # solar_system_ephemeris.set(ephemeride)

    # generate ephemeride
    # body_eph_xyz, body_eph_v = get_body_barycentric_posvel(central_body,epoch,ephemeris=ephemeride)
    if ephemeride == "custom":

        fp, fv = file_eph.red_eph_file(central_body_ephe_file)
        eph_central_body = file_eph.resample_traj(epoch, fp, vel=fv)

    else:

        naif_id = bd.convert_string_to_naifid(central_body)
        eph_central_body = bd.body_position_speed_spice(epoch, naif_id, ep_type=ephemeride, custom_spice=custom_spice)

    # Change local ICRS frame to ICRS frame    
    icrs_traj = ICRS(x=position[0] + eph_central_body.x,
                     y=position[1] + eph_central_body.y,
                     z=position[2] + eph_central_body.z,
                     v_x=speed[0] + eph_central_body.v_x,
                     v_y=speed[1] + eph_central_body.v_y,
                     v_z=speed[2] + eph_central_body.v_z,
                     representation_type=CartesianRepresentation,
                     differential_type=CartesianDifferential)

    return icrs_traj


def to_ICRS(central_body, epoch, position, speed,
            ephemeride="jpl", custom_spice=[], central_body_ephe_file=""):
    """
    Convert Position and Speed body in Local ICRS frame (center is center body)
    to a position and Speed body in ICRS frame (Solar system Barycenter). 
    
    Input:
        central_body (string or int): string for body or int for NAIFID
        epoch : (Float array, Nx1, astropy Time) Date for convert
        position : (Float array, Nx3, astropy unit) body position in local frame (unit = km)
        speed : (float array, Nx3, astropy unit) body speed in local frame (unit = km/s)
    
    Output:
        epoch : (Float array, Nx1, astropy Time) Same date as input
        position : (Float array, Nx3, astropy unit) body position in ICRS frame (unit = km)
        speed : (float array, Nx3, astropy unit) body speed in ICRS frame (unit = km/s)
        
        All Output is in same format/sale or unit of Input
        
    Body availlable : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter,
    Saturn, Uranus, Neptune, Pluto
    For all of this body, center = Geometric center
    (because is keplerian orbit)
    NAIFID availlable : all major body and all naif id in custom_spice
    
    """

    icrs_traj = ICRS_convert(central_body, epoch, position, speed, ephemeride=ephemeride,
                             custom_spice=custom_spice, central_body_ephe_file=central_body_ephe_file)

    return epoch, icrs_traj.data.xyz, icrs_traj.velocity.d_xyz


def to_HTE(central_body, epoch, position, speed,
           equinox=Time("J2000"), ephemeride="jpl", custom_spice=[], central_body_ephe_file=""):
    """
    Convert Position and Speed body in Local ICRS frame (center is center body)
    to a position and Speed body in HeliocentricTrueEcliptic (HTE) frame (Heliocenter). 
    
    Input:
        central_body (string or int): string for body or int for NAIFID
        epoch : (Float array, Nx1, astropy Time) Date for convert
        position : (Float array, Nx3, astropy unit) body position in local frame
        speed : (float array, Nx3, astropy unit) body speed in local frame
    
    Output:
        epoch : (Float array, Nx1, astropy Time) Same date as input
        position : (Float array, Nx3, astropy unit) body position in HTE frame
        speed : (float array, Nx3, astropy unit) body speed in HTE frame
        
        All Output is in same format/sale or unit of Input
        
    Body availlable : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter,
    Saturn, Uranus, Neptune, Pluto
    For all of this body, center = Geometric center
    (because is keplerian orbit)
    NAIFID availlable : all major body and all naif id in custom_spice
    
    """
    icrs_traj = ICRS_convert(central_body, epoch, position, speed, ephemeride=ephemeride,
                             custom_spice=custom_spice, central_body_ephe_file=central_body_ephe_file)

    hte_traj = icrs_traj.transform_to(HeliocentricTrueEcliptic(equinox=equinox,
                                                               obstime=epoch,
                                                               representation_type=CartesianRepresentation,
                                                               differential_type=CartesianDifferential))

    return epoch, hte_traj.data.xyz, hte_traj.velocity.d_xyz


def to_HCRS(central_body, epoch, position, speed,
            ephemeride="jpl", custom_spice=[], central_body_ephe_file=""):
    """
    Convert Position and Speed body in Local ICRS frame (center is center body)
    to a position and Speed body in HCRS frame (Heliocenter). 
    
    Input:
        central_body (string or int): string for body or int for NAIFID
        epoch : (Float array, Nx1, astropy Time) Date for convert
        position : (Float array, Nx3, astropy unit) body position in local frame
        speed : (float array, Nx3, astropy unit) body speed in local frame
    
    Output:
        epoch : (Float array, Nx1, astropy Time) Same date as input
        position : (Float array, Nx3, astropy unit) body position in HCRS frame
        speed : (float array, Nx3, astropy unit) body speed in HCRS frame
        
        All Output is in same format/sale or unit of Input
        
    Body availlable : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter,
    Saturn, Uranus, Neptune, Pluto
    For all of this body, center = Geometric center
    (because is keplerian orbit)
    NAIFID availlable : all major body and all naif id in custom_spice
    
    """
    icrs_traj = ICRS_convert(central_body, epoch, position, speed, ephemeride=ephemeride,
                             custom_spice=custom_spice, central_body_ephe_file=central_body_ephe_file)

    hcrs_traj = icrs_traj.transform_to(HCRS(obstime=epoch,
                                            representation_type=CartesianRepresentation,
                                            differential_type=CartesianDifferential))

    return epoch, hcrs_traj.data.xyz, hcrs_traj.velocity.d_xyz


def ICRS_to_HCRS(central_body, epoch, position, speed,
                 ephemeride="jpl", custom_spice=[], central_body_ephe_file=""):
    """
    Convert Position and Speed body in Local ICRS frame (center is center body)
    to a position and Speed body in HCRS frame (Heliocenter).

    Input:
        central_body (string or int): string for body or int for NAIFID
        epoch : (Float array, Nx1, astropy Time) Date for convert
        position : (Float array, Nx3, astropy unit) body position in local frame
        speed : (float array, Nx3, astropy unit) body speed in local frame

    Output:
        epoch : (Float array, Nx1, astropy Time) Same date as input
        position : (Float array, Nx3, astropy unit) body position in HCRS frame
        speed : (float array, Nx3, astropy unit) body speed in HCRS frame

        All Output is in same format/sale or unit of Input

    Body availlable : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter,
    Saturn, Uranus, Neptune, Pluto
    For all of this body, center = Geometric center
    (because is keplerian orbit)
    NAIFID availlable : all major body and all naif id in custom_spice

    """
    icrs_traj = ICRS_convert(central_body, epoch, position, speed, ephemeride=ephemeride,
                             custom_spice=custom_spice, central_body_ephe_file=central_body_ephe_file)
    sun_eph = bd.body_position_speed_spice(epoch, 10, ep_type=ephemeride, custom_spice=custom_spice)
    new_x = icrs_traj.x - sun_eph.x
    new_vx = icrs_traj.v_x - sun_eph.v_x
    new_y = icrs_traj.y - sun_eph.y
    new_z = icrs_traj.z - sun_eph.z
    new_vy = icrs_traj.v_y - sun_eph.v_y
    new_vz = icrs_traj.v_z - sun_eph.v_z

    hcrs_traj = HCRS(obstime=epoch, x=new_x, y=new_y, z=new_z, v_x=new_vx, v_y=new_vy, v_z=new_vz,
                     representation_type=CartesianRepresentation,
                     differential_type=CartesianDifferential)

    return epoch, hcrs_traj.data.xyz, hcrs_traj.velocity.d_xyz


def to_BTE(central_body, epoch, position, speed,
           equinox=Time("J2000"), ephemeride="jpl", custom_spice=[], central_body_ephe_file=""):
    """
    Convert Position and Speed body in Local ICRS frame (center is center body)
    to a position and Speed body in BarycentricTrueEcliptic(BTE) frame (Solar system Barycenter). 
    
    Input:
        central_body (string or int): string for body or int for NAIFID
        epoch : (Float array, Nx1, astropy Time) Date for convert
        position : (Float array, Nx3, astropy unit) body position in local frame
        speed : (float array, Nx3, astropy unit) body speed in local frame
    
    Output:
        epoch : (Float array, Nx1, astropy Time) Same date as input
        position : (Float array, Nx3, astropy unit) body position in BTE frame
        speed : (float array, Nx3, astropy unit) body speed in BTE frame
        
        All Output is in same format/sale or unit of Input
        
    Body availlable : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter,
    Saturn, Uranus, Neptune, Pluto
    For all of this body, center = Geometric center
    (because is keplerian orbit)
    NAIFID availlable : all major body and all naif id in custom_spice
    
    """
    icrs_traj = ICRS_convert(central_body, epoch, position, speed, ephemeride=ephemeride,
                             custom_spice=custom_spice, central_body_ephe_file=central_body_ephe_file)

    bte_traj = icrs_traj.transform_to(BarycentricTrueEcliptic(equinox=equinox,
                                                              representation_type=CartesianRepresentation,
                                                              differential_type=CartesianDifferential))

    return epoch, bte_traj.data.xyz, bte_traj.velocity.d_xyz


def to_GCRS(central_body, epoch, position, speed,
            equinox=Time("J2000"), ephemeride="jpl", custom_spice=[], central_body_ephe_file=""):
    """
    Convert Position and Speed body in Local ICRS frame (center is center body)
    to a position and Speed body in GCRS frame (Earth Barycenter). 
    
    Input:
        central_body (string or int): string for body or int for NAIFID
        epoch : (Float array, Nx1, astropy Time) Date for convert
        position : (Float array, Nx3, astropy unit) body position in local frame
        speed : (float array, Nx3, astropy unit) body speed in local frame
    
    Output:
        epoch : (Float array, Nx1, astropy Time) Same date as input
        position : (Float array, Nx3, astropy unit) body position in BTE frame
        speed : (float array, Nx3, astropy unit) body speed in BTE frame
        
        All Output is in same format/sale or unit of Input
        
    Body availlable : Sun, Earth, Moon, Mercury, Venus, Mars, Jupiter,
    Saturn, Uranus, Neptune, Pluto
    For all of this body, center = Geometric center
    (because is keplerian orbit)
    NAIFID availlable : all major body and all naif id in custom_spice
    
    """
    icrs_traj = ICRS_convert(central_body, epoch, position, speed, ephemeride=ephemeride,
                             custom_spice=custom_spice, central_body_ephe_file=central_body_ephe_file)

    bte_traj = icrs_traj.transform_to(GCRS(equinox=equinox,
                                           representation_type=CartesianRepresentation,
                                           differential_type=CartesianDifferential))

    return epoch, bte_traj.data.xyz, bte_traj.velocity.d_xyz
