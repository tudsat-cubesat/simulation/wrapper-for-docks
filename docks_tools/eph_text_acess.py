#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 16:31:01 2019

@author: sdurand
"""

from scipy.interpolate import interp1d
import ccsds as cc
from astropy import units as u
from astropy.coordinates import ICRS
from astropy.coordinates import CartesianRepresentation, CartesianDifferential

second_day = 60 * 60 * 24

def red_eph_file(central_body_ephe_file, order=2):
    _, _, _, _, data_time, data_science = cc.read_ccsds(central_body_ephe_file)
    # need custom body time file in MJD
    # need custom body position file in km
    # need custom body velocity file in km/s
    # position and velicity in ICRF SSB.

    eph_time = data_time[0] + data_time[0] / second_day
    if order == 1:
        fp = interp1d(eph_time, [data_science[0], data_science[1], data_science[2]], kind='cubic',
                      fill_value="extrapolate")
        return fp
    elif order == 2:
        fp = interp1d(eph_time, [data_science[0], data_science[1], data_science[2]], kind='cubic',
                      fill_value="extrapolate")
        fv = interp1d(eph_time, [data_science[3], data_science[4], data_science[5]], kind='cubic',
                      fill_value="extrapolate")
        return fp, fv
    elif order == 3:
        fp = interp1d(eph_time, [data_science[0], data_science[1], data_science[2]], kind='cubic',
                      fill_value="extrapolate")
        fv = interp1d(eph_time, [data_science[3], data_science[4], data_science[5]], kind='cubic',
                      fill_value="extrapolate")
        fa = interp1d(eph_time, [data_science[6], data_science[7], data_science[8]], kind='cubic',
                      fill_value="extrapolate")
        return fp, fv, fa


def resample_traj(epoch, pos, vel=[], acc=[]):
    position_unit = "km"
    velocity_unit = "km/s"

    [x, y, z] = pos(epoch.mjd)

    if vel != []:
        [vx, vy, vz] = vel(epoch.mjd)
    if acc != []:
        [ax, ay, az] = acc(epoch.mjd)

    if vel != []:
        eph_central_body = ICRS(x=x * u.Unit(position_unit), y=y * u.Unit(position_unit), z=z * u.Unit(position_unit),
                                v_x=vx * u.Unit(velocity_unit), v_y=vy * u.Unit(velocity_unit),
                                v_z=vz * u.Unit(velocity_unit),
                                representation_type=CartesianRepresentation,
                                differential_type=CartesianDifferential)
    else:
        eph_central_body = ICRS(x=x * u.Unit(position_unit), y=y * u.Unit(position_unit), z=z * u.Unit(position_unit),
                                representation_type=CartesianRepresentation)

    if acc != []:
        acc_vector = [ax, ay, az]
        return eph_central_body, acc_vector
    else:
        return eph_central_body
