#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 16:02:17 2018

@author: developpeur
"""
import numpy as np

def euler_2_quat(alpha_beta_gamma, beta=None, gamma=None, seq="123"):
    """Improve your life drastically

    Assumes the Euler sequence angles correspond to the quaternion R via

        R = exp(alpha*{1}/2) * exp(beta*{2}/2) * exp(gamma*{3}/2)

    Parameters
    ----------
    
    alpha_beta_gamma: float or array of floats
        This argument may either contain an array with last dimension of
        size 3, where those three elements describe the (alpha, beta, gamma)
        values for each rotation; or it may contain just the alpha values,
        in which case the next two arguments must also be given.
    beta: None, float, or array of floats
        If this array is given, it must be able to broadcast against the
        first and third arguments.
    gamma: None, float, or array of floats
        If this array is given, it must be able to broadcast against the
        first and second arguments.
    seq: default = '123', string, content a 3 numbers corresponding to Euler sequence
    12 posibilitis : '123', '132', '121', '131'
                     '213', '231', '212', '232'
                     '312', '321', '313', '323'

    Returns
    -------
    R: float array that represents a quaternion
        The shape of this array will be the same as the input, except that
        the last dimension will be removed.

    """
    if gamma is None:
        alpha_beta_gamma = np.asarray(alpha_beta_gamma, dtype=np.double)
        alpha = alpha_beta_gamma[..., 0]
        beta  = alpha_beta_gamma[..., 1]
        gamma = alpha_beta_gamma[..., 2]
    else:
        alpha = np.asarray(alpha_beta_gamma, dtype=np.double)
        beta  = np.asarray(beta, dtype=np.double)
        gamma = np.asarray(gamma, dtype=np.double)
    R = np.empty(np.broadcast(alpha, beta, gamma).shape + (4,), dtype=np.double)


    if seq == "123":#valid
        R[..., 0] = np.cos(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) - np.sin(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 1] = np.sin(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) + np.cos(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 2] = np.cos(alpha/2)*np.sin(beta/2)*np.cos(gamma/2) - np.sin(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
        R[..., 3] = np.sin(alpha/2)*np.sin(beta/2)*np.cos(gamma/2) + np.cos(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
    elif seq == "132":#valid
        R[..., 0] = np.sin(alpha/2)*np.sin(beta/2)*np.sin(gamma/2) + np.cos(alpha/2)*np.cos(beta/2)*np.cos(gamma/2)
        R[..., 1] = np.sin(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) - np.cos(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 2] = np.cos(alpha/2)*np.cos(beta/2)*np.sin(gamma/2) - np.sin(alpha/2)*np.sin(beta/2)*np.cos(gamma/2)
        R[..., 3] = np.sin(alpha/2)*np.cos(beta/2)*np.sin(gamma/2) + np.cos(alpha/2)*np.sin(beta/2)*np.cos(gamma/2)
    elif seq == "121":#valid
        R[..., 0] = np.cos(beta/2)*np.cos((alpha+gamma)/2)
        R[..., 1] = np.cos(beta/2)*np.sin((alpha+gamma)/2)
        R[..., 2] = np.sin(beta/2)*np.cos((alpha-gamma)/2)
        R[..., 3] = np.sin(beta/2)*np.sin((alpha-gamma)/2)
    elif seq == "131":#valid
        R[..., 0] = np.cos(beta/2)*np.cos((alpha+gamma)/2)
        R[..., 1] = np.cos(beta/2)*np.sin((alpha+gamma)/2)
        R[..., 2] = -1*np.sin(beta/2)*np.sin((alpha-gamma)/2)
        R[..., 3] = np.sin(beta/2)*np.cos((alpha-gamma)/2)
    elif seq == "213":#valid
        R[..., 0] = np.cos(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) + np.sin(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 1] = np.sin(alpha/2)*np.cos(beta/2)*np.sin(gamma/2) + np.cos(alpha/2)*np.sin(beta/2)*np.cos(gamma/2)
        R[..., 2] = np.sin(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) - np.cos(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 3] = np.cos(alpha/2)*np.cos(beta/2)*np.sin(gamma/2) - np.sin(alpha/2)*np.sin(beta/2)*np.cos(gamma/2)
    elif seq == "231":#valid
        R[..., 0] = np.cos(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) - np.sin(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 1] = np.sin(alpha/2)*np.sin(beta/2)*np.cos(gamma/2) + np.cos(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
        R[..., 2] = np.sin(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) + np.cos(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 3] = np.cos(alpha/2)*np.sin(beta/2)*np.cos(gamma/2) - np.sin(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
    elif seq == "212":#valid
        R[..., 0] = np.cos(beta/2)*np.cos((alpha+gamma)/2)
        R[..., 1] = np.sin(beta/2)*np.cos((alpha-gamma)/2)
        R[..., 2] = np.cos(beta/2)*np.sin((alpha+gamma)/2)
        R[..., 3] = -np.sin(beta/2)*np.sin((alpha-gamma)/2)
    elif seq == "232":#valid
        R[..., 0] = np.cos(beta/2)*np.cos((alpha+gamma)/2)
        R[..., 1] = np.sin(beta/2)*np.sin((alpha-gamma)/2)
        R[..., 2] = np.cos(beta/2)*np.sin((alpha+gamma)/2)
        R[..., 3] = np.sin(beta/2)*np.cos((alpha-gamma)/2)
        
    elif seq == "312":#valid
        R[..., 0] = np.cos(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) - np.sin(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 1] = np.cos(alpha/2)*np.sin(beta/2)*np.cos(gamma/2) - np.sin(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
        R[..., 2] = np.sin(alpha/2)*np.sin(beta/2)*np.cos(gamma/2) + np.cos(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
        R[..., 3] = np.sin(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) + np.cos(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
    elif seq == "321":#valid
        R[..., 0] = np.cos(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) + np.sin(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 1] = np.cos(alpha/2)*np.cos(beta/2)*np.sin(gamma/2) - np.sin(alpha/2)*np.sin(beta/2)*np.cos(gamma/2)
        R[..., 2] = np.sin(alpha/2)*np.cos(beta/2)*np.sin(gamma/2) + np.cos(alpha/2)*np.sin(beta/2)*np.cos(gamma/2)
        R[..., 3] = np.sin(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) - np.cos(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
    elif seq == "313":#valid
        R[..., 0] = np.cos(beta/2)*np.cos((alpha+gamma)/2)
        R[..., 1] = np.sin(beta/2)*np.cos((alpha-gamma)/2)
        R[..., 2] = np.sin(beta/2)*np.sin((alpha-gamma)/2)
        R[..., 3] = np.cos(beta/2)*np.sin((alpha+gamma)/2)
    elif seq == "323":#valid
        R[..., 0] = np.cos(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) - np.sin(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
        R[..., 1] = np.cos(alpha/2)*np.sin(beta/2)*np.sin(gamma/2) - np.sin(alpha/2)*np.sin(beta/2)*np.cos(gamma/2)
        R[..., 2] = np.cos(alpha/2)*np.sin(beta/2)*np.cos(gamma/2) + np.sin(alpha/2)*np.sin(beta/2)*np.sin(gamma/2)
        R[..., 3] = np.sin(alpha/2)*np.cos(beta/2)*np.cos(gamma/2) + np.cos(alpha/2)*np.cos(beta/2)*np.sin(gamma/2)
    else:
        print("the euler sequence does not exist")

    return R
