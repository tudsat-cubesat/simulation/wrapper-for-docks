

def filter_unit_name(unit_name, unit_type):

    unit_name = unit_name.lower()

    if unit_type == "time":
        if unit_name == "s" or unit_name == "sec" or unit_name == "second" or unit_name == "seconds" \
                or unit_name == "seconde" or unit_name == "secondes":
            unit_name = "second"
        elif unit_name == "min" or unit_name == "minute" or unit_name == "minutes" or unit_name == "minuts":
            unit_name = "minute"
        elif unit_name == "h" or unit_name == "hrs" or unit_name == "hours" or unit_name == "hour" \
                or unit_name == "heure":
            unit_name = "hour"
        elif unit_name == "day" or unit_name == "days" or unit_name == "jours":
            unit_name = "day"
        else:
            raise ValueError("Unit type : time, not recognised")
    elif unit_type == "angle":
        if unit_name == "degre" or unit_name == "deg" or unit_name == "degree":
            unit_name = "deg"
        elif unit_name == "rad" or unit_name == "radiant":
            unit_name = "rad"
        else:
            raise ValueError("Unit type : angle, not recognised")
    else:
        raise ValueError("Unit type not recognised")
    return unit_name

