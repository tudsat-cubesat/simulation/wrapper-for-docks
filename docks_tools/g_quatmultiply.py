'''
n = quatmultiply(q,r) calculates the quaternion product, n, for two given quaternions, q and r. Inputs q and r can each be either an m-by-4 matrix containing m quaternions, or a single 1-by-4 quaternion. n returns an m-by-4 matrix of quaternion products. Each element of q and r must be a real number. Additionally, q and r have their scalar number as the first column.

The quaternions have the form of:
	q = q0 + i*q1 + i*q2 + i*q3
	r = r0 + i*r1 + i*r2 + i*r3

The quaternion product has the form of:
	n = q*r = n0 + i*n1 + i*n2 + i*n3
where
	n0 = (r0q0 - r1q1 - r2q2 - r3q3)
	n1 = (r0q1 + r1q0 - r2q3 + r3q2)
	n2 = (r0q2 + r1q3 + r2q0 - r3q1)
	n3 = (r0q3 - r1q2 + r2q1 + r3q0)
'''

import numpy as np

def g_quatmultiply(q, r):
    
    if((q.size==4) and (r.size==4)):
        print('Il faut des quaternions en entrée')
    
    n = np.zeros(4)
    
    n[1] = r[1]*q[1] - r[2]*q[2] - r[3]*q[3] - r[4]*q[4]
    n[2] = r[1]*q[2] + r[2]*q[1] - r[3]*q[4] + r[4]*q[3]
    n[3] = r[1]*q[3] + r[2]*q[4] + r[3]*q[1] - r[4]*q[2]
    n[4] = r[1]*q[4] - r[2]*q[3] + r[3]*q[2] + r[4]*q[1]

    return n

