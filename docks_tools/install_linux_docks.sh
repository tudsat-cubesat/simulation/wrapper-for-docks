#! /bin/bash -i
# Release: 09/2020
# Authors:
# Development by Paris Observatory - PSL Université Paris
# CCERES, PSL Space Pole: https://cceres.psl.eu/?lang=en 
# Rashika Jain.

# Should be executed in the terminal before running this file. If user does not have administrative priveleges, user should find other alternative or use anaconda
# sudo apt update
# sudo apt install gcc
# sudo apt install -y python3
# sudo apt install -y python3-pip python3-venv

cd $(dirname $0)
cd ..

# Making a virtual environment. To change the name of the virtual environment change the variable name_venv
name_venv=docks_python_env
if [ -d "$name_venv" ]; then
    rm -rf "$name_venv"; fi
python3 -m venv $name_venv

#Add virtual environment to path
echo "alias docks_python='source `pwd`/$name_venv/bin/activate'" >> ~/.bashrc

# To be run in the virtual environment
source ./$name_venv/bin/activate
python3 -m pip install --upgrade pip setuptools wheel
echo "-----Installing required python libraries-----"
python3 -m pip install numpy scipy lxml tqdm texteditor pandas poliastro==0.10.0
python3 -m pip install astropy astroquery regex Cython pyqt5==5.14 pyquaternion
if [ -d "Propagator" ]; then
	python3 -m pip install calcephpy; fi

#echo -e "-----Python libraries installation is complete in your virtual environment. Always run DOCKS in this environment.-----"
export name_venv

# To install different DOCKS modules, add path to bash scripts after this"
# propagator
if [ -d "Propagator" ]; then
    bash ./Propagator/install_linux_prop.sh; fi










