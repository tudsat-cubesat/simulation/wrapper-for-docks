#! /bin/bash -i
# Release: 09/2020
# Authors:
# Development by Paris Observatory - PSL Université Paris
# CCERES, PSL Space Pole: https://cceres.psl.eu/?lang=en 
# Rashika Jain.

cd $(dirname $0)
cd ..
# Should be executed in the terminal before running this file. If user does not have administrative priveleges, user should find other alternative or use anaconda
#sudo apt-get update
# sudo apt install gcc

echo "-----Do you have anaconda3/miniconda installed? Enter 1 or 2.-----"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) echo -e "Installing Anaconda 3 in /home/$USER/anaconda3";
			wget https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh;
			bash ./Anaconda3-2020.07-Linux-x86_64.sh -p /home/$USER/anaconda3 -b;
			echo "export PATH=\"/home/$USER/anaconda3/bin:\$PATH\""  >> ~/.bashrc;
			anaconda="/home/$USER/anaconda3";
			rm Anaconda3-2020.07-Linux-x86_64.sh;
			PATH="$anaconda/bin:$PATH"; break;;
    esac
done

# Updating conda
conda update -n base -c defaults conda
conda init bash
source ~/.bashrc

# Making a virtual environment. To change the name of the virtual environment change the variable name_venv
name_venv=docks_python_env
echo "Making a virtual environment named $name_venv." 
conda remove -n $name_venv --all
conda create -n $name_venv

echo "-----Installing required python libraries-----"
conda install -n $name_venv pip setuptools wheel tqdm numpy scipy lxml pyqt=5.9 regex cython pandas
conda install -n $name_venv -c astropy astroquery
conda install -n $name_venv -c conda-forge poliastro

# pip installations to be run in the virtual environment
source ~/anaconda3/etc/profile.d/conda.sh
conda activate $name_venv
pip install texteditor pyquaternion
if [ -d "Propagator" ]; then
	pip install calcephpy; fi

echo -e "-----Python libraries installation is complete in your virtual environment. Always run DOCKS in this environment.-----"
export name_venv

# To install different DOCKS modules, add path to bash scripts after this (for linux only)"
# propagator
if [ -d "Propagator" ]; then
    bash ./Propagator/install_linux_prop.sh; fi
