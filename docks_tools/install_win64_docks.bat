@echo off
:: Release: 09/2020
:: Authors:
:: Development by Paris Observatory - PSL Université Paris
:: CCERES, PSL Space Pole: https://cceres.psl.eu/?lang=en 
:: Rashika Jain.

rem Download latest version of python3 if you do not have it or use anaconda. Select the option "Add python to path". Open a command prompt with administrative priveleges
py -m pip install --upgrade pip
py -m pip install virtualenv

rem "Changing directory to the DOCKS folder."
pushd %~dp0
cd ..
rmdir /S docks_python_env 2>nul
echo "-----Making a virtual environment named docks_python_env.-----" 
rem "To change the name of the virtual environment change the variable name_venv" 
set name_venv=docks_python_env
py -m venv %name_venv%

rem "To be run in the virtual environment"
echo "-----Virtual environment activated-----"
call %name_venv%\Scripts\activate.bat
py -m pip install --upgrade pip setuptools wheel

echo "-----Installing required python libraries-----"
py -m pip install numpy scipy lxml tqdm texteditor pandas poliastro==0.10.0
py -m pip install astropy regex Cython pyqt5==5.14 pyquaternion
if exist %CD%\Propagator\ (
	call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat" & py -m pip install calcephpy
	)
echo "-----Python libraries installed-----"

echo "-----Simplifying the activation of virtual environment-----"
SET env_path=%CD%
set path=%env_path:\=\\%

(
echo docks_python = %env_path%\%name_venv%\Scripts\activate.bat $*
) > cmd_aliases.txt
(
echo @echo off
echo cls
echo doskey /macrofile=%env_path%\cmd_aliases.txt
) > cmd_autorun.cmd
(
echo Windows Registry Editor Version 5.00
echo.
echo [HKEY_CURRENT_USER\Software\Microsoft\Command Processor]
echo "Autorun"="%path%\\cmd_autorun.cmd"
) > add_cmd_autorun.reg

call .\add_cmd_autorun.reg

echo.
echo docks_tools installation is complete
if exist %CD%\Propagator\ echo Propagator installation is complete
echo WARNING! This window will be closed if you press any key.
echo.
PAUSE








