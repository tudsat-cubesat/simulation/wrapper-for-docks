@echo off
:: Release: 09/2020
:: Authors:
:: Development by Paris Observatory - PSL Université Paris
:: CCERES, PSL Space Pole: https://cceres.psl.eu/?lang=en 
:: Rashika Jain.

rem Download latest version of Anaconda with python3. 
rem Open a conda prompt with admin priveleges

call conda update -n base -c defaults conda
rem "To change the name of the virtual environment change the variable name_venv" 
set name_venv=docks_python_env
echo "Making a virtual environment named %name_venv%." 
call conda remove -n %name_venv% --all
call conda create -n %name_venv%

Echo "-----Installing required python libraries-----"
call conda install -n %name_venv% pip setuptools wheel tqdm numpy scipy lxml pyqt=5.9 regex pandas
call conda install -n %name_venv% -c anaconda cython
call conda install -n %name_venv% -c astropy astroquery
call conda install -n %name_venv% -c conda-forge poliastro
SET /P UseProp=Are you going to use DOCKS Propagator (Y/[N])?
IF /I "%UseProp%"=="Y" (
	call conda install -n %name_venv% -c peterjc123 vs2017_runtime
	)

rem "To be run in the virtual environment"
call conda activate %name_venv%
call pip install texteditor pyquaternion
IF /I "%UseProp%"=="Y" (
	call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat" & call pip install calcephpy
	)

cmd /k








