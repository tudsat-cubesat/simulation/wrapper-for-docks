#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 09:29:29 2019

@author: Claire Castell
debug: Boris Segret, from ref.: http://kieranwynn.github.io/pyquaternion/ 
       WARNING: in pyquaternion, q1*q2 means the composition of rotation q1 
	   followed by rotation q2 as documented in the ref link above (but
	   it should be the reverse way in quaternion algebra formalism). 
"""

from pyquaternion import Quaternion
import numpy as np
from astropy.coordinates import *
import euler2quat as e2q


def quat_unit(q_sat_icrf, q_element_sat, v_object_icrf):
    """
def quat_unit(q_sat_icrf, q_element_sat, v_object_icrf, idebug):
    qsi: quat sat/icrf
    qes: quat elt/sat
    vei: object pos /icrf
    
    """
    #v_object_element_cart = []
    #v_object_element_sph = []
    v_element_icrf = [] # normal to the surface elements in ICRF frame
    incidence_angle = []
    incidence_rad = []
    u_object_icrf = v_object_icrf/np.linalg.norm(v_object_icrf) # unitizing
    
    for i in range(len(q_element_sat)):
        """ WARNING:
        #q_element_icrf = q_element_sat[i].unit * q_sat_icrf.unit # unitized
		This expression is wrong. See Note in the header.
        """
        q_element_icrf = q_sat_icrf.unit * q_element_sat[i].unit # unitized
        v_element_icrf.append((q_element_icrf * Quaternion(0,1,0,0) * q_element_icrf.conjugate).vector)
        cos2norm = np.dot(u_object_icrf, v_element_icrf[i])
        incidence_rad.append(np.pi/2-np.arccos(cos2norm))
        incidence_angle.append(incidence_rad[i]*180/np.pi)
        
        #if idebug <= 10:
        #    print("v_el_icrf[",i,"]:", v_element_icrf[i],"incidence[",i,"](DEG,RAD):", incidence_angle[i],incidence_rad[i])
    """
        v_object_element_cart.append(list((q_element_icrf * Quaternion(scalar=None, vector=v_object_icrf) * q_element_icrf.conjugate).vector))        
    
    for i in range(len(v_object_element_cart)):
        v_object_element_sph.append(cartesian_to_spherical(v_object_element_cart[i][0], v_object_element_cart[i][1], v_object_element_cart[i][2] ))
    
    for dist, lat, long in v_object_element_sph:
        if lat.value >= 0 :
            incidence_angle.append((np.pi/2 - lat.value) * 180 / (np.pi))
            incidence_rad.append((np.pi/2 - lat.value) )
        elif round(lat.value, 2) == round(-np.pi/2, 2):
            incidence_angle.append((np.pi/2 - lat.value) * 180 / (np.pi)) 
            incidence_rad.append((np.pi/2 - lat.value))
        else:
            incidence_angle.append((-np.pi/2 - lat.value) * 180 / (np.pi))
            incidence_rad.append((-np.pi/2 - lat.value) )
#    return v_object_element_cart , v_object_element_sph , incidence_angle, incidence_rad
"""
    return incidence_angle, incidence_rad

def quat_time(q_sat_icrf_t, q_element_sat, v_object_icrf_t):
    """
    n: number of steps
    i: number of elements (elements can be solar panels, camera, surfaces... )
    :param q_sat_icrf_t: (list of quaternions, size:n) rotation of the satellite in local ICRF frame
    :param q_element_sat: (list of quaternions, size:i) rotation of the elements in satellite frame
    :param v_object_icrf_t: (list of tuple, size:n) coordinates of the object in the ICRF frame
    
    #:return: v_object_element_t: (tuple, list of tuples) object cartesian coordinates in the element's frame (xo_e,yo_e,zo_e)
	:return: incident angle in DEG and RAD of object direction from each q_element_sat surface
                     
    """
    assert len(q_sat_icrf_t) == len(v_object_icrf_t), 'Check the dimensions'
    
    #v_object_element_cart_t = []
    #v_object_element_sph_t = []
    incidence_angle_t = []
    incidence_rad_t = []
    
    """(debug)print("--- q_element list ---")
    for i in range(len(q_element_sat)):
        print(q_element_sat[i])
    print("--- q_sat_icrf_t[0:10] (length =",len(q_sat_icrf_t),") ---")
    for i in range(10):
        print(q_sat_icrf_t[i])
    print("--- v_object_icrf_t[0:10] (length =",len(v_object_icrf_t),") ---")
    for i in range(10):
        print(v_object_icrf_t[i])
    print("-> cart[0]_x, inc[...]:")
    i=0"""
    for q_sat_icrf, v_object_icrf in zip(q_sat_icrf_t, v_object_icrf_t):
        #cart, sph, inc, rad = quat_unit(q_sat_icrf, q_element_sat, v_object_icrf,i)
        #inc, rad = quat_unit(q_sat_icrf, q_element_sat, v_object_icrf,i)
        inc, rad = quat_unit(q_sat_icrf, q_element_sat, v_object_icrf)
        #v_object_element_cart_t.append(cart)
        #v_object_element_sph_t.append(sph)
        incidence_angle_t.append(inc)
        incidence_rad_t.append(rad)
        """if i<=10:
            print(inc)
            i=i+1"""
        
#    return v_object_element_cart_t, v_object_element_sph_t, incidence_angle_t, incidence_rad_t
    return incidence_angle_t, incidence_rad_t


def main():
    quat_time(QSI, QES, VOI)

#if __name__ == "__main__":
#   import doctest
#    doctest.testmod()




