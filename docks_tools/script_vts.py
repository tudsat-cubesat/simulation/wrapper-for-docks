#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 18:25:06 2018

@author: developpeur
"""
from lxml import etree
import subprocess
from datetime import datetime
import ccsds as ccsd
import os
from shutil import copyfile


# import numpy as np

def extract_celestia_addon(vts_path):
    """

    :param vts_path: TODO
    :return:
        TODO
    """
    return ("TODO")


def Run_VTS(vts_path, vts_project_path):
    VTS = subprocess.Popen([vts_path + "startVTS", "--project", vts_project_path], shell=True,
                           stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                           universal_newlines=True)
    return VTS


def basic_vts_project(satellite_name, before_time, after_time, vts_name, path_orbite_file, vts_path):
    # fixed variable
    # ============================

    # sensor_date : (if sensor_on=1)
    sensor_sat_data = [[["sensor1"], ["RECTANGULAR", 0.0872665, 0.0872665, 1000], [0, 0, 0], ["euler", 212, 90, 0, 0]]]

    # 3DS file Path:
    file_3ds_path = "Cube_body.3ds"

    # output trajectory file:
    traj_path = "orbit_" + satellite_name + ".txt"

    # sat data :
    sat_data = [[satellite_name], [file_3ds_path], ["file", traj_path], ["fixed", "quat", 1, 0, 0, 0]]

    copy_3d_file = [[file_3ds_path, "./vts_data", vts_path],
                    ["gold-mx.bmp", "./vts_data", vts_path],
                    ["gold-my.bmp", "./vts_data", vts_path],
                    ["gold-mz.bmp", "./vts_data", vts_path],
                    ["gold-px.bmp", "./vts_data", vts_path],
                    ["gold-py.bmp", "./vts_data", vts_path],
                    ["gold-pz.bmp", "./vts_data", vts_path]]

    vts_project_path = vts_path + "/" + vts_name + ".vts"

    time_track = [before_time, after_time]

    # Ground Station: (if ground_on=1)
    GroundStation_data = [[["Meudon", 48.80687, 2.230129, 165],
                           ["sensor1"],
                           ["ELLIPTICAL", 1.48353, 1.48353, 10000],
                           [0, 0, 0],
                           ["quat", 1, 0, 0, 0]]]

    # ==============================

    # time :
    cic, origin, comment, header, data_time, data_science = ccsd.read_ccsds(path_orbite_file)

    Start_mjd_time = [int(data_time[0][0]), data_time[0][1]]
    End_mjd_time = [int(data_time[-1][0]), data_time[-1][1]]

    # Body Name (center frame) :
    for i in range(len(header)):
        if header[i].replace(" ", "").split("=")[0] == "CENTER_NAME":
            Body_parent_name = header[i].replace(" ", "").split("=")[1]

    # 3DS and texture file Path:
    for i in range(len(copy_3d_file)):

        if os.path.isfile(copy_3d_file[i][2] + "/" + copy_3d_file[i][0]):
            copyfile(copy_3d_file[i][1] + "/" + copy_3d_file[i][0],
                     copy_3d_file[i][2] + "/" + copy_3d_file[i][0] + "_temp")
            os.remove(copy_3d_file[i][2] + "/" + copy_3d_file[i][0])
            copyfile(copy_3d_file[i][2] + "/" + copy_3d_file[i][0] + "_temp",
                     copy_3d_file[i][2] + "/" + copy_3d_file[i][0])
            os.remove(copy_3d_file[i][2] + "/" + copy_3d_file[i][0] + "_temp")
        else:
            copyfile(copy_3d_file[i][1] + "/" + copy_3d_file[i][0], copy_3d_file[i][2] + "/" + copy_3d_file[i][0])

    # Trajectoir file path :
    if os.path.isfile(vts_path + "/" + traj_path):
        copyfile(path_orbite_file, vts_path + "/" + traj_path + "_temp")
        os.remove(vts_path + "/" + traj_path)
        copyfile(vts_path + "/" + traj_path + "_temp", vts_path + "/" + traj_path)
        os.remove(vts_path + "/" + traj_path + "_temp")
    else:
        copyfile(path_orbite_file, vts_path + "/orbit_" + satellite_name + ".txt")

    # run script
    succes = Create_VTS(Start_mjd_time, End_mjd_time, Body_parent_name,
                        sat_data,
                        vts_project_path=vts_project_path,
                        ground_on=1, GroundStation_data=GroundStation_data,
                        sensor_sat_on=1, sensor_sat_data=sensor_sat_data,
                        time_track=time_track)

    if succes == vts_project_path:
        return 1
    else:
        return 0


def Create_VTS(Start_mjd_time, End_mjd_time, Body_parent_name,
               sat_data,
               vts_project_path="VTS-" + datetime.utcnow().strftime("%Y_%m_%d--%H-%M-%S") + ".vts",
               ground_on=0, GroundStation_data=[[]],
               intervisibility_on=0, intervisibility_path=[],
               mode_on=0, mode_path="",
               sensor_sat_on=0, sensor_sat_data=[[]],
               time_track=[12, 12]):
    # time :
    # Start_mjd_time = [55276,0]
    # End_mjd_time = [55276, 500]

    # Body Name :
    # Body_parent_name = "Sol/Earth" or "Sol"

    # sat_data :
    # sat_data=[["name"],["model.3ds"],["fixed",1,1,1],["fixed","euler",212,90,0,0]]
    # sat_data=[["name"],["model.3ds"],["fixed",1,1,1],["fixed","quat",1,0,0,0]]
    # sat_data=[["name"],["model.3ds"],["file","traj_file.txt"],["fixed",quat,1,0,0,0]]
    # sat_data=[["name"],["model.3ds"],["file","traj_file.txt"],["file","quat_file.txt"]]

    # Ground Station :(if ground_on=1)
    # GroundStation_data =[[["Meudon",48.80687, 2.230129, 165],["sensor1"],["ELLIPTICAL",1.48353,1.48353,10000],[0,0,0],["quat",1,0,0,0]]]
    # RECTANGULAR or ELLIPTICAL type
    # quat or euler for attitude mode

    # Satelite Name : 
    # Sat_name = "docks_sat"

    # 3DS file Path:
    # file_3ds_path = "model.3ds"

    # intervisibility_path (if intervisibility_on=1):
    # intervisibility_path = ["inter.txt"] (1 for 1 groundstation)

    # mode_path (if mode_on=1):
    # mode_path = mode.txt

    # sensor_date : (if sensor_on=1)
    # sensor_data =[[["sensor1"],["RECTANGULAR",0.0872665,0.0872665,1000],[0,0,0],["euler",212,90,0,0]]]
    # RECTANGULAR or ELLIPTICAL type

    # _________________________________________________________________________
    if Body_parent_name == "sun":
        body_child_name = ""
        body_name = "Sol"
    elif Body_parent_name == "mercury":
        body_child_name = "Mercury"
        body_name = "Sol/" + body_child_name
    elif Body_parent_name == "venus":
        body_child_name = "Venus"
        body_name = "Sol/" + body_child_name
    elif Body_parent_name == "earth":
        body_child_name = "Earth"
        body_name = "Sol/" + body_child_name
    elif Body_parent_name == "mars":
        body_child_name = "Mars"
        body_name = "Sol/" + body_child_name
    elif Body_parent_name == "jupiter":
        body_child_name = "Jupiter"
        body_name = "Sol/" + body_child_name
    elif Body_parent_name == "saturn":
        body_child_name = "Saturn"
        body_name = "Sol/" + body_child_name
    elif Body_parent_name == "uranus":
        body_child_name = "Uranus"
        body_name = "Sol/" + body_child_name
        body_name = "Sol/" + body_child_name
    elif Body_parent_name == "neptune":
        body_child_name = "Neptune"
        body_name = "Sol/" + body_child_name
    else:
        print("Script does not support this body parent name :", Body_parent_name, "Only sun and planets are accepted.")
        return
        # raise ValueError("Script does not support this body parent name :", Body_parent_name)

    Project = etree.Element("Project")
    Project.set("Revision", "6841")

    General = etree.SubElement(Project, "General")
    General_data = [["Name", ""],
                    ["StartDateTime", str(Start_mjd_time[0]) + " " + str(Start_mjd_time[1])],
                    ["EndDateTime", str(End_mjd_time[0]) + " " + str(End_mjd_time[1])]]
    for data in General_data:
        General.set(data[0], data[1])

    # MetaData = etree.SubElement(Project,"MetaData")
    # MetaData.set("Description","Projet Docks")

    StartOptions = etree.SubElement(Project, "StartOptions")
    StartOptions_data = [["TimeRatio", "1"],
                         ["UseStateTimeRatio", "0"],
                         ["SysTimeSynced", "0"],
                         ["Paused", "0"],
                         ["Looped", "0"],
                         ["Minimized", "0"],
                         ["Hidden", "0"],
                         ["AutoClosed", "0"]]
    for data in StartOptions_data:
        StartOptions.set(data[0], data[1])

    BrokerOptions = etree.SubElement(Project, "BrokerOptions")
    BrokerOptions_data = [["WindowMode", "Undocked"],
                          ["Collapsed", "0"],
                          ["AlwaysOnTop", "1"],
                          ["XPos", "2242"],
                          ["YPos", "692"],
                          ["Width", "679"],
                          ["Height", "411"],
                          ["ActiveTab", "0"]]
    # rajouter une option pour la dernière revision
    for data in BrokerOptions_data:
        BrokerOptions.set(data[0], data[1])

    TimelineOptions = etree.SubElement(Project, "TimelineOptions")
    TimelineOptions_data = [["ProjectLocked", "1"],
                            ["CursorLocked", "0"],
                            ["CursorRatio", "0"],
                            ["ViewStart", "33282 0.000000"],
                            ["ViewSpan", "0"],
                            ["DateFormat", "ISODate"]]
    for data in TimelineOptions_data:
        TimelineOptions.set(data[0], data[1])

    TimelineScenario = etree.SubElement(TimelineOptions, "TimelineScenario")
    TimelineScenario_data = [["Name", "Scenario"],
                             ["Pos", "0"],
                             ["Size", "23"]]
    for data in TimelineScenario_data:
        TimelineScenario.set(data[0], data[1])

    sky = etree.SubElement(Project, "Sky")
    Sun = etree.SubElement(sky, "Sun")
    prop2d_sun = etree.SubElement(Sun, "Prop2d")
    icon = etree.SubElement(prop2d_sun, "Icon")
    icon_data = [["Anchor", "CENTER"],
                 ["Size", "MEDIUM"],
                 ["Opacity", "100"]]
    for data in icon_data:
        icon.set(data[0], data[1])

    font = etree.SubElement(icon, "Font")
    font.set("Size", "MEDIUM")
    font.set("Color", "1 1 1")

    imagelayer = etree.SubElement(icon, "ImageLayer")
    imagelayer.set("Type", "Default")

    track_sun = etree.SubElement(Sun, "Track")
    track_sun_data = [["Color", "0.862745 0.862745 0"],
                      ["PenStyle", "SolidLine"],
                      ["PenWidth", "2"]]
    for data in track_sun_data:
        track_sun.set(data[0], data[1])

    visibilitycircle = etree.SubElement(Sun, "VisibilityCircle")
    visibilitycircle_data = [["ContourColor", "0.501961 0.501961 0"],
                             ["FillColor", "0 0 0"],
                             ["FillOpacity", "50"]]
    for data in visibilitycircle_data:
        visibilitycircle.set(data[0], data[1])

    starcatalog = etree.SubElement(sky, "StarCatalog")
    starcatalog.set("CatalogMode", "Builtin")

    track_star = etree.SubElement(starcatalog, "Track")
    track_star_data = [["Color", "1 1 1"],
                       ["PenStyle", "DotLine"],
                       ["PenWidth", "1"]]
    for data in track_star_data:
        track_star.set(data[0], data[1])

    ToBeUsedApps = etree.SubElement(Project, "ToBeUsedApps")

    Application_data = [[["Name", "SurfaceView"],
                         ["Id", "0"],
                         ["AutoStarted", "1"]],
                        [["Name", "Celestia"],
                         ["Id", "1"],
                         ["AutoStarted", "1"]]]
    for data in Application_data:
        Application = etree.SubElement(ToBeUsedApps, "Application")
        for subdata in data:
            Application.set(subdata[0], subdata[1])

    Entities = etree.SubElement(Project, "Entities")
    if body_child_name != "":
        Body = etree.SubElement(Entities, "Body")
        Body_data = [["Name", body_child_name],
                     ["ParentPath", "Sol"]]
        for data in Body_data:
            Body.set(data[0], data[1])

        Prop2d_body = etree.SubElement(Body, "Prop2d")
        icon_body = etree.SubElement(Prop2d_body, "Icon")
        icon_body.set("Anchor", "CENTER")
        icon_body.set("Size", "MEDIUM")
        icon_body.set("Opacity", "100")
        font_body = etree.SubElement(icon_body, "Font")
        font_body.set("Size", "MEDIUM")
        font_body.set("Color", "1 1 1")
        imagelayer_body = etree.SubElement(icon_body, "ImageLayer")
        imagelayer_body.set("Type", "Default")

        track_body = etree.SubElement(Body, "Track")
        track_body.set("Color", "1 0 0.203998")
        track_body.set("PenStyle", "SolidLine")
        track_body.set("PenWidth", "2")

        visibilitycircle_body = etree.SubElement(Body, "VisibilityCircle")
        visibilitycircle_body.set("ContourColor", "1 0.00849928 0")
        visibilitycircle_body.set("FillColor", "1 0.50425 0.499992")
        visibilitycircle_body.set("FillOpacity", "60")

        EphemerisMode = etree.SubElement(Body, "EphemerisMode")
        EphemerisMode.set("Mode", "Default")

        layers = etree.SubElement(Body, "Layers")
        builtinlayer = etree.SubElement(layers, "BuiltinLayer")
        builtinlayer.set("Name", "defaultLayer")

        if ground_on:

            GroupGroundStations = etree.SubElement(Body, "GroupGroundStations")

            for data in GroundStation_data:
                GroundStation = etree.SubElement(GroupGroundStations, "GroundStation")
                GroundStation.set("Name", data[0][0])
                Prop2d_ground = etree.SubElement(GroundStation, "Prop2d")
                icon_ground = etree.SubElement(Prop2d_ground, "Icon")
                icon_ground.set("Anchor", "CENTER")
                icon_ground.set("Size", "SMALL")
                icon_ground.set("Opacity", "100")
                font_ground = etree.SubElement(icon_ground, "Font")
                font_ground.set("Size", "MEDIUM")
                font_ground.set("Color", "1 1 0")
                imagelayer_ground = etree.SubElement(icon_ground, "ImageLayer")
                imagelayer_ground.set("Type", "Default")
                LatLongAlt = etree.SubElement(GroundStation, "LatLongAlt")
                Value = etree.SubElement(LatLongAlt, "Value")
                Fixed = etree.SubElement(Value, "Fixed")
                Fixed.set("Data", str(data[0][1]) + " " + str(data[0][2]) + " " + str(data[0][3]))
                sensorstation = etree.SubElement(GroundStation, "SensorStation")
                sensortarget = etree.SubElement(sensorstation, "SensorTarget")

                targetaltitude = etree.SubElement(sensortarget, "TargetAltitude")
                targetaltitude.set("Altitude", "1000")

                sensor_ground = etree.SubElement(sensorstation, "Sensor")
                sensor_ground.set("Name", data[1][0])
                sensorprop_ground = etree.SubElement(sensor_ground, "SensorProp")

                sensorattributes_ground = etree.SubElement(sensorprop_ground, "SensorAttributes")
                sensorattributes_ground.set("SensorType", data[2][0])
                sensorattributes_ground.set("HalfAngleX", str(data[2][1]))
                sensorattributes_ground.set("HalfAngleY", str(data[2][2]))

                sensorgraphics = etree.SubElement(sensorprop_ground, "SensorGraphics")
                sensorgraphics.set("Range", str(data[2][3]))
                sensorgraphics.set("VolumeColor", "0.499992 0.775586 1")
                sensorgraphics.set("VolumeOpacity", "60")
                sensorgraphics.set("ContourColor", "0 0.551171 1")

                geometry_sensor_ground = etree.SubElement(sensor_ground, "Geometry")

                position_sensor_ground = etree.SubElement(geometry_sensor_ground, "Position")
                position_sensor_value_ground = etree.SubElement(position_sensor_ground, "Value")
                position_sensor_value_fixed_ground = etree.SubElement(position_sensor_value_ground, "Fixed")
                position_sensor_value_fixed_ground.set("Data",
                                                       str(data[3][0]) + " " + str(data[3][1]) + " " + str(data[3][2]))

                orientation_sensor_ground = etree.SubElement(geometry_sensor_ground, "Orientation")
                if data[4][0] == "euler":
                    euler_orientation_sensor_ground = etree.SubElement(orientation_sensor_ground, "EulerAngle")
                    euler_orientation_sensor_ground.set("RotationSequence", str(data[4][1]))
                    orientation_sensor_value_ground = etree.SubElement(euler_orientation_sensor_ground, "Value")
                    orientation_sensor_value_fixed_ground = etree.SubElement(orientation_sensor_value_ground, "Fixed")
                    orientation_sensor_value_fixed_ground.set("Data",
                                                              str(data[4][2]) + " " + str(data[4][3]) + " " + str(
                                                                  data[4][4]))
                elif data[4][0] == "quat":
                    quaternion_orientation_sensor_ground = etree.SubElement(orientation_sensor_ground, "Quaternion")
                    orientation_sensor_value_ground = etree.SubElement(quaternion_orientation_sensor_ground, "Value")
                    orientation_sensor_value_fixed_ground = etree.SubElement(orientation_sensor_value_ground, "Fixed")
                    orientation_sensor_value_fixed_ground.set("Data",
                                                              str(data[4][1]) + " " + str(data[4][2]) + " " + str(
                                                                  data[4][3]) + " " + str(data[4][4]))

    # rajouter n satellite
    Satellite = etree.SubElement(Entities, "Satellite")
    Satellite.set("Name", sat_data[0][0])
    Satellite.set("ParentPath", body_name)

    CommonProp = etree.SubElement(Satellite, "CommonProp")
    OrbitPath = etree.SubElement(CommonProp, "OrbitPath")
    OrbitPath.set('Color', "1 1 0")
    OrbitPath.set("PenStyle", "SolidLine")
    OrbitPath.set("PenWidth", "2")

    Prop2d_sat = etree.SubElement(Satellite, "Prop2d")
    icon_sat = etree.SubElement(Prop2d_sat, "Icon")
    icon_sat.set("Anchor", "CENTER")
    icon_sat.set("Size", "MEDIUM")
    icon_sat.set("Opacity", "100")

    font_sat = etree.SubElement(icon_sat, "Font")
    font_sat.set("Size", "MEDIUM")
    font_sat.set("Color", "1 1 1")

    imagelayer_sat = etree.SubElement(icon_sat, "ImageLayer")
    imagelayer_sat.set("Type", "Default")

    track_sat = etree.SubElement(Satellite, "Track")
    track_sat.set("Color", "0 0.334493 1")
    track_sat.set("PenStyle", "SolidLine")
    track_sat.set("PenWidth", "2")

    visibilitycircle_sat = etree.SubElement(Satellite, "VisibilityCircle")
    visibilitycircle_sat.set("ContourColor", "0 1 0.996338")
    visibilitycircle_sat.set("FillColor", "0.499992 1 0.998169")
    visibilitycircle_sat.set("FillOpacity", "60")

    Component = etree.SubElement(Satellite, "Component")
    Component.set("Name", sat_data[0][0])

    Graphics3d = etree.SubElement(Component, "Graphics3d")
    File3ds = etree.SubElement(Graphics3d, "File3ds")
    File3ds.set("Name", sat_data[1][0])

    Radius = etree.SubElement(Graphics3d, "Radius")
    Radius.set("Value", "1")

    LightSensitive = etree.SubElement(Graphics3d, "LightSensitive")
    LightSensitive.set("Value", "1")

    Use3dsCoords = etree.SubElement(Graphics3d, "Use3dsCoords")
    Use3dsCoords_data = [["Value", "0"],
                         ["MeshScale", "1"]]
    for data_i in Use3dsCoords_data:
        Use3dsCoords.set(data_i[0], data_i[1])

    AxesPosition = etree.SubElement(Graphics3d, "AxesPosition")
    AxesPosition.set("Value", "1")
    RotationCenter = etree.SubElement(Graphics3d, "RotationCenter")
    RotationCenter_data = [["X", "0"],
                           ["Y", "0"],
                           ["Z", "0"]]
    for data_i in RotationCenter_data:
        RotationCenter.set(data_i[0], data_i[1])

    Geometry_sat = etree.SubElement(Component, "Geometry")
    Position_sat = etree.SubElement(Geometry_sat, "Position")
    Value_pos_sat = etree.SubElement(Position_sat, "Value")
    if sat_data[2][0] == "fixed":
        Value_pos_sat_fixed = etree.SubElement(Value_pos_sat, "Fixed")
        Value_pos_sat_fixed.set("Data", str(data[2][1]) + " " + str(data[2][2]) + " " + str(data[2][3]))
    elif sat_data[2][0] == "file":
        File_pos_sat = etree.SubElement(Value_pos_sat, "File")
        File_pos_sat.set("Name", sat_data[2][1])

    orientation_sat = etree.SubElement(Geometry_sat, "Orientation")
    if sat_data[3][0] == "fixed":
        if sat_data[3][1] == "euler":
            euler_orientation_sat = etree.SubElement(orientation_sat, "EulerAngle")
            euler_orientation_sat.set("RotationSequence", str(sat_data[3][2]))
            orientation_sat_value = etree.SubElement(euler_orientation_sat, "Value")
            orientation_sat_value_fixed = etree.SubElement(orientation_sat_value, "Fixed")
            orientation_sat_value_fixed.set("Data",
                                            str(sat_data[3][3]) + " " + str(sat_data[3][4]) + " " + str(sat_data[3][5]))
        elif sat_data[3][1] == "quat":
            quaternion_orientation_sat = etree.SubElement(orientation_sat, "Quaternion")
            orientation_sat_value = etree.SubElement(quaternion_orientation_sat, "Value")
            orientation_sat_value_fixed = etree.SubElement(orientation_sat_value, "Fixed")
            orientation_sat_value_fixed.set("Data", str(sat_data[3][2]) + " " + str(sat_data[3][3]) + " " + str(
                sat_data[3][4]) + " " + str(sat_data[3][5]))
    elif sat_data[3][0] == "file":
        File = etree.SubElement(Value, "File")
        File.set("Name", sat_data[3][1])

    if sensor_sat_on:
        for data in sensor_sat_data:
            sensorsatellite = etree.SubElement(Component, "SensorSatellite")
            sensor = etree.SubElement(sensorsatellite, "Sensor")
            sensor.set("Name", data[0][0])
            sensorprop = etree.SubElement(sensor, "SensorProp")

            sensorattributes = etree.SubElement(sensorprop, "SensorAttributes")
            sensorattributes.set("SensorType", data[1][0])
            sensorattributes.set("HalfAngleX", str(data[1][1]))
            sensorattributes.set("HalfAngleY", str(data[1][2]))

            sensorgraphics = etree.SubElement(sensorprop, "SensorGraphics")
            sensorgraphics.set("Range", str(data[1][3]))
            sensorgraphics.set("VolumeColor", "0.499992 0.775586 1")
            sensorgraphics.set("VolumeOpacity", "60")
            sensorgraphics.set("ContourColor", "0 0.551171 1")
            sensortrace = etree.SubElement(sensorgraphics, "SensorTrace")
            sensortrace.set("Duration", "0")
            sensortrace.set("Opacity", "60")

            geometry_sensor = etree.SubElement(sensor, "Geometry")

            position_sensor = etree.SubElement(geometry_sensor, "Position")
            position_sensor_value = etree.SubElement(position_sensor, "Value")
            position_sensor_value_fixed = etree.SubElement(position_sensor_value, "Fixed")
            position_sensor_value_fixed.set("Data", str(data[2][0]) + " " + str(data[2][1]) + " " + str(data[2][2]))

            orientation_sensor = etree.SubElement(geometry_sensor, "Orientation")
            if data[3][0] == "euler":
                euler_orientation_sensor = etree.SubElement(orientation_sensor, "EulerAngle")
                euler_orientation_sensor.set("RotationSequence", str(data[3][1]))
                orientation_sensor_value = etree.SubElement(euler_orientation_sensor, "Value")
                orientation_sensor_value_fixed = etree.SubElement(orientation_sensor_value, "Fixed")
                orientation_sensor_value_fixed.set("Data",
                                                   str(data[3][2]) + " " + str(data[3][3]) + " " + str(data[3][4]))
            elif data[3][0] == "quat":
                quaternion_orientation_sensor = etree.SubElement(orientation_sensor, "Quaternion")
                orientation_sensor_value = etree.SubElement(quaternion_orientation_sensor, "Value")
                orientation_sensor_value_fixed = etree.SubElement(orientation_sensor_value, "Fixed")
                orientation_sensor_value_fixed.set("Data", str(data[3][1]) + " " + str(data[3][2]) + " " + str(
                    data[3][3]) + " " + str(data[3][4]))

    Events = etree.SubElement(Satellite, "Events")
    if intervisibility_on:
        for i in range(len(intervisibility_path)):
            File = etree.SubElement(Events, "File")
            File.set("Name", intervisibility_path[0])
    if mode_on:
        File = etree.SubElement(Events, "File")
        File.set("Name", mode_path)

    ignoredfiles = etree.SubElement(Project, "IgnoredFiles")
    if sat_data[2][0] == "file":
        file = etree.SubElement(ignoredfiles, "File")
        file.set("Name", sat_data[2][1])

    AdditionalFiles = etree.SubElement(Project, "AdditionalFiles")

    States = etree.SubElement(Project, "States")

    Instant_data = [["Time", "33282 0"],
                    ["TimeRatio", "1"],
                    ["Label", "Initial state"]]
    Instant = etree.SubElement(States, "Instant")
    for data in Instant_data:
        Instant.set(data[0], data[1])

    Command_data = [["CMD PROP WindowGeometry 2240 215 640 480",
                     "CMD PROP WindowMenus true",
                     "CMD STRUCT TrackVisible \"" + body_name + "\" false",
                     "CMD STRUCT TrackWindow \"" + body_name + "\" 12 12",
                     "CMD STRUCT VisibilityCircleVisible \"" + body_name + "/" + sat_data[0][0] + "\" true",
                     "CMD STRUCT TrackWindow \"" + body_name + "/" + sat_data[0][0] + "\" " + str(
                         time_track[0]) + " " + str(time_track[1])],
                    ["CMD PROP WindowGeometry 2909 675 640 480",
                     "CMD PROP WindowMenus true",
                     "CMD PROP CameraDesc bodyfixed " + body_name + "/" + sat_data[0][0] + "_ref/" + sat_data[0][
                         0] + "_QswAxes" + " nil -0.000000000610260 0.000000000610260 -0.000000000610260 -0.060003068094145 -0.704556324563679 0.540625092093754 0.455768062873244 0.289660692214966",
                     "CMD PROP AmbientLight 1",
                     "CMD STRUCT SatelliteScale \"" + body_name + "/" + sat_data[0][0] + "\" 1e-2",
                     "CMD STRUCT TrackWindow \"" + body_name + "/" + sat_data[0][0] + "\" " + str(
                         time_track[0]) + " " + str(time_track[1]),
                     "CMD STRUCT SunDirectionVisible \"" + body_name + "/" + sat_data[0][0] + "\" true",
                     "CMD STRUCT VelocityVectorVisible \"" + body_name + "/" + sat_data[0][0] + "\" true"]]

    for i in range(len(Command_data)):
        AppState = etree.SubElement(Instant, "AppState")
        AppState.set("Id", str(i))
        for j in range(len(Command_data[i])):
            Command = etree.SubElement(AppState, "Command")
            Command.set("Str", Command_data[i][j])

    tree = etree.ElementTree(Project)
    tree.write(vts_project_path, xml_declaration=True, encoding="utf-8", pretty_print=True)

    return vts_project_path
