#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 18:13:04 2018
Last modified on 29 June 2020

@author: Sébastien Durand, Rashika Jain
"""
from calcephpy import CalcephBin, NaifId, Constants
from astropy import units as u
from astropy.coordinates import ICRS
from astropy.coordinates import CartesianRepresentation, CartesianDifferential
import numpy as np
import os

def scan_spice_file(calceph_object):
    n = calceph_object.getpositionrecordcount()
    target = np.zeros(n)
    center = np.zeros(n)
    first_time = np.zeros(n)
    last_time = np.zeros(n)
    frame = np.zeros(n)
    for j in range(0, n):
        # print(j)
        target[j], center[j], first_time[j], last_time[j], frame[j] = calceph_object.getpositionrecordindex(j + 1)
        target = np.int32(target)
        center = np.int32(center)

    return target, center, first_time, last_time, frame


# def calceph_eph_import(eph_path, ep_type="imcce", file=[], add_file=[]):
#     if ep_type == "imcce":
#         spice_file = [eph_path + "/planete_imcce/inpop17a_TDB_m100_p100_spice.bsp",
#                       eph_path + "/planete_imcce/inpop17a_TDB_m100_p100_spice.tpc",
#                       eph_path + "/mars/mar097.bsp",
#                       eph_path + "/jupiter/jup310.bsp",
#                       eph_path + "/jupiter/jup341.bsp",
#                       eph_path + "/saturn/sat368.bsp",
#                       eph_path + "/saturn/sat375.bsp",
#                       eph_path + "/saturn/sat393.bsp",
#                       eph_path + "/saturn/sat393-rocks_pan.bsp",
#                       eph_path + "/saturn/sat393_daphnis.bsp",
#                       eph_path + "/uranus/ura091-rocks-merge.bsp",
#                       eph_path + "/uranus/ura111.bsp",
#                       eph_path + "/uranus/ura112.bsp",
#                       eph_path + "/neptune/nep081.bsp",
#                       eph_path + "/neptune/nep086.bsp",
#                       eph_path + "/neptune/nep088.bsp",
#                       eph_path + "/pluton/plu055.bsp"] + add_file
#
#     elif ep_type == "jpl":
#         spice_file = [eph_path + "/planete_nasa/de438.bsp",
#                       eph_path + "/mars/mar097.bsp",
#                       eph_path + "/jupiter/jup310.bsp",
#                       eph_path + "/jupiter/jup341.bsp",
#                       eph_path + "/saturn/sat368.bsp",
#                       eph_path + "/saturn/sat375.bsp",
#                       eph_path + "/saturn/sat393.bsp",
#                       eph_path + "/saturn/sat393-rocks_pan.bsp",
#                       eph_path + "/saturn/sat393_daphnis.bsp",
#                       eph_path + "/uranus/ura091-rocks-merge.bsp",
#                       eph_path + "/uranus/ura111.bsp",
#                       eph_path + "/uranus/ura112.bsp",
#                       eph_path + "/neptune/nep081.bsp",
#                       eph_path + "/neptune/nep086.bsp",
#                       eph_path + "/neptune/nep088.bsp",
#                       eph_path + "/pluton/plu055.bsp"] + add_file
#
#     elif ep_type == "custom":
#         spice_file = file + add_file
#
#     else:
#         raise ValueError("just imcce, jpl and custom option")
#
#     print("loading ephemerides.....")
#     peph = CalcephBin.open(spice_file)
#     print("ephemeride file is load")
#
#     return peph

def calceph_eph_import(eph_path):

    spice_file = []
    for dirpath, _, filenames in os.walk(eph_path):
        for f in filenames:
            if not f.endswith('.txt'):
                spice_file.append(os.path.abspath(os.path.join(dirpath, f)))

    peph = CalcephBin.open(spice_file)
    print("Spice kernels loaded")

    return peph

def eph_object_SSB(calceph_object, naif_id, julian_date, order=2):
    jd0 = np.int32(julian_date)
    dt = np.mod(julian_date, 1)

    value_order = calceph_object.compute_order(jd0, dt, naif_id, NaifId.SOLAR_SYSTEM_BARYCENTER,
                                               Constants.UNIT_KM + Constants.UNIT_SEC + Constants.USE_NAIFID, order)

    return value_order


def object_euler_angles(calceph_object, naif_id, julian_date, order=0):
    jd0 = np.int32(julian_date.jd)
    dt = np.mod(julian_date.jd, 1)

    euler_order = calceph_object.orient_order(jd0, dt, naif_id,
                                              Constants.USE_NAIFID + Constants.UNIT_RAD + Constants.UNIT_SEC
                                              + Constants.OUTPUT_EULERANGLES, order)

    return euler_order


def calceph2astropy_icrf(x, y, z, vx=[], vy=[], vz=[], position_unit="km", velocity_unit="km/s"):
    # convert ephemeride to ICRS astropy frame
    if vx:
        eph_astropy = ICRS(x=x * u.Unit(position_unit), y=y * u.Unit(position_unit), z=z * u.Unit(position_unit),
                           v_x=vx * u.Unit(velocity_unit), v_y=vy * u.Unit(velocity_unit),
                           v_z=vz * u.Unit(velocity_unit),
                           representation_type=CartesianRepresentation,
                           differential_type=CartesianDifferential)
    else:

        eph_astropy = ICRS(x=x * u.Unit(position_unit), y=y * u.Unit(position_unit), z=z * u.Unit(position_unit),
                           representation_type=CartesianRepresentation)

    return eph_astropy
