#ATTENTION: module 'julian' needs to be installed first (pip install julian)

import datetime
import julian


def run_generator(avg_start_time,avg_end_time):
    #_____________read prod_power file
    datei = open("../Output/prod_power.txt","r")
    lines = datei.readlines()
    datei.close()
    
    length = len(lines)
    
    start_mjd = julian.to_jd(datetime.datetime.strptime(avg_start_time,"%Y-%m-%dT%H:%M:%S"), fmt="mjd")
    end_mjd = julian.to_jd(datetime.datetime.strptime(avg_end_time,"%Y-%m-%dT%H:%M:%S"), fmt="mjd")
    
    
    #____________calculation
    AmountOfSummands = 0.0
    TotalPower = 0.0
    
    worstCaseAmountOfSummands = 0.0
    worstCaseTotalPower = 0.0
    worstCaseAverage = 10000000.0
    worstCaseAverageNEU = 10000000.0
    
    firsttime = True
    eclipseCounter = 0
    
    for i in range(22,length-1,1):
        split = lines[i].split()
        mjd = float(split[0]) + float(split[1]) / 86400.3
        
        if start_mjd <= mjd and end_mjd >= mjd:
            #remember amount of added powervalues
            AmountOfSummands += 1
            worstCaseAmountOfSummands += 1
            
            #add power values for all solar panels each timestep
            currentPower = 0
            for k in range(2,len(split)):
                currentPower += float(split[k])
            TotalPower += currentPower
            worstCaseTotalPower += currentPower
            
            #check if starts in eclipse or not
            if currentPower == 0 and firsttime == True:
                eclipse = True
            if currentPower != 0 and firsttime == True:
                worstCaseTotalPower = 1000000000000         #invalidate halforbit at start
                eclipse = False
            firsttime = False
    
            #check eclipse
            if currentPower != 0 and eclipse == True:
                eclipse=False
            if currentPower == 0 and eclipse==False:
                eclipse=True
                #count eclipse
                eclipseCounter += 1
                
                
                #compare worstCasePower
                if worstCaseAverageNEU > worstCaseTotalPower / worstCaseAmountOfSummands:
                    worstCaseAverage = worstCaseAverageNEU          #invaildate halforbit at end
                    worstCaseAverageNEU = worstCaseTotalPower / worstCaseAmountOfSummands
                    
                #reset worstCasePower
                worstCaseTotalPower = 0.0
                worstCaseAmountOfSummands = 0.0
    
    
    print("The satellite was " + str(eclipseCounter) + " times in eclipse.\n")
    print("The average power in the given period of time amounts to:\n" + str(TotalPower / AmountOfSummands) + " W")
    
    #currently bugged
    # if eclipseCounter > 0:
    #     print("\nIn the worst case orbit the average power amounts to:\n" + str(worstCaseAverage) + " W")




def getDateOfLine(line):
    split = line.split()
    mjd = float(split[0]) + float(split[1]) / 86400.3
    date = julian.from_jd(mjd, fmt='mjd')
    return date
