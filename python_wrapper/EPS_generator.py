import sys, os, yaml
from astropy.time import Time


sys.path.append('../EPS/')
os.chdir("../EPS/")
from EPS import EPS_simulation
import energy_sim_battery, config_EPS as conf_data
os.chdir("../python_wrapper/")




def run_generator(config,trajectory_file,event_file,quaternion_file):
    #generating config file for EPS simulation using values from given config
    EPS_conf = open("EPS_conf.yaml","w+")
    EPS_conf.write("pathFiles:"
                   + "\n  input:"
                   + "\n    input_trajectory_intervisibility_file: " + trajectory_file
                   + "\n    input_eclipse_file_EPS: ../Output/events_EPS.txt"
                   + "\n    input_attitude_sat_file: " + quaternion_file
                   + "\n    input_mode_file: ../Output/" + config["new_file_names"]["new_mode_file"]
                   + "\n  output:"
                   + "\n    output_incident_angle_file: ../Output/" + config["new_file_names"]["new_angles_file"]
                   + "\n    output_energy_file: ../Output/" + config["new_file_names"]["new_energy_file"]
                   + "\n    output_eclipse_file_vts: "
                   + "\n    output_eclipse_file_EPS: "
                   + "\n    output_flag_file: ../Output/" + config["new_file_names"]["new_flags_file"]
                   + "\n"
                   + "\ntimeInformations:"
                   + "\n  time_format: mjd"
                   + "\n  start: #" + to_mjd(config["general"]["start_time"])
                   + "\n  end: #" + to_mjd(config["general"]["end_time"]) 
                   + "\n"
                   + "\nmodes:"
                   + "\n  modes_number: " + config["power"]["mode_number"]
                   + "\n  modes_names: " + config["power"]["mode_names"]
                   + "\n  modes_values: " + config["power"]["pwr_dmd"]
                   + "\n  modes_average: "   + "0."  #TO-BE-CORRECTED
                   + "\n"
                   + "\nbatteryCharacteristics:"
                   + "\n  capacity_Wh: " + config["power"]["capacity_Wh"]
                   + "\n  capacity_Ah: " + config["power"]["capacity_Ah"]
                   + "\n  voltage: " + config["power"]["voltage"]
                   + "\n  initial_charge: " + config["power"]["initial_charge"]
                   + "\n  minimum_charge: " + config["power"]["minimum_charge"]
                   + "\n"
                   + "\ncellsCharacteristics:"
                   + "\n  cells_type: " + config["power"]["cells_type"]
                   + "\n  surface: " + config["power"]["surface"]
                   + "\n  surface_unity: m2"
                   + "\n  thickness: " + config["power"]["thickness"]
                   + "\n  density: " + config["power"]["density"]
                   + "\n  thermal_capacity: " + config["power"]["thermal_capacity"]
                   + "\n  absorption_coefficient: " + config["power"]["absorption_coefficient"]
                   + "\n  maximum_efficiency: " + config["power"]["maximum_efficiency"]
                   + "\n  efficiency_coefficient: " + config["power"]["efficiency_coefficient"]
                   + "\n"
                   + "\nsatelliteGeometry:"
                   + "\n  panel_position: " + "['" + config["power"]["panel_position"]+ "']"
                   + "\n  sequences: " + "['" + config["power"]["sequences"]+ "']")
    EPS_conf.close()
    
    #configuring EPS
    #data = conf_data.config_eps("EPS_conf.yaml", "load")
    with open(os.getcwd()+"/EPS_conf.yaml") as config_yaml:
        data = yaml.safe_load(config_yaml)

    #starting simulation
    #energy_sim_battery.evaluate_battery_charge(data, os.getcwd()+"/EPS_conf.yaml")
    EPS_simulation(data, os.getcwd()+"/EPS_conf.yaml")

    #delete temporary config file for EPS
    os.remove("EPS_conf.yaml")
  
def to_mjd(time):
    t = Time(time, format="isot")
    return str(t.mjd)
