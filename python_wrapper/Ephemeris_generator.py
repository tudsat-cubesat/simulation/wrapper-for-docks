import datetime, math
from sgp4.api import Satrec, WGS72
import skyfield.sgp4lib as sgp4lib
from astropy.time import Time
import astropy.coordinates as coordinates
import astropy.units as u
import pandas as pd
import numpy as np
from tqdm import tqdm

def run_generator(config, file_name):
    """
    propagates the orbit using sgp4 and writes it to a file
    """
    #setup times, the file requires the modifief julian day separated into day
    #and seconds in that day
    start_time, end_time = mjd_from_config(config)
    time_step = float(config["general"]["time_step"])
    n_steps = int((end_time - start_time) *86400 / time_step)
    times_mjd = [start_time + (i*time_step)/86400 for i in range(n_steps)]
    times_mjd.append(end_time)
    days_seconds = np.array([mjd_to_day_second(t) for t in times_mjd])

    #setup actual propagation
    sgp4_sat = setup_orbit(config)
    positions_velocities_ITRS = np.array([pos_vel(sgp4_sat, t) for t in tqdm(times_mjd)])
    print("converting coordinate system and writing file...")
    positions_velocities = array_ITRS_to_GCRS(positions_velocities_ITRS, times_mjd)
    ephemeris = pd.DataFrame({"day": days_seconds[:,0], "second": days_seconds[:,1],
                              "r_x": positions_velocities[0,0,:],
                              "r_y": positions_velocities[0,1,:],
                              "r_z": positions_velocities[0,2,:],
                              "v_x": positions_velocities[1,0,:],
                              "v_y": positions_velocities[1,1,:],
                              "v_z": positions_velocities[1,2,:]})
    
    write_file(ephemeris, file_name, start_time, end_time, time_step)

def pos_vel(sgp4_sat, mjd):
    """
    returns position and velocities in km and km/s in ITRF frame at specified time
    thsi would be easier with astropy 4.2 but astropy 3.2 doesnt have the TEME frame
    """
    time_mjd = Time(mjd, format="mjd")
    jd_full = time_mjd.jd
    jd = int(time_mjd.mjd)
    frac = jd_full - jd
    e, r, v = sgp4_sat.sgp4(jd, frac)
    r, v = sgp4lib.TEME_to_ITRF(jd_full, np.array(r), np.array(v))
    # e is an error code
    if e != 0:
        # a dict of errors is in sgp4.api import SGP4_ERRORS
        raise Exception("something in the sgp4 propagation went wrong. \
                        check the date and orbital elements")
    return r, v

def array_ITRS_to_GCRS(positions_velocities, times_mjd):
    """
    converts the positions and velocities in the array into the GCRS frame
    """
    times = Time(times_mjd, format="mjd")
    ITRS = coordinates.ITRS(x=positions_velocities[:,0,0]*u.km,
                            y=positions_velocities[:,0,1]*u.km,
                            z=positions_velocities[:,0,2]*u.km,
                            v_x=positions_velocities[:,1,0]*u.km/u.s,
                            v_y=positions_velocities[:,1,1]*u.km/u.s,
                            v_z=positions_velocities[:,1,2]*u.km/u.s,
                            obstime=times)
    GCRS = ITRS.transform_to(coordinates.GCRS(obstime=times))
    return np.array([GCRS.cartesian.xyz.value, GCRS.velocity.d_xyz.value])

def mjd_from_config(config):
    """
    returns the start and end times specified in the config file into
    modified julian days
    """
    start_time = config["general"]["start_time"]
    end_time = config["general"]["end_time"]
    times = Time([start_time, end_time], format="isot", scale="utc")
    return times.mjd

def mjd_to_day_second(mjd):
    """
    convert the modified julain day to that day and the seconds in that day
    """
    day = int(mjd)
    seconds = (mjd - day) * 86400
    return day, seconds

def compute_epoch(config):
    """
    returns the number of days since 1949 December 31 00:00 UT relative to
    the start time specified in the config
    """
    def time_from_config(config):
        """
        converts the start_time string from the config into a tuple of
        year, month, day, hour, minute and seconds
        """
        real_time = config["general"]["start_time"]
        year, mon, day = map(int, real_time.split("T")[0].split("-"))
        hr, minute, sec = map(int, real_time.split("T")[1].split(":"))
        return (year, mon, day, hr, minute, sec)

    time = datetime.datetime(*time_from_config(config), tzinfo=datetime.timezone.utc)
    epoch_seconds = (time - datetime.datetime(1949, 12, 31, tzinfo=datetime.timezone.utc)).total_seconds()
    epoch_days = epoch_seconds / (60*60*24)
    return epoch_days

def setup_orbit(config):
    """
    returns the sgp4 satellite object with orbit specified in config
    """
    sgp4_sat = Satrec()
    sgp4_sat.sgp4init(WGS72, "i",
                666,                                                       #satellite number
                compute_epoch(config),                                     #epoch (days since 1949 December 31)
                float(config["orbital_elements"]["b_star"]) / 6371,        #bstar (/earth_radii)
                float(config["orbital_elements"]["n_dot"]),                #ndot (revs/day)
                float(config["orbital_elements"]["nd_dot"]),               #nddot (revs/day^3)
                float(config["orbital_elements"]["ecco"]),                 #ecco
                float(config["orbital_elements"]["argpo"]),                #argpo (radians)
                float(config["orbital_elements"]["inclo"]) * math.pi/180,  #inclo (radians)
                float(config["orbital_elements"]["mo"]),                   #mo (radians)
                float(config["orbital_elements"]["no_kozai"]),             #no_kozai (radians/minute)
                float(config["orbital_elements"]["nodeo"]) * math.pi/180,  #nodeo (radians)
                )

    return sgp4_sat

def write_file(ephemeris, file_name, start_time, end_time, time_step):
    """
    writes the ephemeris data into the file
    """
    now = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    headline = "CIC_OEM_VERS = 2.0\nCREATION_DATE = "+ now +"\nORIGINATOR = TUDSat team\n\n"
    meta = ("META_START\n\n"
            "COMMENT  TUDSat MBSE\n"
            "COMMENT  "+str(time_step)+"sec increment\n"
            "COMMENT  Format : MJD (int) sec (float) X (km) Y (km) Z (km) VX (km/s) VY (km/s) VZ (km/s)\n"
            "COMMENT  START_TIME = "+str(start_time)+"\n"
            "COMMENT  STOP_TIME  = "+str(end_time)+"\n\n"
            "OBJECT_NAME         = TUDSat\n"
            "OBJECT_ID           = TUDSat\n"
            "CENTER_NAME         = EARTH\n"
            "REF_FRAME           = GCRF\n"
            "TIME_SYSTEM         = UTC\n\n"
            "META_STOP\n\n")

    np.savetxt(file_name, ephemeris,
                header=(headline+meta), delimiter=" ", comments="",
                fmt="%5d %.1f %.3f %.3f %.3f %.3f %.3f %.3f")

if __name__ == "__main__":
    #if the script is called on its own the trajectory.cfg file is used as the config file
    run_generator("trajectory_config.cfg","trajectory_new.txt")

