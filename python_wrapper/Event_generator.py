import sys, os
sys.path.append('../Intervisibility/')
os.chdir("../Intervisibility/")

# import main
from launching_inter import launching_inter

os.chdir("../python_wrapper/")

def run_generator(config, file_name, trajectory_file):
    Visi_conf = open("Visi_conf.yaml","w+")
    Visi_conf.write("input_trajectory_name: " + trajectory_file
                    + "\nintervisibility_output_name: ../Output/"+ config["new_file_names"]["new_event_file"]      #TO-BE-CORRECTED
                    + "\nEPS_output: ../Output/" + "events_EPS.txt"
                    + "\nminimum_inter_event_duration: 20"      #TO-BE-CORRECTED
                    + "\nevent_precision: 6"            #TO-BE-CORRECTED
                    + "\n"
                    + "\nstations:"
                    + "\n  GS1:"
                    + "\n    name: " + config["intervisibility"]["name_1"]
                    + "\n    latitude: " + config["intervisibility"]["lat_1"]
                    + "\n    longitude: " + config["intervisibility"]["lon_1"]
                    + "\n    height: " + config["intervisibility"]["height_1"]
                    + "\n    elevationLimit: " + config["intervisibility"]["visi_cone_1"]
                    + "\n    stationIndex: " + config["intervisibility"]["index_1"]
                    + "\n  GS2:"
                    + "\n    name: " + config["intervisibility"]["name_2"]
                    + "\n    latitude: " + config["intervisibility"]["lat_2"]
                    + "\n    longitude: " + config["intervisibility"]["lon_2"]
                    + "\n    height: " + config["intervisibility"]["height_2"]
                    + "\n    elevationLimit: " + config["intervisibility"]["visi_cone_2"]
                    + "\n    stationIndex: " + config["intervisibility"]["index_2"])
    
    Visi_conf.close()
    
    launching_inter(os.getcwd()+"/Visi_conf.yaml")
    os.remove("Visi_conf.yaml")
