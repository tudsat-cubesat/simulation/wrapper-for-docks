import datetime, math, configparser, utils
from tqdm import tqdm
import pandas as pd
import numpy as np

"""
This script generates the MODE file for the simulation.
The modes are:
IDLE: the satellite is not in range of a groundstation and in eclipse
SUNP: the satellite is not in range of a groundstation but its z+ side is
      pointed at the sun to generate power
Giii: G + the last three digits of the groundstation
      that z+ side is pointing towards
CAM:  once a day the satellite takes a photo instead of the downlink during
      grounstation visibility 
TURNGS:     100min before Giii the satellite starts turning towards gs_attitude
TURNSUN:    delays SUNP by 100min after communication and turns towards SUNP-attitude

"""

def run_generator(config, file_name, event_file):

    # dataframe that will be filled in this script
    modes_df = pd.DataFrame({"day":[], "second":[], "mode":[]})

    ground_stations = utils.get_ground_stations(config)

    events_df = utils.read_events(event_file)
    

    # the satellite will start in IDLE
    start_day, start_second = utils.get_start_time_mjd(config)
    modes_df = add_mode(modes_df, start_day, start_second, "IDLE")
    last_mode = "IDLE"
    
    progress_bar = tqdm(total=len(events_df["event"]))

    #it is assumed that the satellite starts in eclipse
    #errors resulting from that should self corrected after the first eclipse 
    in_sun = False
    
    #if the Camera shall not be simulated cam_activated defaults to True an stays True
    if "CAM" in config["power"]["mode_names"]:
        print("Camera mode included")
        cam_activated = False
    else:
        print("Camera-Mode not included")
        cam_activated = True
        
    turning = False
    turned_before_gs=False
    #maneuver needed for turning maneuver
    turn_time = int(config["pointing"]["turn_time"])
    
    ignore_second_com = config["pointing"]["ignore_second_com"]
    if ignore_second_com == "True": ignore_second_com = True
    elif ignore_second_com == "False": ignore_second_com = False
    else: print("ERROR: wrong input for ignore_second_com")
    skip_com = False
    
    #last_day saves the previous day
    last_day = start_day

    #a dictionary with the groundstation index as key and a boolean as
    #value. boolean is true if the satellite is within comm range of the
    #groundstation. values are initialized as false
    gs_visibilities = make_gs_dictionary(ground_stations)
    
    #save first time for grounstation
    next_gs_day, next_gs_second, next_gs_index = getnextcom(0,events_df)
    
    
    for event_idx, event in enumerate(events_df["event"]):
        event_day = events_df["day"][event_idx]
        event_second = events_df["second"][event_idx]
        event_number = events_df["number"][event_idx]
        

        #the satellite will prioritize pointing at the groundstations

        #true if next comm before turn_time could be over
        commSoon = timedifference(event_day,event_second,next_gs_day,next_gs_second)<=turn_time
        #true if last mode not comm or camera
        currentlysunp = not ("G0" in last_mode or last_mode == "CAM")
        
        if commSoon and currentlysunp and not turning and not turned_before_gs and not skip_com:
            turning = True
            turned_before_gs = True
            mode_name = "TURNG" + next_gs_index[-3:]
            turnstart_day = event_day
            turnstart_sec = event_second

            
        #if turning maneuver has been going on for longer than the turn time
        if turning and timedifference(turnstart_day,turnstart_sec,event_day,event_second)>turn_time:
            turning = False
            if in_sun:
                mode_name = "SUNP"
            else:
                mode_name = "IDLE"
                
                
        if skip_com and timedifference(turnstart_day,turnstart_sec,event_day,event_second)>2*turn_time:
            skip_com = False
            next_gs_day, next_gs_second, next_gs_index = getnextcom(event_idx,events_df)
            if in_sun:
                mode_name = "SUNP"
            else:
                mode_name = "IDLE"
        
        if "ECL/IN" in event:
            in_sun = False
            #change only if not comm or turning
            if True not in gs_visibilities.values() and not turning:
                mode_name = "IDLE"
                
        elif "ECL/EG" in event:
            in_sun = True
            #change only if not comm or turning
            if True not in gs_visibilities.values() and not turning:
                mode_name = "SUNP"
       


        #the satellite just entered the visibility cone of a groundstation
        #an event pointing to that groundstation will be created 
        #if the satellite doesnt already point to a groundstation
        if "COM/IN" in event and not skip_com:
            gs_index = event_number
            
            #evaluates to true if there is no other gs in comm range
            #and the photo for this day was already taken
            if True not in gs_visibilities.values():
                #if the photo for this day was not taken yet it shall be done now
                if not cam_activated:
                    mode_name = "CAM"

                else:
                    #the modes are named G + last three digits of the index,
                    #e.g. G001
                    mode_name = "COM"

                    

            #notes the communication ingress
            gs_visibilities[gs_index] = True

        #the satellite left the visibility cone of a gs. it will point
        #to a different gs if one is visible or go to sun pointing or
        #idle depending on the situation
        if "COM/EG" in event and not skip_com and (last_mode=="CAM" or last_mode=="COM"):
            #remove the gs that just left the FOV of the satellite from
            #the visibilities list
            gs_index = event_number
            gs_visibilities[gs_index] = False
            
            #saves the finish of the camera mode
            if True not in gs_visibilities.values():
                cam_activated = True

            #if there is another groundstation visible the satellite will
            #point to that one. it will point to the first one it finds
            if True in gs_visibilities.values():
                gs_visi_values = list(gs_visibilities.values())
                gs_visi_keys = list(gs_visibilities.keys())
                key_idx = gs_visi_values.index(True)
                gs_index = gs_visi_keys[key_idx]
                mode_name = "COM"


            next_gs_day, next_gs_second, next_gs_index = getnextcom(event_idx,events_df)
            #true if next comm in less than 100min
            commSoon = timedifference(event_day,event_second,next_gs_day,next_gs_second)<turn_time*2
            
            if not ignore_second_com:
                if not commSoon and True not in gs_visibilities.values():
                    mode_name = "TURNSUN"
                    turning = True
                    turned_before_gs = False
                    turnstart_day = event_day
                    turnstart_sec = event_second
            else:
                if True not in gs_visibilities.values():
                    mode_name = "TURNSUN"
                    turning = True
                    turned_before_gs = False
                    skip_com = True
                    turnstart_day = event_day
                    turnstart_sec = event_second


        #resets cam_activated for next day if the camera shall be simulated
        if last_day < event_day and "CAM" in config["power"]["mode_names"]:
            cam_activated = False
            last_day=event_day
            
        
        
        
        #correct error in time caused by big time steps in iteration
        if "TURNG" in mode_name:
            event_day,event_second = subtime(next_gs_day,next_gs_second,turn_time)
        
        if last_mode != mode_name:
            modes_df = add_mode(modes_df, event_day, event_second, mode_name)


        #correct error in time caused by big time steps in iteration
        if mode_name == "TURNSUN":
            turn_end_day, turn_end_sec = addtime(turnstart_day,turnstart_sec,turn_time)
            if in_sun:
                mode_name = "SUNP"
            else:
                mode_name = "IDLE"
            modes_df = add_mode(modes_df, turn_end_day, turn_end_sec, mode_name)
        

        last_mode = mode_name
        
        progress_bar.update(1)
        
    #the simulation needs a mode value at the end of the simulation time
    end_day, end_second = utils.get_end_time_mjd(config)
    end_mode = list(modes_df["mode"])[-1]
    modes_df = add_mode(modes_df, end_day, end_second, end_mode)
    
    progress_bar.close()

    write_file(modes_df, file_name)

def add_mode(modes_df, day, second, mode):
    """
    adds a mode to the end of the modes list
    """
    new_row = pd.DataFrame({"day":[day], "second":[second], "mode":[mode]})
    modes_df = modes_df.append(new_row)
    return modes_df

def make_gs_dictionary(ground_stations):
    """
    makes a groundstation dictionary with the index as key and a boolean
    as value, the boolean is true if the satellite is in communication
    range of the groundstation. values are initialized as false
    """
    gs_dict = {}
    for gs in ground_stations:
        #gs[3] is the index
        gs_dict[gs[3]] = False
    return gs_dict

def timedifference(d1,s1,d2,s2):
    dt = (d2-d1)*86400 + s2-s1
    return dt

def addtime(d,s,dt):
    if s+dt < 86400:
        new_s=s+dt
        new_d=d
    else:
        new_s=s-86400+dt
        new_d=d+1
    return new_d,new_s

def subtime(d,s,dt):
    if s-dt < 0:
        new_s=s+86400-dt
        new_d=d-1
    else:
        new_s=s-dt
        new_d=d
    return new_d,new_s
        
        
def getnextcom(event_idx,event_df):
    out_of_range = False
    new_idx = event_idx
    i = 1
    while new_idx == event_idx:
        if event_idx+i > len(event_df["event"])-1:
            #set next comm_day out of simulation 
            out_of_range = True
            next_gs_day = 1000000000000
            next_gs_second = 0
            next_gs_index = 0
            break
        
        next_mode = event_df["event"][event_idx+i]
        if "COM/IN" in next_mode:
            new_idx = event_idx+i
        
        i += 1
    
    if not out_of_range:
        next_gs_day = event_df["day"][new_idx]
        next_gs_second = event_df["second"][new_idx]
        next_gs_index = event_df["number"][new_idx]
    
    return next_gs_day, next_gs_second, next_gs_index
    
def write_file(modes_df, file_name):
    """
    writes the header and data into the file
    """

    now = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    headline = "CIC_MEM_VERS = 1.0\nCREATION_DATE = "+ now +"\nORIGINATOR = TUDSat team\n\n"
    meta = ("META_START\n"
            "COMMENT  TUDSat MBSE\n"
            "COMMENT  ***DUMMY***\n\n"
            "USER_DEFINED_PROTOCOL = NONE\n"
            "USER_DEFINED_CONTENT = CHRONOGRAMS\n"
            "USER_DEFINED_SIZE = 1\n"
            "USER_DEFINED_TYPE = STRING\n"
            "USER_DEFINED_UNIT = [n/a]\n"
            "TIME_SYSTEM = UTC\n\n"
            "META_STOP\n")
    
    np.savetxt(file_name, list(modes_df.to_numpy()), fmt="%5d %.1f %4s", 
               header=(headline+meta), delimiter=" ", comments="")