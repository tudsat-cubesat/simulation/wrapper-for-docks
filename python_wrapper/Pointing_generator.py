import numpy as np
from pyquaternion import Quaternion
import pandas as pd
import utils, datetime
from astropy.time import Time
import astropy.coordinates as coords
from tqdm import tqdm

"""
this script will give an attitude of the satellite at every time step with
the columns being: day(mjd), second, a, b, c, d
where the quaternion is q = a + b*i + c*j + d*k. the quaternion is unitized

when the mode is the index of a groundstation the satellite will orient
the z+ axis in the nadir direction of the groundstation at the closest approach

the change of attitude is not instant. a rotation rate can be specified in the
config file
"""
def run_generator(config, file_name, mode_file):
    """
    creates and writes the attitudes of the satellite as quaternions

    implementation:
    at the beginning of every mode the required attitude is known.
    the interpolation between these fixed attitudes is done using
    the pyquaternion implementation of SLERP.
    """
    gs_dict = make_ground_station_dict(utils.get_ground_stations(config))
    
    COM_orientation = np.fromstring(config["pointing"]["COM_orientation"],dtype=int,sep=",")
    SUNP_orientation = np.fromstring(config["pointing"]["SUNP_orientation"],dtype=int,sep=",")
    
    dt = float(config["general"]["time_step"])
    turn_time = float(config["pointing"]["turn_time"])
    #number of steps used to describe one turn
    n_steps = 15
    #time step between quaternion-change (is dividable by simulation_steps)
    time_step = int(turn_time/n_steps) - (int(turn_time/n_steps)%dt)

    start_orientation = Quaternion.random()

    #load modes and trajectory files
    modes = utils.read_modes(mode_file)

    pointing = modes.copy()
    start = (pointing["day"][0], pointing["second"][0])
    end = (pointing["day"].iloc[-1], pointing["second"].iloc[-1])
    #drop all modes that dont contribute to the turning
    pointing = pointing[pointing["mode"].str.contains("TURN")].reset_index(drop=True)
    
    #add the column "time" containing the time objects corresponding to the day, second values
    pointing["time"] = [get_time_obj(a[0], a[1]) for \
                        a in zip(pointing["day"], pointing["second"])]
    #add the quaternion required by the mode in that line
    quaternions = [attitude_from_mode(a[0], a[1], gs_dict, COM_orientation, SUNP_orientation) \
                   for a in zip(pointing["mode"], pointing["time"])]
    #the first time step of the simulation gets a random Quaternion
    quaternions.insert(0, start_orientation)
    pointing["quaternion"] = quaternions[:-1]
    
    last_end_time = pointing["time"].values[len(pointing)-1]+turn_time/86400
    last_row = {"time":[last_end_time],"quaternion":[quaternions[-1:][0]]}
    last_row=pd.DataFrame(last_row)
    pointing = pointing.append(last_row,ignore_index=True)
    pointing.reset_index(drop=True, inplace=True)

    pointing = add_interpolation_columns(pointing, n_steps)
    pointing = add_interpolated_quaternions(pointing, time_step, n_steps)

    #VTS needs the times as days and seconds in that day in modified julian day
    times = np.array([get_day_second(time) for time in pointing["time"]])
    pointing["day"] = pd.Series(times[:, 0])
    pointing["second"] = pd.Series(times[:, 1])

    #append first frame for VTS
    first_frame = pd.DataFrame({"day":[start[0]], "second":[start[1]],"quaternion":[start_orientation]})
    pointing = first_frame.append(pointing)
    #append last frame for VTS
    last_quaternion = pointing["quaternion"].iloc[-1]
    last_frame = pd.DataFrame({"day": [end[0]], "second": [end[1]], "quaternion":[last_quaternion]},
                    index=[len(pointing)])
    pointing = pointing.append(last_frame)
    
    #remove duplicate lines
    pointing = pointing.drop_duplicates(subset=["day","second","quaternion"])
    pointing.reset_index(drop=True, inplace=True)
    
    write_file(pointing, file_name)

def get_time_obj(day, second):
    """
    returns an astropy time obj
    """
    day_fraction = second / 86400.0
    time = Time(day + day_fraction, format="mjd")
    return time

def get_day_second(time):
    """
    returns the day and seconds in that day from an astropy time object
    """
    time = time.value
    days = int(time)
    seconds = (time - days) * 86400
    return days, seconds

def get_attitude(vector1, vector2):
    """
    returns the quaternion that rotates vector1 into vector2.
    vectors are unitized after input
    """
    unit_vector1 = vector1 / np.linalg.norm(vector1)
    unit_vector2 = vector2 / np.linalg.norm(vector2)
    rotation_axis = np.cross(unit_vector1, unit_vector2)
    rotation_angle = np.arccos(np.dot(unit_vector1, unit_vector2))
    #the length of the vector represents the angle of rotation
    quaternion = Quaternion(axis=rotation_axis, radians=rotation_angle)
    return quaternion

def attitude_from_mode(mode, time, gs_dict, COM_orientation, SUNP_orientation):
    """
    returns the attitude required by the mode, mode cant be IDLE
    """
    if mode == "TURNSUN":
        #TODO: figure out why this is the correct order to specify the vectors
        #VTS reuires the quaternion from ICRS to the body frame wich should
        #mean body_vector = q.rotate(icrs_vector). but this actually calculates
        #the quaternion for icrs_vector = q.rotate(body_vector)
        return get_attitude(SUNP_orientation, get_sun_vector(time))
    if "TURNG" in mode:
        return get_attitude(COM_orientation, get_nadir_vector(gs_dict[mode[4:]], time))
    else:
        raise Exception("Unsupported mode: {}".format(mode))

def angle_between_quaternions(quat1, quat2):
    """
    returns the absolute angle between two quaternions in radians.
    formula taken from: https://math.stackexchange.com/q/90098
    """
    quat1 = quat1.unit
    quat2 = quat2.unit
    if quat1 == quat2:
        return 0
    angle = np.arccos(abs(2 * (np.inner(quat1.elements, quat2.elements))**2 - 1))              
    return abs(angle)

def make_ground_station_dict(ground_stations):
    """
    returns a dictionary with the keys being the mode name and the values being
    the earthlocations of the ground stations.
    the mode name is G + the last three digits of the ground station index
    """
    gs_dict = {}
    for gs in ground_stations:
        index = gs[3]
        name = "G" + index[-3:]
        gs_dict[name] = gs[1]
    return gs_dict

def get_nadir_vector(earth_location, time):
    """
    returns the nadir vector when directly above the groundstation
    """
    earth_to_gs_gcrs = earth_location.get_gcrs(time)
    earth_to_gs = earth_to_gs_gcrs.transform_to(coords.GCRS(obstime=time))
    earth_to_gs = earth_to_gs.cartesian.xyz
    earth_to_gs = earth_to_gs / np.linalg.norm(earth_to_gs)
    #multiply by -1 for it to point towards the earth
    nadir = earth_to_gs * -1
    return nadir

def get_sun_vector(time):
    """
    the unit sun vector from the earth to the sun in the gcrs frame
    """
    sun_vec_gcrs = coords.get_sun(time)
    sun_vec = sun_vec_gcrs.cartesian.xyz.value
    q = get_attitude(sun_vec / np.linalg.norm(sun_vec), [1,0,0])
    return sun_vec / np.linalg.norm(sun_vec)

def make_interpolation_frame(series, time_step, n_steps):
    """
    returns a dataframe containing the interpolated steps with the correct
    corresponding times. the interpolated steps are just before the next_time
    """
    times = [series["time"]]
    #seconds in a day: 86400
    times.extend([series["time"] + (i*time_step)/86400 for i in range(0,n_steps+1)])

    quaternions = [series["quaternion"]]
    #the generator includes the start and endpoint of the interpolation
    #the endpoint is not included in this list
    
    quaternions.extend([next(series["steps_after"]) for i in range(0,n_steps+1)])
    
    return pd.DataFrame({"time":times, "quaternion":quaternions})

def add_interpolation_columns(pointing_df, n_steps):
    """
    adds the columns n_steps, steps_after and next_time to the pointing dataframe.
    they are used for the interpolation.
    """
    #interpolation between each quaternion and the next one, values are generators
    #the endpoints are included to avoid the interpolation done in VTS. it caused the attitude
    #to shift from the mode attitude to the first interpolation step over the duration of the mode
    #n_steps-1 to get the final quaternion in the necessary steps
    steps_after = [Quaternion.intermediates(quat[0], quat[1], n_steps-1, include_endpoints=True) \
                    for quat in \
                    zip(pointing_df["quaternion"][:-1],
                        pointing_df["quaternion"][1:])]
    pointing_df["steps_after"] = pd.Series(steps_after)
    return pointing_df

def add_interpolated_quaternions(pointing_df, time_step, n_steps):
    """
    adds the interpolated quaternions defined by the generator in the steps after column
    """
    #each interpolation gets a seperate frame
    frames = [make_interpolation_frame(pointing_df.iloc[i], time_step, n_steps) for i in \
                 tqdm(range(len(pointing_df["time"])-1))]
    pointing_df = pd.concat(frames)
    pointing_df.reset_index(drop=True, inplace=True)
    return pointing_df

def write_file(pointing, file_name):
    """
    writes the header and data into the file
    """

    now = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    headline = "CIC_AEM_VERS = 1.0\nCREATION_DATE = "+ now +"\nORIGINATOR = TUDSat team\n\n"
    meta = ("META_START\n"
            "COMMENT  TUDSat MBSE\n"
            "COMMENT  ***DUMMY***\n\n"
            "OBJECT_NAME = EPS\n"
            "OBJECT_ID = EPS\n"
            "REF_FRAME_A = GCRF\n"
            "REF_FRAME_B = EPS\n"
            "ATTITUDE_DIR = A2B\n"
            "TIME_SYSTEM = UTC\n"
            "ATTITUDE_TYPE = QUATERNION\n"
            "QUATERNION_TYPE = FIRST\n\n"
            "META_STOP\n")

    #each element must be its own list for the numpy concatenate function
    days = [[i] for i in pointing["day"]]
    seconds = [[i] for i in pointing["second"]]
    quats = [quat.elements for quat in pointing["quaternion"]]
    data = np.concatenate((days, seconds, quats), axis=1)

    np.savetxt(file_name, data,
               fmt="%5d %.1f %.5f %.5f %.5f %.5f",
               header=(headline+meta), delimiter=" ", comments="")