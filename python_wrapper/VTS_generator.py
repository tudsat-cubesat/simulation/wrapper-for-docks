# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 19:53:19 2020

@author: Joschka
"""
import datetime, julian

def run_generator(config,file_name,trajectory_file,event_file,mode_file,quaternion_file,energy_file,angle_file,flag_file):
    start_time = julian.to_jd(datetime.datetime.strptime(config["general"]["start_time"],"%Y-%m-%dT%H:%M:%S"), fmt="mjd")
    end_time = julian.to_jd(datetime.datetime.strptime(config["general"]["end_time"],"%Y-%m-%dT%H:%M:%S"), fmt="mjd")
    
    source = open("VTS_template.txt", "r")
    destination = open(file_name, "w+")
    for line in source:
        if "StartDateTime" in line:
            destination.write(' <General Name="" StartDateTime="'+str(int(start_time))+" "+str(round(start_time%1 * 86400,3))+'" EndDateTime="'+str(int(end_time))+" "+str(round(end_time%1 * 86400,3))+'"/>')
        else:
            destination.write(line)
    source.close()
    destination.close()
