import sys, configparser
sys.path.append('./docks_tools/')
sys.path.append('./Intervisibility/')
sys.path.append('./EPS/')

import Ephemeris_generator, Event_generator, Mode_generator, Pointing_generator, EPS_generator, VTS_generator, Average_generator

# TODO: check config file for illegal values
# TODO: improve sun vector. currently as seen from earth
# TODO: implement config file name as argument at command line
# TODO: implement feature to run several configs in succession automatically

def main(config_file):

    print("reading DOCKS config\n")

    config = configparser.ConfigParser()
    config.read(config_file)
    
    
    #_________________Trajectory_________________

    if config["file_names"]["trajectory_file"] == "NEW":
        file_name = "../Output/"+config["new_file_names"]["new_trajectory_file"]
        print("starting trajectory simulation")
        Ephemeris_generator.run_generator(config, file_name)
        print("trajectory simulation finished\n")
        trajectory_file = config["new_file_names"]["new_trajectory_file"]
        
    else:
        print("skipping trajectory simulation\n")
        trajectory_file = config["file_names"]["trajectory_file"]


    #_________________Intervisibility____________

    if config["file_names"]["event_file"] == "NEW":
        file_name = "../Output/"+config["new_file_names"]["new_event_file"]
        print("starting Intervisibility Event simulation")
        Event_generator.run_generator(config, file_name, ("../Output/"+trajectory_file))
        print("finished Intervisibility Event simulation\n")
        event_file = config["new_file_names"]["new_event_file"]
        
    else:
        print("skipping Intervisibility Event simulation\n")
        event_file = config["file_names"]["event_file"]


    #_________________Modes______________________

    if config["file_names"]["mode_file"] == "NEW":
        file_name = "../Output/"+config["new_file_names"]["new_mode_file"]
        print("starting modes simulation")
        Mode_generator.run_generator(config, file_name, ("../Output/"+event_file))
        print("finished modes simulation\n")
        mode_file = config["new_file_names"]["new_mode_file"]
        
    else:
        print("skipping modes simulation\n")
        mode_file = config["file_names"]["mode_file"]

    #_________________Pointing___________________

    if config["file_names"]["quaternion_file"] == "NEW":
        file_name = "../Output/" + config["new_file_names"]["new_quaternion_file"]
        print("starting Pointing simulation")
        Pointing_generator.run_generator(config, file_name, ("../Output/"+mode_file))
        print("finished Pointing simulation\n")
        quaternion_file =  config["new_file_names"]["new_quaternion_file"]
        
    else:
        print("skipping Pointing simulation\n")
        quaternion_file =  config["file_names"]["quaternion_file"]

    #_________________Power______________________

    if config["file_names"]["energy_file"] == "NEW":
        print("starting Power simulation")
        EPS_generator.run_generator(config,("../Output/"+trajectory_file),("../Output/"+event_file),("../Output/"+quaternion_file))
        print("finished Power simulation\n")
        energy_file =  config["new_file_names"]["new_energy_file"]
        angle_file =  config["new_file_names"]["new_angles_file"]
        flag_file =  config["new_file_names"]["new_flags_file"]
        
    else:
        print("skipping Power simulation\n")
        energy_file =  config["file_names"]["energy_file"]
        angle_file =  config["file_names"]["angles_file"]
        flag_file =  config["file_names"]["flags_file"]
        
    #_________________VTS________________________
    if config["file_names"]["vts_file"] == "NEW":
        file_name = "../Output/" + config["new_file_names"]["vts_file"]
        print("start writing vts file")
        VTS_generator.run_generator(config,file_name,trajectory_file,event_file,mode_file,quaternion_file,energy_file,angle_file,flag_file)
        print("finished writing vts file\n")
    
    else:
        print("skip writing vts file\n")

    #________________AveragePower________________
    if config["general"]["avg_calc"] == str(1):
        avg_start_time = config["general"]["start_time"]
        avg_end_time = config["general"]["end_time"]
        print("start computing average produced power\n")
        Average_generator.run_generator(avg_start_time,avg_end_time)
    
    print("\nthe simulation is finished. All produced files are in the output folder!")



config = configparser.ConfigParser()
config.read("../DOCKS_config.cfg")
global DEBUG
DEBUG = int(config["general"]["extra_files"])

if __name__ == "__main__":    
    #the name of the config name will be passed at the command line (see TODOs)
    main("../DOCKS_config.cfg")

