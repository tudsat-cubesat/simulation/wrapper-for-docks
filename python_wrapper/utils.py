import pandas as pd
import numpy as np
from astropy.time import Time
from astropy.coordinates import EarthLocation
from astropy import units as u

def read_events(event_file):
    """
    reads the event file and returns the relevant data as 
    a pandas dataframe
    """
    df = pd.read_csv(event_file, skiprows=15, header=0, sep="\t",
        names=["day", "second", "event", "number"], 
        dtype={"day":np.int64, "second":np.float64, "event":str, "number":str})

    return df

def read_modes(mode_file):
    """
    reads the event file and returns the relevant data as
    a pandas dataframe
    """
    df = pd.read_csv(mode_file, skiprows=15, header=0, delim_whitespace=True,
        names=["day", "second", "mode"])

    return df

def read_trajectory(trajectory_file):
    """
    reads the trajectory file and returns the relevant data as 
    a pandas dataframe
    """
    df = pd.read_csv(trajectory_file, skiprows=19, header=0, sep="\t",
         names=["day", "second", "x", "y", "z", "v_x", "v_y", "v_z"])

    return df

def get_start_time_mjd(config):
    """
    converts the start time to mjd format and returns the day and second
    both as int 
    """
    t = Time(config["general"]["start_time"], format="isot", scale="utc")
    day = int(t.mjd)
    second = int((t.mjd - day) * 24*60*60) 
    return day, second

def get_end_time_mjd(config):
    """
    converts the end time to mjd format and returns the day and second
    both as int 
    """
    t = Time(config["general"]["end_time"], format="isot", scale="utc")
    day = int(t.mjd)
    second = int((t.mjd - day) * 24*60*60) 
    return day, second

def get_ground_stations(config):
    """
    returns a list of groundstations with each element in the list 
    containing [name, EarthLocation, visibility cone, index]
    """

    ground_stations = []
    for i in range(1, int(config["intervisibility"]["n_stations"])+1):
        
        name = config["intervisibility"]["name_"+str(i)]
        index = config["intervisibility"]["index_"+str(i)]
        lat = float(config["intervisibility"]["lat_"+str(i)])
        lon = float(config["intervisibility"]["lon_"+str(i)])
        height = float(config["intervisibility"]["height_"+str(i)])
        visi_cone = float(config["intervisibility"]["visi_cone_"+str(i)])
                   
        #add the groundstation to the list
        ground_stations.append([name, EarthLocation(lat=lat*u.deg, 
            lon=lon*u.deg, height=height*u.m), visi_cone, index])

    return ground_stations
